#!/bin/bash

os=$(uname -s)

case "${os}" in
    Darwin*)    machine_ip=$(ipconfig getifaddr en1);;
    *)          machine_ip=$(hostname -I | awk '{print $1}')
esac

yarn ionic capacitor run $1 --livereload-url=http://$machine_ip:8100