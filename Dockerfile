FROM node:20.16.0-bullseye-slim AS node_builder

ARG NS=www
ARG ENV64=""

WORKDIR /builder

RUN apt-get update && apt-get install -y git python3 make g++

COPY . .

RUN set -a && REACT_APP_REVISION=$(git describe --tags || echo "devbench")-$NS \
  && . /builder/env.$NS 2> /dev/null \
  && echo ENV64: $ENV64 \
  && echo $ENV64 | base64 -d > /tmp/env \
  && . /tmp/env 2> /dev/null \
  && set +a \
  && yarn \
  && yarn build

FROM golang:1.14.6-alpine3.12 AS go_builder

WORKDIR /builder

COPY ./server/ .

ENV GO111MODULE=on

RUN apk add -U --no-cache git
RUN go get -d -v ./...
ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64
RUN go build -ldflags="-s -w" .

FROM alpine:3.12

WORKDIR /app

COPY --from=go_builder /builder/pantahub-www /app/pantahub-www
COPY --from=node_builder /builder/build /app/public

RUN chmod +x /app/pantahub-www

CMD ["/app/pantahub-www"]
