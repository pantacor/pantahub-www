const ReactCompilerConfig = {
	target: "17", // '17' | '18' | '19'
};

module.exports = function () {
	return {
		presets: [
			["@parcel/babel-preset-env", { targets: { node: "current" } }],
			"@babel/preset-typescript",
		],
		plugins: [["babel-plugin-react-compiler", ReactCompilerConfig]],
	};
};
