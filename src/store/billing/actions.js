/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import * as Types from './types'

import { buildBasicActions } from '../../lib/redux.helpers'
import { processService } from '../../services/api.service'
import { catchError } from '../general-flash/actions'
import {
  GetCustomers,
  UpdateCustomer,
  GetSubscriptions,
  UpdateSubscriptions,
  GetPrices,
  GetSubscription,
  GetPayment,
  RefreshPayment,
  RefreshSubscription
} from '../../services/billing.service'

export const getCustomerActions = buildBasicActions(Types, 'CUSTOMER_GET')
export const updateCutomersActions = buildBasicActions(Types, 'CUSTOMER_UPDATE')
export const getSubscriptionsActions = buildBasicActions(Types, 'SUBSCRIPTIONS_GET')
export const getSubscriptionActions = buildBasicActions(Types, 'SUBSCRIPTION_GET')
export const updateSubscriptionActions = buildBasicActions(Types, 'SUBSCRIPTION_UPDATE')
export const getPricesActions = buildBasicActions(Types, 'PRICES_GET')
export const getPaymentActions = buildBasicActions(Types, 'PAYMENT_GET')
export const updatePaymentActions = buildBasicActions(Types, 'PAYMENT_UPDATE')

export const GetPricesAction = () => async (dispatch, getState) => {
  dispatch(getPricesActions.inProgress())

  const state = getState()
  return processService(
    () => GetPrices(state.auth.token),
    (resp) => dispatch(getPricesActions.success(resp)),
    (error) => dispatch(catchError(error, getPricesActions.failure))
  )
}

export const GetCustomersAction = () => async (dispatch, getState) => {
  dispatch(getCustomerActions.inProgress())
  const state = getState()
  return processService(
    GetCustomers.bind(null, state.auth.token, undefined),
    (resp) => dispatch(getCustomerActions.success(resp)),
    (error) => dispatch(catchError(error, getCustomerActions.failure))
  )
}

export const UpdateCustomerAction = (payload) => async (dispatch, getState) => {
  dispatch(updateCutomersActions.inProgress())
  const state = getState()

  return processService(
    UpdateCustomer.bind(null, state.auth.token, payload),
    (resp) => dispatch(updateCutomersActions.success(resp)),
    (error) => dispatch(catchError(error, updateCutomersActions.failure))
  )
}

export const GetSubscriptionsAction = () => async (dispatch, getState) => {
  dispatch(getSubscriptionsActions.inProgress())
  const state = getState()
  return processService(
    GetSubscriptions.bind(null, state.auth.token, undefined),
    (resp) => dispatch(getSubscriptionsActions.success(resp)),
    (error) => dispatch(catchError(error, getSubscriptionsActions.failure))
  )
}

export const GetSubscriptionAction = (id = '') => async (dispatch, getState) => {
  dispatch(getSubscriptionActions.inProgress())
  const state = getState()
  return processService(
    GetSubscription.bind(null, state.auth.token, id, undefined),
    (resp) => dispatch(getSubscriptionActions.success(resp)),
    (error) => dispatch(catchError(error, getSubscriptionActions.failure))
  )
}

export const UpdateSubscriptionAction = (id = '', body = {}) => async (dispatch, getState) => {
  dispatch(updateSubscriptionActions.inProgress())
  const state = getState()
  return processService(
    UpdateSubscriptions.bind(null, state.auth.token, id, body),
    (resp) => dispatch(updateSubscriptionActions.success(resp)),
    (error) => dispatch(catchError(error, updateSubscriptionActions.failure))
  )
}

export const RefreshSubscriptionAction = (id = '') => async (dispatch, getState) => {
  dispatch(updateSubscriptionActions.inProgress())
  const state = getState()
  return processService(
    RefreshSubscription.bind(null, state.auth.token, id, undefined),
    (resp) => dispatch(updateSubscriptionActions.success(resp)),
    (error) => dispatch(catchError(error, updateSubscriptionActions.failure))
  )
}

export const GetPaymentAction = (id = '') => async (dispatch, getState) => {
  dispatch(getPaymentActions.inProgress())
  const state = getState()
  return processService(
    GetPayment.bind(null, state.auth.token, id, undefined),
    (resp) => dispatch(getPaymentActions.success(resp)),
    (error) => dispatch(catchError(error, getPaymentActions.failure))
  )
}

export const RefreshPaymentAction = (id = '') => async (dispatch, getState) => {
  dispatch(getPaymentActions.inProgress())
  const state = getState()
  return processService(
    RefreshPayment.bind(null, state.auth.token, id, undefined),
    (resp) => dispatch(getPaymentActions.success(resp)),
    (error) => dispatch(catchError(error, getPaymentActions.failure))
  )
}

export const UpdatePaymentAction = (id = '', body = {}) => async (dispatch, getState) => {
  dispatch(updatePaymentActions.inProgress())
  const state = getState()
  return processService(
    GetPayment.bind(null, state.auth.token, id, body),
    (resp) => dispatch(updatePaymentActions.success(resp)),
    (error) => dispatch(catchError(error, updatePaymentActions.failure))
  )
}
