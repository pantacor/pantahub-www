
export const IMAGES_RELEASES_GET = 'imagereleases/get'
export const IMAGES_RELEASES_GET_INPROGRESS = 'imagereleases/get/progress'
export const IMAGES_RELEASES_GET_SUCCESS = 'imagereleases/get/success'
export const IMAGES_RELEASES_GET_FAILURE = 'imagereleases/get/fail'
