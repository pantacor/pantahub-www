/*
 * Copyright (c) 2017-2024 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import * as Types from "./types";
import { buildBasicActions } from "../../lib/redux.helpers";
import { safeAwait } from "../../lib/safeawait";
import { createObject, putObject } from "../../services/objects.service";

const uploadObjectsActions = buildBasicActions(Types, "UPLOAD_OBJECTS");

export const SetObjects = (objects) => (dispatch) => {
	return dispatch({
		type: Types.UPLOAD_OBJECTS_SET_OBJECTS,
		payload: objects,
	});
};

export const UploadObjects =
	(onSuccess = () => null) =>
	async (dispatch, getState) => {
		dispatch(uploadObjectsActions.inProgress());
		const state = getState();
		for (const [_, object] of state.imports.objects) {
			const payload = {
				size: object.size,
				sha256sum: object.sha,
				objectname: object.file_name,
				"mime-type": "application/octet-stream",
			};
			let [response, err] = await safeAwait(
				createObject(state.auth.token, payload),
			);
			if (err != null) {
				dispatch(uploadObjectsActions.failure(err));
				return;
			}

			if (response.status === 409) {
				const sizeint = response.json?.sizeint;
				if (
					sizeint !== undefined &&
					(sizeint === 0 || sizeint !== object.size)
				) {
					response.ok = true;
				}
			}

			if (!response.ok) {
				object.percentage = 100;
				object.done = true;
				dispatch({
					type: Types.UPLOAD_OBJECTS_UPDATE_OBJECT,
					payload: object,
				});
				return;
			}

			putObject(
				state.auth.token,
				response.json["signed-puturl"],
				object.data,
				(percentage) => {
					object.percentage = percentage;
					dispatch({
						type: Types.UPLOAD_OBJECTS_UPDATE_OBJECT,
						payload: object,
					});
				},
				() => {
					object.done = true;
					let alldone = true;
					state.imports.objects.forEach((o) => {
						alldone = alldone && o.done;
					});
					if (alldone) {
						dispatch(uploadObjectsActions.success());
						return onSuccess();
					} else {
						dispatch({
							UPLOAD_OBJECTS_UPDATE_OBJECT,
							payload: object,
						});
					}
				},
				(err) => {
					return dispatch(uploadObjectsActions.failure(err));
				},
			);
		}
	};
