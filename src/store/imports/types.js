/*
 * Copyright (c) 2024 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

export const UPLOAD_OBJECTS = "imports/upload_objects";
export const UPLOAD_OBJECTS_INPROGRESS = "imports/upload_objects/INPROGRESS";
export const UPLOAD_OBJECTS_SUCCESS = "imports/upload_objects/SUCCESS";
export const UPLOAD_OBJECTS_FAILURE = "imports/upload_objects/FAILURE";
export const UPLOAD_OBJECTS_SET_OBJECTS = "imports/upload_objects/SET_OBJECTS";
export const UPLOAD_OBJECTS_UPDATE_OBJECT =
	"imports/upload_objects/UPDATE_OBJECT";
