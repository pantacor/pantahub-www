/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import * as Types from './types'
import { buildBasicActions } from '../../lib/redux.helpers'
import { GetExportedParts } from '../../services/devices.service'
// import debounce from 'lodash.debounce'

// const refreshTime = 60000 * 30 // 30 minutes
const getExportedPartsActions = buildBasicActions(Types, 'EXPORTS_FETCH_SELECTION')

export const GetExportedPartsAction = (username, device, rev, filename, extra = {}) => async (dispatch, getState) => {
  dispatch(getExportedPartsActions.inProgress())
  const state = getState()

  return GetExportedParts(state.auth.token, username, device, rev, filename, extra)
    .then(async (res) => {
      const reader = res.body.getReader()
      let receivedLength = 0 // received that many bytes at the moment
      let chunks = [] // array of received binary chunks (comprises the body)

      while (true) {
        const { done, value } = await reader.read()

        if (done) {
          break
        }

        chunks.push(value)
        receivedLength += value.length

        dispatch(getExportedPartsActions.inProgress({ received: receivedLength }))
      }

      return new window.Blob(chunks)
    })
    .then((blob) => {
      var file = window.URL.createObjectURL(blob)
      var a = document.createElement('a')
      a.href = file
      a.download = filename
      a.target = '_black'
      a.referrerPolicy = 'noopener,noreferrer'
      a.click()
      return file
    })
    .then((url) => dispatch(getExportedPartsActions.success(url)))
    .catch((err) => dispatch(getExportedPartsActions.failure(err)))
}
