/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import * as Types from './types'

const s = {
  IN_PROGRESS: 'inprogr',
  FAILURE: 'failure',
  SUCCESS: 'success'
}

export const STATES = {
  [Types.EXPORTS_FETCH_SELECTION]: s
}

const exportsInitialState = {
  status: null,
  error: null
}

export const reduce = (state = exportsInitialState, action) => {
  switch (action.type) {
    case Types.EXPORTS_FETCH_SELECTION_INPROGRESS:
      return {
        ...state,
        status: STATES[Types.EXPORTS_FETCH_SELECTION].IN_PROGRESS,
        error: null,
        progress: action.payload
      }
    case Types.EXPORTS_FETCH_SELECTION_SUCCESS:
      return {
        ...state,
        status: STATES[Types.EXPORTS_FETCH_SELECTION].SUCCESS,
        fileUrl: action.payload,
        error: null
      }
    case Types.EXPORTS_FETCH_SELECTION_FAILURE:
      return {
        ...state,
        status: STATES[Types.EXPORTS_FETCH_SELECTION].FAILURE,
        error: action.error,
        progress: null
      }
    default:
      return state
  }
}
