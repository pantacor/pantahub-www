/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

// Support previous actions when claims where inside the devices store
export const DEV_CLAIM_SET_DEVICEID = 'claim/set-deviceid'
export const DEV_CLAIM_SET_SECRET = 'claim/set-secret'
export const DEV_CLAIM_INPROGR = 'claim/inprogress'
export const DEV_CLAIM_SUCCESS = 'claim/success'
export const DEV_CLAIM_FAILURE = 'claim/failure'
export const DEV_CLAIM_RESET = 'claim/reset'

// New actions for the claim store
export const TOGGLE_WATCH_NETWORK_DEVICES = 'claim/toggle-watch-network-devices'
export const TOGGLE_MANUAL = 'claim/toggle-manual'
export const ADD_DEVICE = 'claim/add-device'
export const REMOVE_DEVICE = 'claim/remove-device'
export const DEVICE = 'claim/device'
export const ALL_DEVICES = 'claim/all-devices'
