import merge from 'lodash.merge'
import * as Types from './types'

// eslint-disable-next-line camelcase
export const ClAIM_STATUSES = {
  IN_PROGRESS: 'inprogr',
  FAILURE: 'failure',
  SUCCESS: 'success'
}

const initialState = {
  devices: {},
  device: null,
  secret: null,
  loading: false,
  response: null,
  error: null,
  manual: false,
  watching: false
}

const serviceClass = {
  toString () {
    return `${this.id}`
  }
}

const mapDevice = (service) => ({
  ...serviceClass,
  ...service,
  id: service.txtRecord['device-id'],
  secret: service.txtRecord.challenge
})

export function reduce (state = initialState, action) {
  switch (action.type) {
    case Types.DEV_CLAIM_SET_DEVICEID:
      return merge({}, state, {
        device: action.id
      })

    case Types.TOGGLE_MANUAL:
      return {
        ...state,
        manual: !state.manual
      }

    case Types.DEV_CLAIM_SET_SECRET:
      return merge({}, state, {
        secret: action.secret
      })

    case Types.DEV_CLAIM_INPROGR:
      return merge({}, state, {
        loading: true,
        devices: {
          ...state.devices,
          [action.id]: {
            ...state.devices[action.id],
            status: ClAIM_STATUSES.IN_PROGRESS
          }
        }
      })

    case Types.DEV_CLAIM_RESET:
      return merge({}, state, {
        loading: false,
        response: null,
        error: null,
        device: null,
        secret: null
      })

    case Types.DEV_CLAIM_SUCCESS:
      return merge({}, state, {
        loading: false,
        response: action.response,
        error: null,
        device: null,
        secret: null,
        devices: {
          ...state.devices,
          [action.id]: {
            ...state.devices[action.id],
            status: ClAIM_STATUSES.SUCCESS
          }
        }
      })

    case Types.DEV_CLAIM_FAILURE:
      return merge({}, state, {
        loading: false,
        error: action.error,
        response: null,
        device: null,
        secret: null,
        devices: {
          ...state.devices,
          [action.id]: {
            ...state.devices[action.id],
            status: ClAIM_STATUSES.FAILURE
          }
        }
      })

    case Types.TOGGLE_WATCH_NETWORK_DEVICES:
      return {
        ...state,
        watching: !state.watching
      }

    case Types.ADD_DEVICE:
      return {
        ...state,
        devices: {
          ...state.devices,
          [action.payload.txtRecord['device-id']]: mapDevice(action.payload)
        }
      }

    default:
      return state
  }
}
