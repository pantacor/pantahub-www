/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import merge from 'lodash.merge'
import findIndex from 'lodash.findindex'
import orderBy from 'lodash.orderby'

import * as Types from './types'
import { resolvePath } from '../../lib/object.helpers'
import { GetEnv } from '../../lib/const.helpers'
import { findLatestStep } from '../../services/devices.service'

export const STATES = {
  IN_PROGRESS: 'inprogr',
  FAILURE: 'failure',
  SUCCESS: 'success'
}

const LOGS_BUFFER_SIZE = 10000
const devsInitialState = {
  devices: [],
  current: {
    device: null,
    rev: null,
    timestamp: null,
    currentStep: {},
    navigator: {
      activeTab: 'readme',
      setMeta: {
        loading: false,
        response: null,
        error: null
      },
      logsFilter: { logsFilterId: 0 },
      files: {}
    },
    editing: false
  },
  logs: {
    entries: [],
    loading: false,
    error: null,
    pastCursor: {
      valid: false,
      id: null
    },
    futureCursor: {
      valid: false,
      id: null
    }
  },
  timestamp: null,
  error: null,
  loaded: false,
  loading: false,
  refreshing: false,
  search: '',
  sortField: 'timestamp',
  sortDirection: true,
  delete: {},
  publish: {},
  unpublish: {},
  deploy: {},
  initializing: false,
  stepPosting: {
    loading: false,
    error: null
  }
}

function mergeLogs (state, newLogs, fromRefresher, fromFuture) {
  // if first fetch, invalidate previous log entries
  const oldEntries = fromRefresher ? resolvePath(state, 'logs.entries', []) : []
  const bufferSize = parseInt(GetEnv('REACT_APP_LOGS_SCROLLBACK', LOGS_BUFFER_SIZE))
  let newEntries
  let sliced = false
  if (fromFuture) {
    newEntries = [...oldEntries, ...newLogs]
    if (newEntries.length > bufferSize) {
      newEntries = newEntries.slice(-bufferSize)
      sliced = true
    }
  } else {
    newEntries = [...newLogs, ...oldEntries]
    if (newEntries.length > bufferSize) {
      newEntries = newEntries.slice(0, bufferSize)
      sliced = true
    }
  }
  return { entries: newEntries, sliced }
}

export const devsReducer = (state = devsInitialState, action) => {
  switch (action.type) {
    case Types.DEVS_INIT_INPROGR:
    case Types.DEV_INIT_INPROGR:
      return {
        ...state,
        initializing: true,
        loading: action.refresh !== undefined ? action.refresh : true,
        refreshing: action.refresh,
        search: action.refresh ? state.search : ''
      }

    case Types.DEVS_INIT_SUCCESS:
      return {
        ...state,
        initializing: false,
        loaded: true,
        devices: action.devices,
        error: null,
        timestamp: new Date()
      }

    case Types.DEVS_INIT_FAILURE:
    case Types.DEV_INIT_FAILURE:
      return {
        ...state,
        initializing: false,
        error: action.error,
        refreshing: false,
        loading: false
      }

    case Types.DEVS_SET_SEARCH:
      return {
        ...state,
        search: action.search
      }

    case Types.DEVS_SET_SORT_FIELD:
      const sortDirection =
        action.field === state.sortField ? !state.sortDirection : true
      return {
        ...state,
        sortField: action.field,
        sortDirection
      }

    case Types.DEVS_RESET:
    case Types.DEV_RESET:
      return { ...devsInitialState }

    case Types.DEV_REFRESH_LOGS_INPROGR:
      return {
        ...state,
        logs: {
          ...state.logs,
          loading: true
        }
      }
    case Types.DEV_REFRESH_LOGS_SUCCESS:
      return {
        ...state,
        logs: {
          ...state.logs,
          error: null,
          entries: mergeLogs(state, action.payload.newLogs).entries
        }
      }
    case Types.DEV_CREATE_LOGS_PAST_CURSOR_SUCCESS:
    case Types.DEV_FETCH_LOGS_PAST_CURSOR_SUCCESS:
    case Types.DEV_CREATE_LOGS_FUTURE_CURSOR_SUCCESS:
    case Types.DEV_FETCH_LOGS_FUTURE_CURSOR_SUCCESS:
      const mergeLogsResult = mergeLogs(state, action.payload.newLogs, action.payload.fromRefresher, action.payload.fromFuture)
      const futureCursor = action.payload.fromFuture
        ? {
          ...state.logs.futureCursor,
          valid: action.payload.newLogs.length > 0,
          id: action.payload.nextCursor
        }
        // - slicing when receiving logs will invalidate the cursor on other side of the log entries list
        // - first fetch will also invalidate the cursor
        : mergeLogsResult.sliced || action.payload.fromRefresher ? { valid: false, id: null } : state.logs.futureCursor
      const pastCursor = !action.payload.fromFuture
        ? {
          ...state.logs.pastCursor,
          valid: action.payload.newLogs.length > 0,
          id: action.payload.nextCursor
        }
        // - slicing when receiving logs will invalidate the cursor on other side of the log entries list:
        // - first fetch will also invalidate the cursor
        : mergeLogsResult.sliced || action.payload.fromRefresher ? { valid: false, id: null } : state.logs.pastCursor
      return {
        ...state,
        logs: {
          ...state.logs,
          entries: mergeLogsResult.entries,
          loading: false,
          error: null,
          pastCursor,
          futureCursor
        }
      }

    case Types.DEV_FETCH_LOGS_FUTURE_CURSOR_FAILURE:
    case Types.DEV_FETCH_LOGS_PAST_CURSOR_FAILURE:
      // A failure in fetching will invalidate current cursors
      return {
        ...state,
        logs: {
          ...state.logs,
          loading: false,
          error: action.error,
          pastCursor: {
            valid: false,
            id: null
          },
          futureCursor: {
            valid: false,
            id: null
          }
        }
      }

    case Types.DEV_SET_LOGS_FILTER:
      return {
        ...state,
        current: {
          ...state.current,
          navigator: {
            ...state.current.navigator,
            logsFilter: {
              ...action.logsFilter,
              logsFilterId: state.current.navigator.logsFilter.logsFilterId + 1
            }
          }
        },
        logs: {
          ...state.logs,
          pastCursor: {
            valid: false,
            id: null
          },
          futureCursor: {
            valid: false,
            id: null
          }
        }
      }

    case Types.DEV_INIT_SUCCESS:
      return {
        ...state,
        loading: false,
        initializing: false,
        error: null,
        refreshing: false,
        current: {
          ...state.current,
          device: {
            ...((state.current || {}).device || {}),
            ...(action.device || {})
          },
          timestamp: new Date()
        }
      }

    case Types.DEV_NAVIGATOR_SET_TAB:
      return {
        ...state,
        current: {
          ...state.current,
          navigator: {
            ...state.current.navigator,
            activeTab: action.tab
          }
        }
      }

    case Types.DEV_DELETE_INPROGR:
      return merge({}, state, {
        delete: {
          [action.id]: {
            loading: true
          }
        }
      })
    case Types.DEV_DELETE_SUCCESS:
      // delete the device from store to avoid the roundtrip
      return merge({}, state, {
        devices: orderBy(action.devices, ['device-nick'], ['asc']).filter(d => d.deviceid !== action.id),
        delete: {
          [action.id]: {
            error: null,
            loading: false
          }
        }
      })
    case Types.DEV_DELETE_FAILURE:
      return merge({}, state, {
        delete: {
          [action.id]: {
            loading: false,
            error: action.error
          }
        }
      })

    case Types.DEV_PUBLISH_INPROGR:
      return merge({}, state, {
        publish: {
          [action.id]: {
            loading: true
          }
        }
      })
    case Types.DEV_PUBLISH_SUCCESS:
      const devIndex = findIndex(state.devices, { deviceid: action.id })
      const allDevs = state.devices.slice()
      const dev = allDevs[devIndex]
      allDevs[devIndex] = {
        ...dev,
        public: action.isPublic
      }

      return {
        ...state,
        devices: allDevs,
        publish: {
          ...state.publish,
          [action.id]: {
            loading: false
          }
        }
      }
    case Types.DEV_PUBLISH_FAILURE:
      return merge({}, state, {
        publish: {
          [action.id]: {
            loading: false,
            error: action.error
          }
        }
      })

    case Types.DEV_SET_META_INPROGR:
      return {
        ...state,
        current: {
          ...state.current,
          navigator: {
            ...state.current.navigator,
            setMeta: {
              loading: true
            }
          }
        }
      }
    case Types.DEV_SET_META_SUCCESS:
      return {
        ...state,
        current: {
          ...state.current,
          navigator: {
            ...state.current.navigator,
            setMeta: {
              loading: false,
              error: null
            }
          }
        }
      }
    case Types.DEV_SET_META_FAILURE:
      return {
        ...state,
        current: {
          ...state.current,
          navigator: {
            ...state.current.navigator,
            setMeta: {
              loading: false,
              error: action.error
            }
          }
        }
      }

    case Types.DEV_FETCH_FILE_INPROGR:
      return {
        ...state,
        current: {
          ...state.current,
          navigator: {
            ...state.current.navigator,
            files: {
              ...state.current.navigator.files,
              [action.id]: {
                ...(state.current.navigator.files[action.id] || {}),
                [action.key]: { loading: true }
              }
            }
          }
        }
      }
    case Types.DEV_FETCH_FILE_FAILURE:
      return {
        ...state,
        current: {
          ...state.current,
          navigator: {
            ...state.current.navigator,
            files: {
              ...state.current.navigator.files,
              [action.id]: {
                ...(state.current.navigator.files[action.id] || {}),
                [action.key]: { loading: false, error: action.error }
              }
            }
          }
        }
      }
    case Types.DEV_FETCH_FILE_SUCCESS:
      return {
        ...state,
        current: {
          ...state.current,
          navigator: {
            ...state.current.navigator,
            files: {
              ...state.current.navigator.files,
              [action.id]: {
                ...(state.current.navigator.files[action.id] || {}),
                [action.key]: { loading: false, content: action.content }
              }
            }
          }
        }
      }
    case Types.DEV_SET_EDITING:
      return {
        ...state,
        current: {
          ...state.current,
          editing: action.editing
        }
      }
    case Types.DEV_PATCH_INPROGR:
      return {
        ...state,
        saving: {
          ...state.saving,
          [action.id]: STATES.IN_PROGRESS
        }
      }
    case Types.DEV_PATCH_SUCCESS:
      return {
        ...state,
        saving: {
          ...state.saving,
          [action.id]: STATES.SUCCESS
        }
      }
    case Types.DEV_PATCH_FAILURE:
      return {
        ...state,
        saving: {
          ...state.saving,
          [action.id]: STATES.FAILURE
        }
      }
    //  completing code (post_rev)
    case Types.DEV_POST_REV_INPROGR:
      return {
        ...state,
        stepPosting: {
          loading: true,
          error: null
        },
        deploy: {
          ...state.deploy,
          [action.id]: STATES.IN_PROGRESS
        }
      }
    case Types.DEV_POST_REV_SUCCESS:
      return {
        ...state,
        stepPosting: {
          loading: false,
          error: null
        },
        deploy: {
          ...state.deploy,
          [action.id]: STATES.SUCCESS
        }
      }
    case Types.DEV_POST_REV_FAILURE:
      return {
        ...state,
        stepPosting: {
          loading: false,
          error: action.payload
        },
        deploy: {
          ...state.deploy,
          [action.id]: STATES.FAILURE
        }
      }
    case Types.DEV_FETCH_SUMMARY_SUCCESS:
      return {
        ...state,
        loading: false,
        initializing: false,
        error: null,
        refreshing: false,
        current: {
          ...state.current,
          device: {
            ...resolvePath(state, 'current.device', {}),
            ...resolvePath(action.payload, 'device', {}),
            objects: action.payload.objects,
            summary: {
              ...resolvePath(state, 'current.device.summary', {}),
              ...resolvePath(action.payload, 'summary', {})
            },
            history: {
              ...resolvePath(state, 'current.device.history', {}),
              revision: action.payload.revision,
              lastRevision: action.payload.lastRevision,
              revIndex: action.payload.revision,
              currentStep: action.payload.currentStep
            }
          },
          timestamp: new Date()
        }
      }
    case Types.DEV_FETCH_SUMMARY_FAILURE:
      return {
        ...state,
        loading: false,
        initializing: false,
        error: action.error,
        refreshing: false
      }
    case Types.DEV_FETCH_SUMMARY_INPROGRESS:
      return {
        ...state,
        loading: false,
        initializing: true,
        error: null,
        refreshing: true
      }
    case Types.DEV_FETCH_STEPS_INPROGRESS:
      return state
    case Types.DEV_FETCH_STEPS_SUCCESS:
      return {
        ...state,
        current: {
          ...state.current,
          device: {
            ...resolvePath(state, 'current.device', {}),
            history: {
              ...resolvePath(state, 'current.device.history', {}),
              steps: action.payload,
              lastRevision: findLatestStep(action.payload).rev
            }
          }
        }
      }
    case Types.DEV_FETCH_OBJECTS_FAILURE:
      return state
    case Types.DEV_FETCH_OBJECTS_INPROGRESS:
      return state
    case Types.DEV_FETCH_OBJECTS_SUCCESS:
      return {
        ...state,
        current: {
          ...state.current,
          device: {
            ...resolvePath(state, 'current.device', {}),
            history: {
              ...resolvePath(state, 'current.device.history', {}),
              currentStep: action.payload.currentStep
            },
            objects: action.payload.objects
          }
        }
      }
    case Types.DEV_FETCH_STEPS_FAILURE:
      return state
    default:
      return state
  }
}
