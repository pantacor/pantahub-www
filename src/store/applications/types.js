/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

export const APP_SET = 'app/set'
export const APP_SET_SORT = 'app/set/sort'
export const APP_CURRENT_CLEAN = 'app/current/clean'

export const APP_CREATE = 'app/create'
export const APP_CREATE_INPROGRESS = 'app/create/progress'
export const APP_CREATE_SUCCESS = 'app/create/success'
export const APP_CREATE_FAILURE = 'app/create/progress'

export const APP_GET_ALL = 'app/get-all'
export const APP_GET_ALL_INPROGRESS = 'app/get-all/inprogress'
export const APP_GET_ALL_SUCCESS = 'app/get-all/success'
export const APP_GET_ALL_FAILURE = 'app/get-all/failure'

export const APP_UPDATE = 'app/update'
export const APP_UPDATE_INPROGRESS = 'app/update/inprogress'
export const APP_UPDATE_SUCCESS = 'app/update/success'
export const APP_UPDATE_FAILURE = 'app/update/failure'

export const APP_GET = 'app/get'
export const APP_GET_INPROGRESS = 'app/get/inprogress'
export const APP_GET_SUCCESS = 'app/get/success'
export const APP_GET_FAILURE = 'app/get/failure'

export const APP_DELETE = 'app/delete'
export const APP_DELETE_INPROGRESS = 'app/delete/inprogress'
export const APP_DELETE_SUCCESS = 'app/delete/success'
export const APP_DELETE_FAILURE = 'app/delete/failure'

export const APP_GET_SCOPES = 'app/get-scopes'
export const APP_GET_SCOPES_INPROGRESS = 'app/get-scopes/inprogress'
export const APP_GET_SCOPES_SUCCESS = 'app/get-scopes/success'
export const APP_GET_SCOPES_FAILURE = 'app/get-scopes/failure'
