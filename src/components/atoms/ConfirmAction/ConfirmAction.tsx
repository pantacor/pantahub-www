/*
 * Copyright (c) 2017-2023 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import * as React from 'react'
import DeleteIcon from '@mui/icons-material/Delete'
import { Button } from '../Button/Button'

export interface IConfirmActionProps {
  onClose: () => void;
  onConfirm: () => void;
  btnClassName: string;
  closeClasses: string;
  confirmClases: string;
  closeBtnTxt: string;
  confirmBtnTxt: string;
  btnchildren: React.ReactChildren;
  title: string;
  delay: number;
}

export const ConfirmAction: React.FunctionComponent<IConfirmActionProps> = (props) => {
  const {
    delay = 400,
    closeBtnTxt = 'Close',
    confirmBtnTxt = 'Confirm',
    btnClassName = 'btn btn-sm btn-light btn-link',
    closeClasses = 'btn btn-secondary btn-sm',
    confirmClases = 'btn btn-primary btn-sm',
    title = 'Remove',
    onClose = () => {},
    onConfirm = () => {},
    ...extras
  } = props

  const [open, setOpen] = React.useState(false)

  const onCloseHandler = () => {
    setOpen(false)
    const timeout = setTimeout(() => {
      onClose()
    }, delay)
    return () => {
      clearTimeout(timeout)
    }
  }

  const onConfirmHandler = () => {
    setOpen(false)
    const timeout = setTimeout(() => {
      onConfirm()
    }, delay)
    return () => {
      clearTimeout(timeout)
    }
  }

  if (!open) {
    return (
      <Button
        title={title}
        type="button"
        className={btnClassName}
        style={{ color: 'red' }}
        aria-label="Remove"
        onClick={() => setOpen(true)}
        {...extras}
      >
        {props.btnchildren || (<DeleteIcon />)}
      </Button>
    )
  }

  return (
    <div className={`modal fade ${open ? 'show' : ''}`} tabIndex={-1} style={{ display: open ? 'block' : 'none' }} >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">
              {props.title}
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              onClick={onCloseHandler}
              aria-label="Close"></button>
          </div>
          <div className="modal-body">
            {props.children}
          </div>
          <div className="modal-footer">
            <button
              type="button"
              onClick={onCloseHandler}
              className={closeClasses}
              data-bs-dismiss="modal"
            >
              {closeBtnTxt}
            </button>
            <button
              type="button"
              onClick={onConfirmHandler}
              className={confirmClases}
            >
              {confirmBtnTxt}
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}
