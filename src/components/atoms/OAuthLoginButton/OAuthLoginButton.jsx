/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React from 'react'

import './oauthloginbutton.scss'

export default function OAuthLoginButton ({ service, message, disabled, size }) {
  const onClick = (e) => {
    if (e && e.preventDefault) {
      e.preventDefault()
    }

    const searchParams = new URLSearchParams(window.location.search)
    if (searchParams.get('redirect')) {
      window.localStorage.setItem('returnto', searchParams.get('redirect'))
    }

    window.location.href = service.GetUrl()
  }
  return (
    <a
      className={`btn btn-sm btn-oauth-login btn-oauth-login--${service.id} btn-oauth-login--${size} ${disabled && 'btn-disabled'}`}
      onClick={onClick}
      href={service.GetUrl()}
    >
      <i className={`mdi ${service.icon}`} aria-hidden="true" />
      <span className="text">
        {message || 'Login with '}
        <span className="brand">{service.name}</span>
      </span>
    </a>
  )
}
