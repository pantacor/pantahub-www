import React, { useEffect, useState } from "react";

export function AlertMessage({ msg, type = "primary", timeout = 1000 }) {
	const [active, setActive] = useState(true);

	useEffect(() => {
		const id = setTimeout(() => {
			setActive(false);
		}, timeout);

		return () => {
			clearTimeout(id);
		};
	}, []);

	if (!active) {
		return null;
	}

	return (
		<div className={`alert alert-${type}`} role="alert">
			{msg}
		</div>
	);
}
