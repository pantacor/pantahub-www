/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import Editor from '@monaco-editor/react'
import React, { useEffect, useRef, useState } from 'react'
import RenderIf from '../RenderIf/RenderIf'
import { CopyToClipboard } from 'react-copy-to-clipboard'

export const JSONDisplayEditable = ({ value, saveValue, height = '400px' }) => {
  const jsonStr = JSON.stringify(value, null, 2)
  const [isEditing, setIsEditing] = useState(false)
  const [editedJson, setEditedJson] = useState(jsonStr)
  const [commitText, setcommit] = useState('')
  const [err, setErr] = useState('')

  const editorRef = useRef()

  useEffect(() => {
    if (editorRef.current && !editorRef.current.classList.contains('hljs')) {
      window.hljs.highlightAll()
      window.hljs.initLineNumbersOnLoad()
    }
  }, [])

  const handleCancelClick = () => {
    setIsEditing(false)
    setEditedJson(jsonStr)
  }

  const handleSaveClick = () => {
    if (commitText === '') {
      setErr(`commit mesasge can't be empty`)
      return
    }
    setIsEditing(false)
    const updatedValue = JSON.parse(editedJson)
    saveValue(updatedValue, commitText)
    setcommit('')
  }

  const handleEditClick = () => {
    setIsEditing(true)
  }

  const handleEditorChange = (value) => {
    setEditedJson(value)
  }

  const handleCommitMessageChange = (event) => {
    setcommit(event.target.value)
  }

  if (isEditing) {
    return (
      <div className="json-with-clipboard">
        <Editor
          height={height}
          defaultLanguage="json"
          value={editedJson}
          onChange={handleEditorChange}
          options={{
            automaticLayout: true,
            wordWrap: 'on'
          }}
        />
        <div className='row col-11 mt-4 ml-3'>
          <div className='col-10'>
            <input
              className='col-12'
              type="text"
              value={commitText}
              onChange={handleCommitMessageChange}
              placeholder="Please insert your commit text"
            />
            <RenderIf condition={err !== ''}>
              <div className="alert alert-danger" role="alert">
                {err}
              </div>
            </RenderIf>
          </div>
          <div className='col'>
            <button className="btn btn-sm btn-light" onClick={handleCancelClick}>
              Cancel
            </button>
          </div>
          <div className='col'>
            <button className="btn btn-sm btn-light" onClick={handleSaveClick}>
              Save
            </button>
          </div>
        </div>
      </div>
    )
  }

  return (
    <div className="json-with-clipboard">
      <span>
        <div className='justify-content-end' style={{ position: 'absolute', right: 0 }}>
          <div>
            <button
              className="btn btn-sm btn-light"
              onClick={handleEditClick}>
              <i className="mdi mdi-pencil" aria-hidden="true" />
            </button>
            <CopyToClipboard
              text={jsonStr}>
              <button className="btn btn-sm btn-light">
                <i className="mdi mdi-content-copy" aria-hidden="true" />
              </button>
            </CopyToClipboard>
          </div>
        </div>
        <div className="json-value-wrapper js-syntax-highlight dark">
          <pre>
            <code className="language-json">{jsonStr}</code>
          </pre>
        </div>
      </span>
    </div>
  )
}
