import React from 'react'
import dayjs from 'dayjs'

export function DateFromStamp ({ when, format = 'DD MMM YYYY' }) {
  const date = dayjs.unix(when)

  return (
    <time dateTime={date.format()}>
      <span title={date.format()}>
        {date.format(format)}
      </span>
    </time>
  )
}
