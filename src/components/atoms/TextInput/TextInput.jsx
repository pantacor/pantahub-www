import React from 'react'

export default function TextInput (props) {
  const {
    name,
    label,
    onInput = () => {},
    onChange = () => {},
    error,
    className = 'form-control',
    type = 'text',
    ...rest
  } = props

  if (!label) {
    return (
      <React.Fragment>
        <input
          type={type}
          id={name}
          name={name}
          onInput={onInput}
          onChange={onChange}
          className={className}
          {...rest}
        />
        <div className="invalid-feedback"> {error} </div>
      </React.Fragment>
    )
  }

  return (
    <div className="form-group pb-2">
      <label htmlFor={name}>{label}</label>
      <input
        type={type}
        id={name}
        name={name}
        onInput={onInput}
        onChange={onChange}
        className={className}
        {...rest}
      />
      <div className="invalid-feedback"> {error} </div>
    </div>
  )
}
