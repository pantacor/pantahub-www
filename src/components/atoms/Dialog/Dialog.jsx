import React, { useRef, useState } from 'react'

const CloseDefault = () => (
  <span>Close</span>
)

const AcceptDefault = () => (
  <span>Accept</span>
)

export function Dialog (props) {
  const [open, setOpen] = useState(true)
  const ref = useRef()
  const Close = props.Close || CloseDefault
  const Accept = props.Accept || AcceptDefault

  const handleClose = (e) => {
    e.preventDefault()
    e.stopPropagation()

    if (!e.target || !ref.current) return
    if (ref.current.contains(e.target) && (!e.target.classList.contains('btn') || e.target.closest('.btn') === undefined)) {
      return
    }

    if (props.onClose) {
      props.onClose(e)
    }

    setOpen(false)
  }
  const handleAccept = (e) => {
    e.preventDefault()
    e.stopPropagation()

    if (props.onAccept) {
      props.onAccept(e)
    }
    setOpen(false)
  }

  if (!open) {
    return null
  }
  return (
    <div
      className={`modal fade ${open ? 'show' : ''}`} tabIndex="-1"
      onClick={handleClose}
      style={{ display: open ? 'block' : 'none' }}
    >
      <div ref={ref} className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            {props.title && (
              <h5 className="modal-title">
                {props.title}
              </h5>
            )}
            <button
              type="button"
              className="btn btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
              onClick={handleClose}
            />
          </div>
          <div className="modal-body">
            {props.children}
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-bs-dismiss="modal"
              onClick={handleClose}

            >
              <Close />
            </button>
            <button
              type="button"
              className="btn btn-primary"
              onClick={handleAccept}
            >
              <Accept />
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}
