import React from 'react'

export default function TextArea (props) {
  const {
    name,
    label,
    onInput = () => {},
    onChange = () => {},
    error,
    className = 'form-control',
    ...rest
  } = props

  if (!label) {
    return (
      <React.Fragment>
        <textarea
          id={name}
          name={name}
          onInput={onInput}
          onChange={onChange}
          className={className}
          {...rest}
        />
        <div className="invalid-feedback"> {error} </div>
      </React.Fragment>
    )
  }

  return (
    <div className="form-group pb-2">
      <label htmlFor={name}>{label}</label>
      <textarea
        id={name}
        name={name}
        onInput={onInput}
        onChange={onChange}
        className={className}
        {...rest}
      />
      <div className="invalid-feedback"> {error} </div>
    </div>
  )
}
