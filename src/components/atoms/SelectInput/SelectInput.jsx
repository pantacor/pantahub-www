import React from 'react'

export default function SelectInput (props) {
  const {
    name,
    onInput = () => {},
    onChange = () => {},
    options = [],
    label = '',
    error,
    value,
    ...rest
  } = props

  const inner = (
    <React.Fragment>
      <select
        className="form-control"
        id={name}
        name={name}
        onInput={onInput}
        onChange={onChange}
        value={value}
        {...rest}
      >
        {options.map((o, id) => (
          <option key={id} value={o.value}>{o.label}</option>
        ))}
      </select>
      <div className="invalid-feedback">
        {error}
      </div>
    </React.Fragment>
  )

  return label.length > 0
    ? (
      <div className="form-group pb-2">
        <label htmlFor={name}>{label}</label>
        {inner}
      </div>
    )
    : (
      <React.Fragment>
        {inner}
      </React.Fragment>
    )
}
