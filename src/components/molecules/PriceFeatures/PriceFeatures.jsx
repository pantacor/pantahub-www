import React from 'react'
import './style.scss'

export function PriceFeatures ({ features = [] }) {
  if (features === null) {
    return null
  }

  return (
    <ul className="price-features">
      {features.map((val, index) => (
        <li key={index} className="price-feature">
          <span className='price-feature--value flex-fill'>
            {val}
          </span>
        </li>
      ))}
    </ul>
  )
}
