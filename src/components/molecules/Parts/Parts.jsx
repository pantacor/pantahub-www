import React, { useEffect, useState } from 'react'
import { resolvePath } from '../../../lib/object.helpers'
import { EMPTY_SOURCE, ShortSha, SourceImageUrl, SourceSummary } from '../Tile/Tile'
import { useRouteMatch, useHistory } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { GetPvrURL } from '../../../lib/const.helpers'
import { Dialog } from '../../atoms/Dialog/Dialog'
import { SignedIcon } from '../../atoms/SignedIcon/SignedIcon'

const DEFAULT_STATE_MAPPER = [
  '/src.json',
  '/run.json'
]

export function getStateKeys (state = {}, types = DEFAULT_STATE_MAPPER) {
  const mappedStateByService = Object.keys(state)
    .reduce((acc, key) => {
      if (types.some(t => key.indexOf(t) >= 0) && key.indexOf('bsp') < 0) {
        const cleanKey = types.reduce((acc, t) => acc.replace(t, ''), key)
        const value = acc[cleanKey] || { signatures: [] }
        acc[cleanKey] = {
          ...value,
          ...EMPTY_SOURCE,
          ...state[key].value,
          ...state[key],
          signatures: value.signatures.concat(state[key].signatures),
          title: cleanKey
        }
      }
      return acc
    }, {})

  return Object
    .keys(mappedStateByService)
    .reduce((acc, key) => [...acc, mappedStateByService[key]], [])
}

export default function Parts (props) {
  const state = resolvePath(
    props,
    'device.history.currentStep.state',
    {}
  )
  const rawStateExists =
    resolvePath(props, 'device.history.currentStep.rawState', null) !== null
  const [sources, setSources] = useState([])

  useEffect(() => {
    if (rawStateExists) {
      setSources(getStateKeys(state))
    }
  }, [state, rawStateExists])

  return (
    <div className='device-tab__inner device-tiles'>
      <div className='device-tiles__inner pt-30 pb-30'>
        <div className="table-responsive">
          <table className="table table-borderless table-hover table-responsive table-lg">
            <thead>
              <tr>
                <th>Name</th>
                <th>Image</th>
                <th>Version</th>
                <th>Run Level</th>
                <th>pvr</th>
              </tr>
            </thead>
            <tbody>
              {sources.map((s, index) => (
                <Part key={s.value.docker_digest || index} source={s} />
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

function Part ({ source }) {
  const [cloneUrl, setCloneUrl] = useState()
  const { url } = useRouteMatch()
  const { revision, device } = useSelector((state) => ({
    revision: state.devs.current.device.history.currentStep.rev,
    device: state.devs.current.device
  }))
  const browserHistory = useHistory()

  const baseCloneUrl = device.history.lastRevision === device.history.revision
    ? `${GetPvrURL()}/${device['owner-nick']}/${device.nick}`
    : `${GetPvrURL()}/${device['owner-nick']}/${device.nick}/${revision}`

  const onClickRedirectToFile = (e) => {
    if (!e.target) {
      return
    }
    browserHistory.push(`${url.replace('/components', '')}/files/-/${source.title}`)
  }

  const onClickCopyURL = (e) => {
    e.preventDefault()
    e.stopPropagation()

    const signatures = (source.signatures || []).reduce((acc, sig) => {
      acc = `${acc},${sig}`
      return acc
    }, '')

    const url = `${baseCloneUrl}/#${source.title},_config/${source.title}${signatures}`
    setCloneUrl(url)
    navigator.clipboard.writeText(url)
  }

  return (
    <tr onClick={onClickRedirectToFile} style={{ cursor: 'pointer' }}>
      <td>
        {source.title}
      </td>
      <td>
        <SourceSummary
          data={{
            value: {
              summary: SourceImageUrl(source.docker_name, source.docker_tag),
              full: `${source.docker_name}:${source.docker_tag}`
            }
          }}
        />
      </td>
      <td>
        <SourceSummary
          data={{
            value: {
              summary: ShortSha(source.docker_digest, source.docker_ovl_digest),
              full: source.docker_digest === 'sha:' ? source.docker_ovl_digest : source.docker_digest
            }
          }}
        />
      </td>
      <td>
        {source.runlevel || 'platform'}
      </td>
      <td>
        <div className="d-flex justify-content-start">
          <div className="btn-group">
            <button
              onClick={onClickCopyURL}
              className="btn btn-sm btn-default"
            >
              <i
                className="mdi mdi-content-copy"
                title="Copy clone URL"
              />
            </button>
            {source.signatures.map((s) => (
              <button
                key={s}
                className="btn btn-sm btn-default"
                onClick={onClickRedirectToFile}
              >
                <SignedIcon />
              </button>
            ))}
          </div>
          {cloneUrl && (
            <Dialog
              title="url copied to your clipboard!"
              onClose={() => setCloneUrl(false)}
              onAccept={() => setCloneUrl(false)}
            >
              <span className="text-break">{cloneUrl}</span>
            </Dialog>
          )}
        </div>
      </td>
    </tr>
  )
}
