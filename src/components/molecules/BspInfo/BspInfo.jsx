import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import { Link, useRouteMatch } from 'react-router-dom'
import { GetPvrURL } from '../../../lib/const.helpers'
import { resolvePath } from '../../../lib/object.helpers'
import { SignedIcon } from '../../atoms/SignedIcon/SignedIcon'
import { Dialog } from '../../atoms/Dialog/Dialog'
import { useHistory } from 'react-router'

export function BspInfoList ({ state = {} }) {
  const { url } = useRouteMatch()

  const bspState = Object.keys(state).reduce((acc, key) => {
    if (key.indexOf('bsp/') >= 0) {
      acc[key.replaceAll('bsp/', '')] = state[key]
    }
    return acc
  }, {})

  if (!bspState || !bspState['build.json'] || !bspState['src.json']) {
    return null
  }

  const signed = resolvePath(state, 'signatures.include', []).some((include) => {
    return include.sig.indexOf('bsp') >= 0
  })

  return (
    <section className='bsp-info row'>
      <div className="col-md-4">
        <div className="table-responsive">
          <ul className="ml-0 pl-0">
            <li className="d-flex flex-column">
              <span className="list-title mt-2 text-capitalize fw-bolder text-primary">
                version
              </span>
              <span className="text-monospace">
                {bspState['build.json']['value']['gitdescribe']}
                {signed && (
                  <Link
                    className="btn btn-sm btn-default"
                    to={`${url.replace('/components', '')}/files/-/bsp`}
                  >
                    <SignedIcon />
                  </Link>
                )}
              </span>
            </li>
            <li className="d-flex flex-column">
              <span className="list-title mt-2 text-capitalize fw-bolder text-primary">
                platform
              </span>
              <span className="">
                {bspState['build.json']['value']['platform']}
              </span>
            </li>
            <li className="d-flex flex-column">
              <span className="list-title mt-2 text-capitalize fw-bolder text-primary">
                target
              </span>
              <span className="">
                {bspState['build.json']['value']['target']}
              </span>
            </li>
          </ul>
        </div>
      </div>
      <div className="col-md-8">
        <div className="table-responsive">
          <ul className="ml-0 pl-0">
            <li className="d-flex flex-column">
              <span className="list-title mt-2 text-capitalize fw-bolder text-primary">
                source
              </span>
              <span className="">
                {bspState['src.json']['value']['pvr']}
              </span>
            </li>
            <li className="d-flex flex-column">
              <span className="list-title mt-2 text-capitalize fw-bolder text-primary">
                PVR version
              </span>
              <span className="text-monospace">
                {bspState['build.json']['value']['pvrversion'].replaceAll('pvr version ', '')}
              </span>
            </li>
          </ul>
        </div>
      </div>
    </section>
  )
}

export function BspInfoTable ({ state = {} }) {
  const [cloneUrl, setCloneUrl] = useState()
  const { url } = useRouteMatch()
  const { revision, device } = useSelector((state) => ({
    revision: state.devs.current.device.history.currentStep.rev,
    device: state.devs.current.device
  }))

  if (!device) {
    return null
  }

  const browserHistory = useHistory()

  const baseCloneUrl = device.history.lastRevision === device.history.revision
    ? `${GetPvrURL()}/${device['owner-nick']}/${device.nick}`
    : `${GetPvrURL()}/${device['owner-nick']}/${device.nick}/${revision}`

  const bspState = Object.keys(state).reduce((acc, key) => {
    if (key.indexOf('bsp/') >= 0) {
      acc[key.replaceAll('bsp/', '')] = state[key]
    }
    return acc
  }, {})

  if (!bspState || !bspState['build.json'] || !bspState['src.json']) {
    return null
  }

  const signed = Object.keys(bspState).reduce((acc, key) => {
    if (key.indexOf('src.json') >= 0) {
      return acc
    }

    return acc && bspState[key].signatures.length > 0
  }, true)

  const onClickRedirectToFile = (e) => {
    if (!e.target) {
      return
    }
    browserHistory.push(`${url.replace('/components', '')}/files/-/bsp`)
  }

  const onClickCopyURL = (e) => {
    e.preventDefault()
    e.stopPropagation()

    const url = `${baseCloneUrl}#bsp,_sigs/bsp.json,device.json`
    setCloneUrl(url)
    navigator.clipboard.writeText(url)
  }
  return (
    <section className='bsp-info row' onClick={onClickRedirectToFile} style={{ cursor: 'pointer' }}>
      <div className="col-md-12">
        <div className="table-responsive">
          <table className="table table-borderless table-hover table-responsive table-lg">
            <thead>
              <tr>
                <th>Target</th>
                <th>Platform</th>
                <th>Source</th>
                <th>Version</th>
                <th>PVR version</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className="">
                  <span className="">
                    {bspState['build.json']['value']['target']}
                  </span>
                </td>
                <td className="">
                  <span className="">
                    {bspState['build.json']['value']['platform']}
                  </span>
                </td>
                <td className="">
                  <span className="">
                    {bspState['src.json']['value']['pvr']}
                  </span>
                </td>
                <td className="">
                  <span className="">
                    {bspState['build.json']['value']['gitdescribe']}
                  </span>
                </td>
                <td className="">
                  <span className="">
                    {bspState['build.json']['value']['pvrversion'].replaceAll('pvr version ', '')}
                  </span>
                </td>
                <td>
                  <button
                    onClick={onClickCopyURL}
                    className="btn btn-sm btn-default"
                  >
                    <i
                      className="mdi mdi-content-copy"
                      title="Copy clone URL"
                    />
                  </button>
                  {signed && (
                    <Link
                      className="btn btn-sm btn-default"
                      to={`${url.replace('/components', '')}/files/-/bsp`}
                    >
                      <SignedIcon />
                    </Link>
                  )}
                </td>
              </tr>
            </tbody>
          </table>
          {cloneUrl && (
            <Dialog
              title="url copied to your clipboard!"
              onClose={() => setCloneUrl(false)}
              onAccept={() => setCloneUrl(false)}
            >
              <span className="text-break">{cloneUrl}</span>
            </Dialog>
          )}
        </div>
      </div>
    </section>
  )
}

export function BspInfoWithTitle ({ state = {} }) {
  const bspState = Object.keys(state).reduce((acc, key) => {
    if (key.indexOf('bsp/') >= 0) {
      acc[key.replaceAll('bsp/', '')] = state[key]
    }
    return acc
  }, {})

  if (Object.keys(bspState).length <= 0) {
    return null
  }

  if (!bspState['build.json'] || !bspState['src.json']) {
    return null
  }

  return (
    <section>
      <div className="row">
        <header className="col-md-12">
          <h3 className='mb-4'>Bsp information</h3>
        </header>
      </div>
      <div className="col-md-12">
        <BspInfoTable state={state} />
      </div>
    </section>
  )
}
