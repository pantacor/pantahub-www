import React from 'react'
import { capitalize } from '../../../lib/utils'
import './style.scss'

export function PriceMetaData ({ meta = {} }) {
  if (meta === null) {
    return null
  }

  const filterMeta = Object.keys(meta).reduce((acc, key) => {
    if (key !== 'PUBLIC' && meta[key] !== 'false' && meta[key] !== '0GiB') {
      acc[key] = meta[key]
    }

    return acc
  }, {})
  return (
    <table className="price-metas">
      <tbody>
        {Object.keys(filterMeta).map((key) => (
          <tr key={key} className="price-meta">
            <td className='price-meta--label flex-fill'>
              {capitalize(key)}
            </td>
            <td className='price-meta--value flex-fill'>
              {meta[key]}
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  )
}
