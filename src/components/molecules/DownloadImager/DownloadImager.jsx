/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { TrackedButton } from '../../atoms/Tracker/Tracker'

const options = [
  {
    name: 'Select your OS'
  },
  {
    name: 'Download for Windows',
    link: 'https://downloads.raspberrypi.org/imager/imager_1.6.exe'
  },
  {
    name: 'Download for Mac OS',
    link: 'https://downloads.raspberrypi.org/imager/imager_1.6.dmg'
  },
  {
    name: 'Download for Linux (deb package)',
    link: 'https://downloads.raspberrypi.org/imager/imager_1.6_amd64.deb',
    extra: 'To run, download and extract the Zip file and double-click on the extracted file.'
  }
]

function getOS () {
  const userAgent = navigator.userAgent.toLowerCase()
  if (userAgent.indexOf('win') >= 0) return 'windows'
  if (userAgent.indexOf('mac') >= 0) return 'macos'
  if (userAgent.indexOf('linux') >= 0) return 'linux'
  return 'Unknown OS'
}

function defaultSelected () {
  switch (getOS()) {
    case 'windows':
      return 1
    case 'macos':
      return 2
    case 'linux':
      return 3
    default:
      return 0
  }
}

export default function DownloadImager () {
  const [open, setOpen] = React.useState(false)
  const anchorRef = React.useRef(null)
  const [selectedIndex] = React.useState(defaultSelected())

  const handleToggle = (event, name) => {
    if (!name || name === options[0].name) {
      setOpen((prevOpen) => !prevOpen)
    }
  }

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return
    }

    setOpen(false)
  }

  const closeOnClick = (event) => {
    setOpen(false)
  }

  React.useEffect(() => {
    document.addEventListener('mousedown', handleClose)
    return () => {
      document.removeEventListener('mousedown', handleClose)
    }
  }, [])

  return (
    <React.Fragment>
      <div id="download-pi-imager" className="btn-group" ref={anchorRef}>
        <a
          className="btn btn-info  btn-select"
          target="_blank"
          rel="noopener noreferrer"
          href={options[selectedIndex].link}
          onClick={(e) => handleToggle(e, options[selectedIndex].name)}
        >
          {options[selectedIndex].name}
        </a>
        <TrackedButton
          type="button"
          className="btn  btn-info dropdown-toggle dropdown-toggle-split"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded={open ? 'true' : 'false'}
          onClick={handleToggle}
        >
          <span className="sr-only">Toggle Dropdown</span>
        </TrackedButton>
        <div className={`dropdown-menu ${open ? 'show' : ''}`}>
          {options.map((option, index) => (
            <a
              key={option.name}
              className="dropdown-item"
              disabled={index === 0}
              target="_blank"
              rel="noopener noreferrer"
              href={option.link}
              onClick={closeOnClick}
            >
              {option.name}
            </a>
          ))}
        </div>
      </div>
      <div style={{ visibility: !options[selectedIndex].extra ? 'hidden' : '' }}>
        <br/>
        {options[selectedIndex].extra ? options[selectedIndex].extra : ''}
      </div>
    </React.Fragment>
  )
}
