import React, { useState } from 'react'
import { TrackedButton } from '../../atoms/Tracker/Tracker'

import './add-list.scss'

export default function AddList ({ onAdd, onUpdate, onRemove, values, fields = [], name, label }) {
  const [state, setState] = useState({})
  const [error, setError] = useState(null)

  const updateLocal = (field) => (event) => {
    const newValue = event.target.type === 'checkbox' ? event.target.checked : event.target.value
    setError(null)
    setState(s => ({ ...s, [field]: newValue }))
  }

  const onChange = (id, field) => (event) => {
    const newValue = event.target.type === 'checkbox' ? event.target.checked : event.target.value
    if (newValue === '') {
      return remove(id)(event)
    }
    if (!field) {
      values[id] = newValue
    } else {
      values[id][field] = newValue
    }

    onUpdate(values)
  }

  const addNew = (event) => {
    event.preventDefault()

    const anyEmpty = fields.some(f => {
      return !state[f.name] || state[f.name] === ''
    })
    if (anyEmpty) {
      setError('Field can\'t be empty')
      return
    }
    onAdd(state)
    setState({})
  }

  const remove = (id) => (event) => {
    event.preventDefault()
    onRemove(id)
  }

  return (
    <div className="col-12">
      <label htmlFor={name}>{label}</label>
      <div className="table-responsive">
        <section className="table table-borderless table-hover add-list">
          <header className="add-list__header row">
            <div className="add-list__header-container p-0 d-flex justify-content-between">
              {fields.map((f, id) => (
                <div key={id} className={f.className}>{f.label}</div>
              ))}
              <div>
                Actions
              </div>
            </div>
          </header>
          <section className="add-list__body">
            {values.map((v, i) => (
              <div key={i} className="row align-items-center" >
                {fields.map((f, j) => (
                  <div className={f.className} key={j}>
                    <input
                      name={`${f.name}-${f.id}`}
                      id={`${f.name}-${f.id}`}
                      type={`${f.type}`}
                      value={v[f.name] || v || ''}
                      onChange={onChange(i, f.name)}
                    />
                  </div>
                ))}
                <div className="col">
                  <div className="row justify-content-md-center">
                    <TrackedButton className="btn btn-danger" onClick={remove(i)} >
                      <i className="mdi mdi-delete-forever" />
                    </TrackedButton>
                  </div>
                </div>
              </div>
            ))}
            <div className="row align-items-stretch">
              {fields.map((f, j) => (
                <div className={f.className} key={j}>
                  <input
                    type={`${f.type}`}
                    value={state[f.name] || ''}
                    onChange={updateLocal(f.name)}
                    placeholder={f.placeholder}
                    className={`${error && 'is-invalid'}`}
                  />
                  <div className="invalid-feedback">{error}</div>
                </div>
              ))}
              <div className="col">
                <div className="row justify-content-md-center">
                  <a href="#addElement" className="btn btn-success" onClick={addNew}>
                    <i className="mdi mdi-plus" />
                  </a>
                </div>
              </div>
            </div>
          </section>
        </section>
      </div>
    </div>
  )
}
