/* eslint-disable no-console */
/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import Recaptcha from "react-google-recaptcha";

import Loading from "../../atoms/Loading/Loading";
import useFormValidation from "../../../hooks/useFormValidation";
import useCaptcha from "../../../hooks/useCaptcha";
import { parseImplicitData } from "../../../lib/api.helpers";
import OAuthButtons from "../OAuthButtons/OAuthButtons";
import {
  setUsername,
  setPassword,
  setEmail,
  signUp,
} from "../../../store/auth/actions";
import { TrackedButton } from "../../atoms/Tracker/Tracker";
import { Track } from "../../../lib/analytics.helper";
import { GetEnv } from "../../../lib/const.helpers";
import TextSeparator from "../../atoms/TextSeparator/TextSeparator";

function InnerSignUpForm(props) {
  const [encryptedAccount, setEncryptedAccount] = useState();
  const [loaded, setLoaded] = useState(false);
  const [captchaLoaded, setCaptchaLoaded] = useState(false);
  const {
    onChangeUsername,
    onChangeEmail,
    onChangePassword,
    onSignUp,
    username,
    email,
    password,
    signup,
    size = "default",
    labels = true,
  } = props;
  const { loading, response, error } = signup;
  const { formState, onSubmit, onInput, getError } = useFormValidation();
  const {
    captchaRef,
    captchaToken,
    onErroredCallback,
    verifyCallback,
    expiredCallback,
  } = useCaptcha();

  useEffect(() => {
    const account = parseImplicitData(window.location.hash).account;
    if (account && account !== "") {
      setEncryptedAccount(account);
    }

    const timer = setTimeout(() => {
      if (!loaded) {
        setLoaded(true);
      }
    }, 500);

    return () => {
      clearTimeout(timer);
    };
  }, []);

  const submit = (event) => {
    const { isValid } = onSubmit(event);
    if (isValid && (!!captchaToken || CAPTCHA_SITE_KEY === "")) {
      onSignUp(username, email, password, captchaToken, encryptedAccount);
    }
  };

  const wasValidatedClass = formState.wasValidated ? "was-validated" : null;

  if (response !== null) {
    Track("Action", { action: "signup-success" });

    return (
      <p key="thanks">
        Thanks for registering to use PantaHub! Your account request will be
        reviewed and you will get an email notification with the details to
        verify your account.
      </p>
    );
  }

  if (error) {
    Track("Action", { action: "signup-error" });
  }

  const CAPTCHA_SITE_KEY = GetEnv("REACT_APP_CAPTCHA_SITE_KEY", "");
  const disableEmailPasswordLogin =
    GetEnv("REACT_APP_DISABLE_EMAIL_PASSWORD_LOGIN", "false") === "true";

  const onCaptchaLoaded = () => {
    setCaptchaLoaded(true);
  };

  if (loading || !loaded) {
    return (
      <React.Fragment>
        <Loading key="loading" />
        {!captchaLoaded && CAPTCHA_SITE_KEY !== "" && (
          <React.Fragment>
            <Recaptcha
              sitekey={CAPTCHA_SITE_KEY}
              // type="explicit"
              ref={captchaRef}
              onErrored={onErroredCallback}
              onChange={verifyCallback}
              onExpired={expiredCallback}
              asyncScriptOnLoad={onCaptchaLoaded}
            />
            {!captchaToken && (
              <div className="invalid-feedback"> {getError("password")} </div>
            )}
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }

  return (
    <React.Fragment>
      {error && (
        <p key="errors" className="alert alert-danger">
          {error.Error || error.message || JSON.stringify(error)}
        </p>
      )}
      {disableEmailPasswordLogin && (
        <div className="mt-2em mb-2em">
          <p key="disableEmailPasswordLogin" className="text-danger">
            Account creation via email/password is disabled
          </p>
          <p>Please use one of the following options to create an account:</p>
        </div>
      )}
      {!disableEmailPasswordLogin && (
        <>
          <form
            key="form"
            onSubmit={submit}
            className={wasValidatedClass}
            noValidate
          >
            {(!encryptedAccount || error) && (
              <React.Fragment>
                <div className="form-group pb-2 pb-2">
                  {labels && <label htmlFor="username">Username</label>}
                  <input
                    type="text"
                    autoCapitalize="none"
                    className="form-control"
                    id="username"
                    name="username"
                    minLength="4"
                    maxLength="50"
                    pattern="[a-zA-Z0-9][a-zA-Z0-9_-]+[a-zA-Z0-9]"
                    data-pattern-error-message="Username may only contain alphanumeric characters, hyphens, and cannot begin or end with a hyphen. "
                    onInput={onInput}
                    onChange={onChangeUsername}
                    placeholder="Username"
                    required
                  />
                  <div className="invalid-feedback">
                    {" "}
                    {getError("username")}{" "}
                  </div>
                </div>
                <div className="form-group pb-2 pb-2">
                  {labels && <label htmlFor="email">Email address</label>}
                  <input
                    type="email"
                    className="form-control"
                    name="email"
                    id="email"
                    required
                    onInput={onInput}
                    onChange={onChangeEmail}
                    placeholder="me@example.com"
                  />
                  <div className="invalid-feedback"> {getError("email")} </div>
                </div>
                <div className="form-group pb-2 pb-2">
                  {labels && <label htmlFor="password">Password</label>}
                  <input
                    type="password"
                    id="password"
                    name="password"
                    required
                    onInput={onInput}
                    className="form-control"
                    onChange={onChangePassword}
                    placeholder="Password"
                  />
                  <div className="invalid-feedback">
                    {" "}
                    {getError("password")}{" "}
                  </div>
                </div>
              </React.Fragment>
            )}
            <div className="form-row d-flex justify-content-center pt-2 pb-4">
              {CAPTCHA_SITE_KEY !== "" && (
                <React.Fragment>
                  <Recaptcha
                    sitekey={CAPTCHA_SITE_KEY}
                    // type="explicit"
                    ref={captchaRef}
                    onErrored={onErroredCallback}
                    onChange={verifyCallback}
                    onExpired={expiredCallback}
                  />
                  {!captchaToken && (
                    <div className="invalid-feedback">
                      {" "}
                      {getError("password")}{" "}
                    </div>
                  )}
                </React.Fragment>
              )}
            </div>
            <div className="d-flex justify-content-end">
              <TrackedButton
                type="submit"
                event="Action"
                payload={{ action: "signup" }}
                disabled={!captchaToken && CAPTCHA_SITE_KEY !== ""}
                className="btn btn-primary btn-block"
              >
                Sign Up
              </TrackedButton>
            </div>
          </form>
          <TextSeparator />
        </>
      )}
      <OAuthButtons size={size} />
    </React.Fragment>
  );
}

export default connect(
  (state) => state.auth,
  (dispatch) => ({
    onChangeUsername: (evt) => dispatch(setUsername(evt.target.value)),
    onChangePassword: (evt) => dispatch(setPassword(evt.target.value)),
    onChangeEmail: (evt) => dispatch(setEmail(evt.target.value)),
    onSignUp: (nick, email, password, captchaToken, encrypted) =>
      dispatch(signUp(nick, email, password, captchaToken, encrypted)),
  }),
)(InnerSignUpForm);
