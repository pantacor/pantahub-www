import React from 'react'

import './download-image.scss'
import { connect } from 'react-redux'
import { DownloadImageWithAuth } from '../../../store/images/actions'
import { GetEnv } from '../../../lib/const.helpers'
import { TrackedButton } from '../../atoms/Tracker/Tracker'

class DownloadImageSelector extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      open: false
    }
    this.wrapperRef = React.createRef()
  }

  downloadImage = (device) => () => {
    this.props.DownloadImageWithAuth(device, GetEnv('REACT_APP_IMAGES_CI_CHANNEL'))
    this.setState({ open: false })
  }

  toggleSubMenu = (event) => {
    event.preventDefault()
    this.setState({ open: !this.state.open })
  }

  componentDidMount () {
    document.addEventListener('mousedown', this.handleClickOutside)
  }

  componentWillUnmount () {
    document.removeEventListener('mousedown', this.handleClickOutside)
  }

  handleClickOutside = (event) => {
    if (
      this.wrapperRef &&
      this.wrapperRef.current &&
      !this.wrapperRef.current.contains(event.target)
    ) {
      this.setState({ open: false })
    }
  }

  render () {
    const text = this.props.text || 'Select your device type'
    return (
      <div
        ref={this.wrapperRef}
        className="download-pantavisor dropdown"
      >
        <TrackedButton
          className="btn btn-info  dropdown-toggle"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded={this.state.open ? 'true' : 'false'}
          onClick={this.toggleSubMenu}
        >
          {text}
        </TrackedButton>
        <div className={`${this.state.open ? 'show' : ''} dropdown-menu`}>
          {this.props.images.map((image, id) => (
            <TrackedButton
              key={`${image.name}-${id}`}
              className="dropdown-item"
              onClick={this.downloadImage(image.target)}
            >
              {image.name}
              {image.type && image.type !== 'default' && (
                ` - ${image.type}`
              )}
            </TrackedButton>
          ))}
        </div>
      </div>
    )
  }
}

export default connect(undefined, {
  DownloadImageWithAuth
})(DownloadImageSelector)
