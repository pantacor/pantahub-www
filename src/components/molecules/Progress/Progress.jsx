/*
 * Copyright (c) 2023 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { GetFileSize } from '../../organisms/Files/FilesFromState'

export function Usage ({ value, max }) {
  const percentage = (value * 100) / max
  return (
    <div className="progress" style={{ height: '20px' }}>
      <div
        className="progress-bar"
        role="progressbar"
        aria-valuenow={GetFileSize(value).toString()}
        aria-valuemin="0"
        aria-valuemax={GetFileSize(max).toString()}
        style={{ width: `${percentage}%` }}
      >
        {percentage > 50 && (
          <>{GetFileSize(value).toString()}/{GetFileSize(max).toString()}</>
        )}
      </div>
      {percentage < 50 && (
        <div
          style={{ textAlign: 'center', width: `${100 - percentage}%` }}
        >
          {GetFileSize(value).toString()}/{GetFileSize(max).toString()}
        </div>
      )}
    </div>
  )
}
