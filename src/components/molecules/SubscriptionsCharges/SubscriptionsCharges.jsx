import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { resolvePath } from '../../../lib/object.helpers'
import { subscriptionsPath } from '../../../router/routes'
import { initializeDashboard } from '../../../store/dashboard/actions'
import { Breadcumbs } from '../../atoms/Breadcumbs/Breadcumbs'
import Loading from '../../atoms/Loading/Loading'
import { SubscriptionDetail } from '../../organisms/SubscriptionDetail/SubscriptionDetail'

export function SubscriptionsCharges () {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(initializeDashboard())
  }, [])

  const data = useSelector((state) => ({
    subscription: state.billing.subscriptions.current
  }))

  let productName = resolvePath(data, 'subscription.product.name', 'Free')
  if (productName === '') {
    productName = 'Free'
  }

  if (resolvePath(data, 'subscription.meta', []).length === 0) {
    return (
      <div
        style={{ height: '70vh' }}
        className='d-flex flex-column justify-content-center align-items-center'
      >
        <Loading />
        <h6 className='text-center'>
          Loading your subscription data, please wait...
        </h6>
      </div>
    )
  }

  return (
    <section>
      <header className='content-header'>
        <Breadcumbs
          steps={[
            {
              to: subscriptionsPath,
              title: 'Subscription'
            },
            {
              title: 'Details'
            }
          ]}
        />
      </header>
      <SubscriptionDetail />
    </section>
  )
}
