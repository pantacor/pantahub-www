import React from 'react'
import { resolvePath } from '../../../lib/object.helpers'
import { Money } from '../../atoms/Money/Money'

import { PriceMetaData } from '../PriceMetaData/PriceMetaData'

import '../PriceSelector/priceselector.scss'

export function PriceDetails ({ price }) {
  if (!price) {
    return null
  }

  return (
    <div
      className='price-selector d-flex align-items-stretch flex-column'
    >
      <div className='price-selector--border-top-primary'></div>
      <header>
        <h3>{resolvePath(price, 'product.name', 'Free')}</h3>
        <p>
          {resolvePath(price, 'product.description', '')}
        </p>
      </header>
      <div className='price-content'>
        <h3 className='price-amount'>
          <Money amount={price.unit_amount} currency={price.currency} />
          <small>/month</small>
        </h3>
        <div>
          <PriceMetaData meta={resolvePath(price, 'product.metadata', {})} />
        </div>
      </div>
    </div>
  )
}
