import React, { useRef } from 'react'
import isEqual from 'lodash.isequal'

import './add-form-list.scss'
import { TrackedButton } from '../../atoms/Tracker/Tracker'

const defaultOptionsMap = { getValue: (o) => o, getLabel: (o) => o }

function Inner (props) {
  const {
    name,
    label,
    onAdd = () => {},
    onRemove = () => {},
    onUpdate = () => {},
    getId = (v) => v,
    options = [],
    values = [],
    optionsMap = {}
  } = props

  const option = useRef()
  const removeId = (id) => (event) => {
    event.preventDefault()
    onRemove(id)
  }
  const updatePart = (id, part) => (event) => {
    const newValue = event.target.type === 'checkbox' ? event.target.checked : event.target.value
    onUpdate(id, { ...values[id], [part]: newValue })
  }
  const addOption = (event) => {
    event.preventDefault()
    if (!values.some(v => isEqual(getId(v), getId(options[option.current.value])))) {
      onAdd(options[option.current.value])
    }
  }
  const mapper = {
    ...defaultOptionsMap,
    ...optionsMap
  }

  return (
    <div className="add-form-list">
      <div className="header row">
        <label htmlFor={name} style={{ marginLeft: '10px' }}>{label}</label>
        <div className="col-11">
          <select
            id={name}
            name={name}
            ref={option}
            className="form-control"
          >
            {options.map((o, id) => (
              <option key={id} value={id}>{mapper.getLabel(o)}</option>
            ))}
          </select>
        </div>
        <div className="col-1 d-flex justify-content-center">
          <TrackedButton
            className="btn btn-success d-flex justify-content-center"
            onClick={addOption}
          >
            <i className="mdi mdi-plus"/>
          </TrackedButton>
        </div>
      </div>
      { values.length > 0 && (
        <div className="body row">
          <div className="col-12">
            <div className="list-header row">
              <div className={props.always_required ? 'col-11 text-left name' : 'col-9 text-left name'}>
                <b style={{ marginLeft: '10px' }}>Name</b>
              </div>
              {!props.always_required && (
                <div className="col-2 text-center default">
                  <b>Required</b>
                </div>
              )}
              <div
                className="col-1 text-center actions"
              >
                {' '}
              </div>
            </div>
          </div>
          {values.map((v, id) => (
            <div key={id} className="header col-12 pt-2">
              <div className="row align-items-center">
                <div className={props.always_required ? 'col-11' : 'col-9'}>
                  <span style={{ marginLeft: '10px' }}>{v.description} ({v.service}/{v.id})</span>
                </div>
                {!props.always_required && (
                  <div className="col-2 text-center">
                    <input
                      type="checkbox"
                      id={`required$-${id}`}
                      className="form-checkbox"
                      defaultChecked={v.required}
                      onChange={updatePart(id, 'required')}
                    />
                  </div>
                )}
                <div className="col-1 d-flex justify-content-center">
                  <a
                    href="#removeScope"
                    onClick={removeId(id)}
                    className="btn btn-danger text-center">
                    <i className="mdi mdi-delete-forever" />
                  </a>
                </div>
              </div>
            </div>
          ))}
        </div>
      )}
    </div>
  )
}

export default function AddFromList (props) {
  const {
    label = '',
    name
  } = props
  return label > 0
    ? (
      <div className="form-group pb-2">
        <label htmlFor={name}>{label}</label>
        <Inner {...props} />
      </div>
    )
    : (
      <React.Fragment>
        <Inner {...props} />
      </React.Fragment>
    )
}
