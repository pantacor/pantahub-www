import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { resolvePath } from '../../../lib/object.helpers'
import { CreateSubscriptions } from '../../../services/billing.service'
import LoadingButton from '../../atoms/LoadingButton/LoadingButton'
import {
  GetSubscriptionsAction,
  updateSubscriptionActions
} from '../../../store/billing/actions'
import {
  paymentConfirmPath,
  subscriptionsNewPath
} from '../../../router/routes'
import { useHistory } from 'react-router-dom'
import './priceselector.scss'
import { TrackedButton } from '../../atoms/Tracker/Tracker'
import { Money } from '../../atoms/Money/Money'
import { Path } from 'path-parser'
import { PriceFeatures } from '../PriceFeatures/PriceFeatures'
import RenderIf from '../../atoms/RenderIf/RenderIf'
import { PriceMetaData } from '../PriceMetaData/PriceMetaData'

const paymentDowngradeUrl = Path.createPath(paymentConfirmPath)

const baseColor = 0x2cd8b1
const finalColor = 0X4401ff

const colorHexString = (intColor) => {
  return `#${intColor.toString(16)}`
}

const colorIntIntoRgb = (int) => {
  return { r: (int & 0xFF0000) >> 16, g: (int & 0xFF00) >> 8, b: int & 0xFF }
}

const colorRGBToString = ({ r = 0, g = 0, b = 0 }) => {
  return colorHexString((r << 16) + (g << 8) + b)
}

const applyRelative = (from, to, relative) => {
  let diff = from + Math.round((to - from) * relative)
  if (diff < 0) {
    diff = 255 - diff
  }
  if (diff > 255) {
    diff = diff - 255
  }
  return diff
}

export const moveColor = (color, finalColor, amount, max) => {
  // debugger // eslint-disable-line
  const from = colorIntIntoRgb(color)
  const to = colorIntIntoRgb(finalColor)
  const relative = Math.max(0, amount) / max
  const relativeR = applyRelative(from.r, to.r, relative)
  const relativeG = applyRelative(from.g, to.g, relative)
  const relativeB = applyRelative(from.b, to.b, relative)

  return { r: relativeR, g: relativeG, b: relativeB }
}

const Loading = ({ size = '10px', style, className, ...extra }) => {
  return (
    <div
      style={{ ...style, height: size, width: size }}
      className={`spinner-border ${className}`}
      {...extra}
    />
  )
}

export function PriceSelector ({ price, currentPrice = {}, showSubmit = true, setMessage, pricesLength }) {
  const [loading, setLoading] = useState(false)
  const dispatch = useDispatch()
  const history = useHistory()
  const data = useSelector((state) => ({
    token: state.auth.token,
    customerID: state.billing.customer.id
  }))

  const createSubscription = async (e) => {
    if (currentPrice.unit_amount) {
      history.push(paymentDowngradeUrl.build({
        paymentid: price.id
      }))
      return
    }

    setLoading(true)
    const response = await CreateSubscriptions(data.token, {
      'customer_id': data.customerID,
      'price_id': price.id
    })

    if (!response.ok) {
      dispatch(updateSubscriptionActions.failure(response.json))
      setMessage(`Something whent wrong, please contact the support team support@pantacor.com`, 'error')
    }

    if (response.ok) {
      dispatch(updateSubscriptionActions.success(response.json))
      dispatch(GetSubscriptionsAction())

      history.push(`${subscriptionsNewPath}/${response.json.id}`)
    }

    setLoading(false)
  }

  if (!pricesLength) {
    pricesLength = 1
  }
  const index = price.index
  const bgColorStart = colorRGBToString(moveColor(baseColor, finalColor, index - 1, pricesLength))
  const bgColorFinish = colorRGBToString(moveColor(baseColor, finalColor, index, pricesLength))

  return (
    <div
      className='price-selector d-flex align-items-stretch flex-column'
    >
      <div
        style={{
          borderRadius: '4px 4px 0 0',
          height: '10px',
          background: `linear-gradient(90deg, ${bgColorStart} 0%, ${bgColorFinish} 100%)`
        }}
      >
      </div>
      <header>
        <h4>{resolvePath(price, 'product.name', 'Free')}</h4>
        <p>
          {resolvePath(price, 'product.description', '')}
        </p>
      </header>
      <div className='price-content'>
        <h3 className='price-amount'>
          <Money amount={price.unit_amount} currency={price.currency} />
          <small>
            /{resolvePath(price, 'recurring.interval', 'month')}
          </small>
        </h3>
        <div>
          <RenderIf condition={resolvePath(price, 'product.features', []).length > 0}>
            <PriceFeatures features={resolvePath(price, 'product.features', [])} />
          </RenderIf>
          <RenderIf condition={resolvePath(price, 'product.features', []).length === 0}>
            <PriceMetaData meta={resolvePath(price, 'product.metadata', [])} />
          </RenderIf>
        </div>
        <div
          className='d-flex justify-content-center align-items-center submit-area'
        >
          <SubmitButton
            showSubmit={showSubmit}
            currentPrice={currentPrice.id}
            price={price}
            createSubscription={createSubscription}
            loading={loading}
          />
        </div>
      </div>
    </div>
  )
}

function SubmitButton ({ showSubmit, currentPrice, price, createSubscription, loading }) {
  if (!showSubmit) {
    return null
  }
  if (currentPrice === price.id || price.active) {
    return (
      <TrackedButton
        disabled={true}
        className='btn btn-secondary'
      >
        Current Plan
      </TrackedButton>
    )
  }
  return (
    <LoadingButton
      loadingChild={() => (<Loading style={{ color: 'white' }} size='1em' />)}
      loading={loading}
      onClick={createSubscription}
      className='btn btn-primary'
    >
      Choose plan
    </LoadingButton>
  )
}
