/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { Component } from 'react'

import { connect } from 'react-redux'

import { NavLink, withRouter } from 'react-router-dom'
import { logout } from '../../../store/auth/actions'
import { navToggle } from '../../../store/nav/actions'
import * as routes from '../../../router/routes'

class SidebarNav extends Component {
  constructor (props) {
    super(props)

    this.state = {
      open: false
    }
    this.wrapperRef = React.createRef()
  }

  componentDidMount () {
    document.addEventListener('mousedown', this.handleClickOutside)
  }

  componentWillUnmount () {
    document.removeEventListener('mousedown', this.handleClickOutside)
  }

  handleClickOutside = (event) => {
    if (
      this.wrapperRef &&
      this.wrapperRef.current &&
      !this.wrapperRef.current.contains(event.target)
    ) {
      this.setState({ open: false })
    }
  }

  toggleDowndown = () => {
    this.setState({ open: !this.state.open })
  }

  render () {
    const { auth, nav } = this.props
    const { username, token } = auth
    const { show } = nav
    const loggedIn = token !== null

    if (!loggedIn) {
      return null
    }

    return (
      <div className="main-nav-sidebar">
        <div
          className={`navigation-main ${
            show ? 'show' : ''
          }`}
        >
          {loggedIn && (
            <ul className='navbar-nav ml-auto'>
              <div className='d-block'>
                <li className='nav-item'>
                  <NavLink
                    exact
                    className='nav-link'
                    to={`${routes.userDashboardPath}`}
                  >
                    Dashboard
                  </NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink
                    className='nav-link'
                    to={`${routes.userDashboardPath}/${username}/devices`}
                  >
                    Devices
                  </NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink
                    className='nav-link'
                    to={`${routes.userDashboardPath}/apps`}
                  >
                    Applications
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    exact
                    className="nav-link"
                    to={routes.globalConfig}
                  >
                    Configuration
                  </NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink
                    exact
                    className='nav-link'
                    to={'/download-image'}
                  >
                    Download Image
                  </NavLink>
                </li>
              </div>
            </ul>
          )}
          {!loggedIn && (
            <ul className='navbar-nav ml-auto navbar-profile'>
              <li className='nav-item'>
                <NavLink
                  className='nav-link nav-link-account'
                  to={routes.loginPath}
                >
                  Log In
                </NavLink>
              </li>
              <li className='nav-item'>
                <NavLink to={routes.signUpPath} className='nav-link'>
                  Sign Up for Free
                </NavLink>
              </li>
            </ul>
          )}
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    profile: state.profile,
    auth: state.auth,
    nav: state.nav
  }),
  {
    logout,
    navToggle
  }
)(withRouter(SidebarNav))
