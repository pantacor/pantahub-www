/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { PureComponent } from "react";

import { connect } from "react-redux";

import { NavLink, withRouter } from "react-router-dom";

import { logout } from "../../../store/auth/actions";
import { navToggle } from "../../../store/nav/actions";
import * as routes from "../../../router/routes";
import { TrackedButton } from "../../atoms/Tracker/Tracker";
import { GetEnv } from "../../../lib/const.helpers";
import { tabs } from "../../organisms/Navigator/index";

class HeaderNav extends PureComponent {
	constructor(props) {
		super(props);

		this.state = {
			open: false,
		};
		this.wrapperRef = React.createRef();
	}

	componentDidMount() {
		document.addEventListener("mousedown", this.handleClickOutside);
	}

	componentWillUnmount() {
		document.removeEventListener("mousedown", this.handleClickOutside);
	}

	handleClickOutside = (event) => {
		if (
			this.wrapperRef &&
			this.wrapperRef.current &&
			!this.wrapperRef.current.contains(event.target)
		) {
			this.setState({ open: false });
		}
	};

	toggleDowndown = () => {
		this.setState({ open: !this.state.open });
	};

	render() {
		const { auth, profile, nav, onLogout, onToggle } = this.props;
		const { username, token } = auth;
		const { show } = nav;
		const loggedIn = token !== null;
		// const headerInclinitation = (this.props.location.pathname.replace('/', '') !== '') || isNative()
		//   ? 'bg-color bg-color__small-right'
		//   : 'bg-color bg-color__small-right'

		const logoSrc =
			loggedIn && GetEnv("REACT_APP_HOSTED_LOGO", "") !== ""
				? GetEnv("REACT_APP_HOSTED_LOGO", "")
				: GetEnv("REACT_APP_MAIN_LOGO", "") !== ""
					? GetEnv("REACT_APP_MAIN_LOGO", "")
					: GetEnv("PUBLIC_URL") + "/pantacor_logo.svg";

		return (
			<nav id="navbar-main" className={"navbar navbar-expand-lg"}>
				<div className="container-fluid">
					<NavLink className="navbar-brand" to="/">
						<img alt="PantacorHub" src={logoSrc} height="34" />
					</NavLink>
					<TrackedButton
						className="navbar-toggler btn-primary"
						type="button"
						aria-expanded={show}
						aria-label="Toggle navigation"
						onClick={onToggle}
					>
						<span className="navbar-toggler-icon">
							<i className="mdi mdi-menu" />
						</span>
					</TrackedButton>

					<div
						className={`collapse navbar-collapse navigation-main ${
							show ? "show" : ""
						}`}
					>
						{loggedIn && (
							<ul className="navbar-nav ml-auto navbar-profile">
								<li
									className="nav-item dropdown"
									ref={this.wrapperRef}
								>
									<TrackedButton
										className="btn btn-link nav-link dropdown-toggle"
										onClick={this.toggleDowndown}
									>
										{profile.current.picture &&
											profile.current.picture !== "" && (
												<img
													height={40}
													alt={`${username} profile`}
													style={{
														borderRadius: 100,
														marginRight: "5px",
													}}
													src={
														profile.current.picture
													}
												/>
											)}
										<span style={{ marginRight: "5px" }}>
											{username}
										</span>
									</TrackedButton>
									<div className="d-none d-sm-block ">
										<div
											className={`dropdown-menu ${this.state.open ? "show" : ""}`}
											aria-labelledby="navbarDropdown"
											onClick={this.toggleDowndown}
										>
											<NavLink
												exact
												className="dropdown-item"
												to={routes.editProfile}
											>
												Profile
											</NavLink>
											<NavLink
												exact
												className="dropdown-item"
												to={routes.tokens}
											>
												Personal Tokens
											</NavLink>
											{GetEnv(
												"REACT_APP_BILLING",
												"false",
											) === "true" && (
												<NavLink
													exact
													className="dropdown-item"
													to={
														routes.subscriptionsPath
													}
												>
													Subscription
												</NavLink>
											)}
											<hr />
											<TrackedButton
												onClick={onLogout}
												className="dropdown-item"
											>
												Logout
											</TrackedButton>
										</div>
									</div>
								</li>
								<div className="d-block d-sm-none">
									<li className="nav-item">
										<NavLink
											className="nav-link nav-link-account"
											to={`${routes.userDashboardPath}/${username}`}
										>
											Dashboard
										</NavLink>
									</li>
									<li className="nav-item">
										<NavLink
											className="nav-link"
											to={`${routes.userDashboardPath}/${username}/devices`}
										>
											Devices
										</NavLink>
										{this.props.match.path ===
											"/u/:username/devices/:deviceId" && (
											<ul className="nav flex-column sub-menu">
												{tabs.map((tab) => (
													<li
														className="nav-item"
														key={tab.id}
													>
														<NavLink
															exact={tab.exact}
															to={`${this.props.match.url}${tab.id}`}
															className="btn btn-link btn-block nav-link"
															activeClassName="active"
															id={`tab-${tab.id}`}
															event="pageview"
															payload={{
																"Page Path":
																	"user/device/tab-navigation",
																value: `${tab.id}`,
															}}
															aria-controls={`tab-${tab.id}`}
														>
															{tab.label}
														</NavLink>
													</li>
												))}
											</ul>
										)}
									</li>
									<div className="dropdown-divider"></div>
									<li className="nav-item">
										<NavLink
											className="nav-link"
											to={`${routes.userDashboardPath}/apps`}
										>
											Applications
										</NavLink>
									</li>
									<li className="nav-item">
										<NavLink
											exact
											className="nav-link"
											to={routes.editProfile}
										>
											Profile
										</NavLink>
									</li>
									<li className="nav-item">
										<NavLink
											exact
											className="nav-link"
											to={routes.globalConfig}
										>
											Configuration
										</NavLink>
									</li>
									<li className="nav-item">
										<NavLink
											exact
											className="nav-link"
											to={"/download-image"}
										>
											Download Image
										</NavLink>
									</li>
									<li className="nav-item">
										<TrackedButton
											onClick={onLogout}
											className="btn btn-link nav-link"
										>
											Logout
										</TrackedButton>
									</li>
								</div>
							</ul>
						)}
						{!loggedIn && (
							<ul className="navbar-nav ml-auto navbar-profile">
								<li className="nav-item">
									<NavLink
										className="nav-link nav-link-account"
										to={routes.loginPath}
									>
										Log In
									</NavLink>
								</li>
								<li className="nav-item">
									<NavLink
										to={routes.signUpPath}
										className="nav-link"
									>
										Sign Up for Free
									</NavLink>
								</li>
							</ul>
						)}
					</div>
				</div>
			</nav>
		);
	}
}

export default connect(
	(state) => ({
		profile: state.profile,
		auth: state.auth,
		nav: state.nav,
		location: state.router.location,
	}),
	(dispatch) => ({
		onLogout: () => dispatch(logout()),
		onToggle: () => dispatch(navToggle()),
	}),
)(withRouter(HeaderNav));
