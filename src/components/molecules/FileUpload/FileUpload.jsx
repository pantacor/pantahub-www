/*
 * Copyright (c) 2024 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { useState, useRef } from "react";
import DoneIcon from "@mui/icons-material/Done";
import Loading from "../../atoms/Loading/Loading";
import FileInput from "../../atoms/FileInput/FileInput";

import "./styles.scss";

export default function FileUpload(props) {
	const {
		onChange,
		value,
		message,
		style = {},
		pictureStyle = {},
		onFile = () => {},
		...rest
	} = props;

	const fileUpload = useRef();
	const [state, setState] = useState({
		error: null,
		dragOver: false,
		cropping: false,
		bin: value ? [value] : [],
		files: {},
		uploading: false,
		uploaded: 0,
	});

	const openUploadDialog = (event) => {
		event.preventDefault();
		if (fileUpload.current) {
			fileUpload.current.click();
		}
	};

	const onDragOver = (event) => {
		event.preventDefault();
	};

	const onDragleave = (event) => {
		event.preventDefault();
		setState({ ...state, dragOver: false });
	};

	const onDragStart = (event, id) => {
		event.preventDefault();
		setState({ ...state, dragOver: true });
	};

	const processFile = async (file, bin) => {
		const resp = await onFile(bin);
		let newState = {};
		if (!resp) {
			newState = {
				uploading: false,
				error: "onFile didn't respond anything",
				bin: null,
			};
		} else {
			newState = {
				uploading: false,
				error: resp.error,
				bin: null,
			};
		}

		setState((s) => ({
			...s,
			...newState,
			files: {
				...state.files,
				[file.name]: {
					...state.files[file.name],
					uploaded: true,
				},
			},
			bin: [...s.bin, bin],
		}));
	};

	const onFileHandler = () => {
		const files = fileUpload.current.files;
		if (files.length === 0) {
			setState({
				...state,
				error: "Please drop a file",
				dragOver: false,
				bin: null,
			});

			return;
		}

		const filesState = {};
		for (let i = 0; i < files.length; i++) {
			const file = files[i];
			if (!file || typeof file === "function") {
				break;
			}

			filesState[file.name] = file;
			const reader = new window.FileReader();
			reader.onload = function () {
				return processFile(file, this.result);
			};
			reader.readAsArrayBuffer(file);
		}

		setState((s) => ({
			...s,
			dragOver: false,
			uploading: true,
			uploaded: 0,
			files: {
				...state.files,
				...filesState,
			},
		}));
	};

	const onDrop = (event) => {
		event.preventDefault();
		const dt = event.dataTransfer;
		const files = dt.files;
		const newEvent = new window.Event("change");
		fileUpload.current.files = files;
		fileUpload.current.dispatchEvent(newEvent);
	};

	const finished = Object.keys(state.files).reduce((acc, key) => {
		return acc && state.files[key].uploaded;
	}, true);

	if (!finished) {
		return (
			<div
				className="d-flex justify-content-center align-items-center flex-column"
				style={pictureStyle}
			>
				<h6>Uploading files</h6>
				<ul className="list-group" style={{ minWidth: "50vw" }}>
					{Object.keys(state.files).map((k) => (
						<li
							key={k}
							className="list-group-item d-flex d-flex justify-content-between"
						>
							{k}
							{state.files[k].uploaded ? (
								<DoneIcon />
							) : (
								<Loading />
							)}
						</li>
					))}
				</ul>
			</div>
		);
	}

	return (
		<React.Fragment>
			<FileInput
				{...rest}
				ref={fileUpload}
				error={state.error}
				onChange={onFileHandler}
				className="invisible"
				style={{ height: 0 }}
				multiple
			/>
			{!state.cropping && (
				<React.Fragment>
					<div
						draggable
						style={{ ...style }}
						className={`drap-drop-area ${state.dragOver ? "highlight" : ""}`}
						onDragOver={onDragOver}
						onDragStart={onDragStart}
						onDragEnter={onDragStart}
						onDragLeave={onDragleave}
						onDrop={onDrop}
					>
						<div onClick={openUploadDialog} style={pictureStyle}>
							{message || "Drag your logo image here"}
						</div>

						{state.error && (
							<p className="text-danger">{state.error}</p>
						)}
					</div>
				</React.Fragment>
			)}
		</React.Fragment>
	);
}
