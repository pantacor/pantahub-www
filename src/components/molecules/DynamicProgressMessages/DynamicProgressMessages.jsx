/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'
import { resolvePath } from '../../../lib/object.helpers'
import { DeviceStatus, DeviceStatusMessage } from '../../pages/UserDevice/DeviceStatus'
import RenderIf from '../../atoms/RenderIf/RenderIf'

dayjs.extend(relativeTime)

const defaultTotal = {
  object_name: 'total',
  object_id: 'none',
  total_size: 0,
  start_time: 0,
  current_time: 0,
  total_downloaded: 0
}

const listStyle = {
  listStyle: 'none',
  paddingLeft: 0,
  marginLeft: 0,
  opacity: '0.7',
  fontSize: '0.8em'
}

const getDownloadData = (download = defaultTotal) => {
  const currentTime = dayjs()
  const startTime = dayjs.unix(download.start_time)
  const downloaded = download.total_downloaded / (1024 * 1024)
  const size = download.total_size / (1024 * 1024)
  const timepassed = (
    download.current_time > 0 &&
    download.start_time > 0
  )
    ? currentTime.diff(startTime, 'second')
    : 0

  const downloadSpeed = timepassed <= 0
    ? 0
    : downloaded / timepassed

  const eta = downloadSpeed > 0
    ? (size - downloaded) / downloadSpeed
    : -1

  return {
    ...download,
    eta,
    downloaded: downloaded,
    size: size,
    speed: downloadSpeed
  }
}

function Eta ({ eta }) {
  if (eta === -1) {
    return (
      <span>
        Time remaining: unknown
      </span>
    )
  }

  const now = dayjs()
  const finished = now.add(eta, 'second')
  // onMinutes = (onMinutes / 60).toFixed(2)
  //       Time remaining: {onMinutes < 1 ? eta.toFixed(2) : onMinutes} {onMinutes < 1 ? 'seconds' : 'minutes'}
  return (
    <span>
      Time remaining: {now.to(finished, true)}
    </span>
  )
}

function Speed ({ speed }) {
  return (
    <span>
      Download Speed: {speed.toFixed(2)} MB/s
    </span>
  )
}

function Progress ({ downloaded, size }) {
  const progress = ((downloaded / size) * 100).toFixed(2)
  const text = `Downloaded ${downloaded.toFixed(2)} MB of ${size.toFixed(2)} MB`
  return (
    <div className="progress-with-label">
      <span className="progress-with-label--label">
        {text}
      </span>
      <div className="progress" style={{ height: '20px' }}>
        <div
          className="progress-bar"
          role="progressbar"
          style={{ width: `${progress}%` }}
          aria-valuenow={progress}
          aria-valuemin="0"
          aria-valuemax="100"
        ></div>
      </div>
    </div>
  )
}

function Message ({ progress, statusMsg, status }) {
  const download = getDownloadData(resolvePath(progress, 'downloads.total', defaultTotal))

  if (progress.status === 'DOWNLOADING' && download.eta === -1) {
    return (
      <div style={{ opacity: '0.7', fontSize: '0.8em' }}>
        Starting download process
      </div>
    )
  }

  if (progress.status === 'DOWNLOADING' && download.eta >= 0) {
    return (
      <ul className="list-no-bullets list-unstyled" style={listStyle}>
        <li style={{ paddingTop: '1em', paddingBottom: '1em' }}>
          <Progress {...download} />
        </li>
        <li><Eta {...download} /></li>
        <li><Speed {...download} /> </li>
      </ul>
    )
  }

  if (progress.status === 'WRONG' || progress.status === 'ERROR') {
    return (
      <div style={{ opacity: '0.7', fontSize: '0.8em', marginTop: '0.2em' }}>
        <DeviceStatusMessage status={status} statusMsg={statusMsg} />
      </div>
    )
  }

  if (progress.status !== 'UPDATED' && progress.status !== 'DONE') {
    return (
      <div style={{ opacity: '0.7', fontSize: '0.8em', marginTop: '0.2em' }}>
        <DeviceStatusMessage status={status} statusMsg={statusMsg} />
      </div>
    )
  }

  return null
}

function Title ({ progress, status }) {
  if (progress.status === 'DOWNLOADING') {
    return (
      <div>
        <RenderIf condition={status !== null}>
          <DeviceStatus status={status} />&nbsp;
          <span style={{ opacity: '0.7', fontSize: '0.8em' }}>
            {resolvePath(progress, 'progress', 0)}%
          </span>
        </RenderIf>
      </div>
    )
  }

  return (
    <div>
      <RenderIf condition={status !== null}>
        <DeviceStatus status={status} />
      </RenderIf>
    </div>
  )
}

export default function DynamicProgressMessages (props) {
  return (
    <div className="downloading-rev-progress">
      <div className="downloading-rev-progress--inner">
        <Title
          progress={resolvePath(props, 'currentStep.progress', {})}
          status={props.status}
          statusMsg={props.statusMsg}
        />
        <Message
          progress={resolvePath(props, 'currentStep.progress', {})}
          status={props.status}
          statusMsg={props.statusMsg}
        />
      </div>
    </div>
  )
}
