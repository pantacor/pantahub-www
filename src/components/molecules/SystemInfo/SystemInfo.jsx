/*
 * Copyright (c) 2023 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { Usage } from '../Progress/Progress'
import RenderIf from '../../atoms/RenderIf/RenderIf'

export function SystemInfo ({ info, ...extras }) {
  return (
    <div {...extras}>
      <RenderIf condition={info.totalram && info.freeram}>
        <section className="col-md-6">
          <h6 className="fw-light mb-3">Ram</h6>
          <Usage value={info.totalram - info.freeram} max={info.totalram} />
        </section>
      </RenderIf>
      <RenderIf condition={info.totalswap && info.freeswap}>
        <section className="col-md-6">
          <h6 className="fw-light mb-3">Swap</h6>
          <Usage value={info.totalswap - info.freeswap} max={info.totalswap} />
        </section>
      </RenderIf>
    </div>
  )
}
