/*
 * Copyright (c) 2017-2023 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { Fragment, useState } from "react";
import WidgetsIcon from "@mui/icons-material/Widgets";
import FolderIcon from "@mui/icons-material/Folder";
import DataObjectIcon from "@mui/icons-material/DataObject";
import ShieldIcon from "@mui/icons-material/Shield";
import AttachFileIcon from "@mui/icons-material/AttachFile";
import { TYPES } from "../../../lib/state";
import { JSONDisplayWithClipboard } from "../../atoms/ClipboardFields/ClipboardFields";

import "./partdescription.scss";
import { ConfirmAction } from "../../atoms/ConfirmAction/ConfirmAction";

const mergeContentDescription = (desc = {}) => {
	if ([TYPES.JSON, TYPES.OBJECT].some((v) => v === desc.type)) {
		return desc.content;
	}

	return Object.keys(desc.content).reduce((acc, key = "") => {
		const d = desc.content[key];
		if (d.type === TYPES.FOLDER) {
			return { ...acc, ...mergeContentDescription(d) };
		}

		switch (d.type) {
			case TYPES.FOLDER:
				return { ...acc, ...mergeContentDescription(d) };
			case TYPES.SIGNATURE:
				return { ...acc, [d.id]: d.raw };
			default:
				return { ...acc, [d.id]: d.content };
		}
	}, {});
};

function AcordionItem({ pkg, title, name, onDelete, onSelect, children }) {
	const [show, setShow] = useState(false);

	const toggleShow = (evnt) => {
		evnt.preventDefault();
		setShow(!show);
	};

	const onSelectHandler = (evnt) => {
		if (!!onSelect) {
			onSelect(evnt.target.checked);
		}
	};

	let id = "";
	if (pkg.id.length > 0) {
		id = window.btoa(encodeURIComponent(pkg.id));
	}

	return (
		<div className="accordion-item">
			<div className="accordion-header">
				<button
					className={`accordion-button ${!show ? "collapsed" : ""}`}
					type="button"
					onClick={toggleShow}
				>
					{typeof title === "string" ? title : title()}
				</button>
				{onSelect && (
					<div className="pl-1 pr-1 d-flex">
						<input
							type="checkbox"
							name={id}
							id={id}
							onChange={onSelectHandler}
						/>
					</div>
				)}
				{onDelete && (
					<ConfirmAction
						title={`Delete ${name || title}`}
						confirmBtnTxt="Delete this"
						onConfirm={onDelete}
						btnClassName={`btn btn-sm btn-light btn-link ${!show ? "collapsed" : ""}`}
					>
						You are going to delete <b>{name || title}</b> from your
						device.
					</ConfirmAction>
				)}
			</div>
			<div
				className={`accordion-collapse collapse ${show ? "show" : ""}`}
			>
				<div className="accordion-body">{children}</div>
			</div>
		</div>
	);
}

function TitleIcon({ type }) {
	switch (type) {
		case TYPES.FOLDER:
			return <FolderIcon />;
		case TYPES.JSON:
			return <DataObjectIcon />;
		case TYPES.OBJECT:
			return <AttachFileIcon />;
		case TYPES.SIGNATURE:
			return <ShieldIcon />;
		case TYPES.PACKAGE:
			return <WidgetsIcon />;
		default:
			return null;
	}
}
function Title({ desc, title }) {
	return (
		<>
			<span style={{ paddingRight: "10px" }}>
				<TitleIcon type={desc.type} />
			</span>
			{title || desc.id}
		</>
	);
}

function Part({ title, desc, onDelete = () => null, onSelect = () => null }) {
	return (
		<AcordionItem
			onDelete={onDelete(desc.id)}
			onSelect={onSelect(desc.id)}
			name={desc.id}
			pkg={desc}
			title={() => <Title desc={desc} title={title} />}
		>
			<JSONDisplayWithClipboard value={mergeContentDescription(desc)} />
		</AcordionItem>
	);
}

function Parts({ desc, onDelete, onSelect }) {
	if (desc.type === TYPES.FOLDER) {
		return <Folder desc={desc} onDelete={onDelete} onSelect={onSelect} />;
	}

	return (
		<Part
			title={desc.id}
			desc={desc}
			onDelete={onDelete}
			onSelect={onSelect}
		/>
	);
}

export function Folder({
	root = false,
	desc,
	onDelete = () => null,
	onSelect = () => null,
}) {
	const Wrapper = root ? AcordionItem : Fragment;
	const props = {};
	if (root) {
		Object.assign(props, {
			onDelete: onDelete(desc.id),
			onSelect: onSelect(desc.id),
			name: desc.id,
			pkg: desc,
			title: function TitleOfFolder() {
				return <Title desc={desc} title={desc.id} />;
			},
		});
	}

	return (
		<div className="part-description accordion">
			<Wrapper {...props}>
				{Object.entries(desc.content).map(([_, value]) => (
					<Fragment key={value.id}>
						{[TYPES.JSON, TYPES.OBJECT].some(
							(v) => v === value.type,
						) ? (
							<Parts
								desc={value}
								onDelete={onDelete}
								onSelect={onSelect}
							/>
						) : (
							<AcordionItem
								onDelete={onDelete(value.id)}
								onSelect={onSelect(value.id)}
								name={value.id}
								pkg={desc}
								title={() => (
									<Title desc={value} title={value.id} />
								)}
							>
								<Parts
									desc={value}
									onDelete={onDelete}
									onSelect={onSelect}
								/>
							</AcordionItem>
						)}
					</Fragment>
				))}
			</Wrapper>
		</div>
	);
}

export function Package({
	desc,
	title,
	onDelete = () => null,
	onSelect = () => null,
}) {
	return (
		<AcordionItem
			onDelete={onDelete(desc.id)}
			onSelect={onSelect(desc.id)}
			name={desc.id}
			pkg={desc}
			title={() => <Title desc={desc} title={title} />}
		>
			<JSONDisplayWithClipboard value={mergeContentDescription(desc)} />
		</AcordionItem>
	);
}

function PartByType(props) {
	switch (props.desc.type) {
		case TYPES.PACKAGE:
			return <Package {...props} />;
		case TYPES.FOLDER:
			return <Folder {...props} />;
		default:
			return <Part {...props} />;
	}
}

export function PartDescription(props) {
	return (
		<div className="part-description accordion">
			<PartByType root={true} {...props} />
		</div>
	);
}
