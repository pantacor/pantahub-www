import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { CreateSubscriptions } from '../../../services/billing.service'
import LoadingButton from '../../atoms/LoadingButton/LoadingButton'
import {
  GetSubscriptionsAction,
  updateSubscriptionActions
} from '../../../store/billing/actions'
import { subscriptionsNewPath } from '../../../router/routes'
import { resolvePath } from '../../../lib/object.helpers'
import { capitalize } from '../../../lib/utils'
import { Money } from '../../atoms/Money/Money'

import './style.scss'
import '../PriceSelector/priceselector.scss'
import { PriceMetaData } from '../PriceMetaData/PriceMetaData'

export function SubscriptionCard ({ subscription = {} }) {
  return (
    <section className={`price-selector  d-flex align-items-stretch flex-column ${subscription.status ? 'active' : ''}`}>
      <header>
        <h3>{resolvePath(subscription, 'product.name', 'Free')}</h3>
        <p>
          {resolvePath(subscription, 'product.description', '')}
        </p>
      </header>
      <section className='price-content'>
        <h1 className='price-amount'>
          <Money
            amount={subscription.payment.amount}
            currency={subscription.payment.currency}
          />
          <small className='fw-lighter'>/month</small>
        </h1>
        <p className='description'>
          {resolvePath(subscription, 'product.description', '')}
        </p>
        <div>
          <PriceMetaData meta={resolvePath(subscription, 'product.metadata', {})} />
        </div>
      </section>
    </section>
  )
}
export function PriceCard ({ price, submiteable = true }) {
  const [loading, setLoading] = useState(false)
  const dispatch = useDispatch()
  const history = useHistory()
  const data = useSelector((state) => ({
    token: state.auth.token,
    customerID: state.billing.customer.id
  }))

  const createSubscription = async (e) => {
    if (price.active) { return }
    setLoading(true)
    const response = await CreateSubscriptions(data.token, {
      'customer_id': data.customerID,
      'price_id': price.id
    })

    if (!response.ok) {
      dispatch(updateSubscriptionActions.failure(response.json))
    }

    if (response.ok) {
      dispatch(updateSubscriptionActions.success(response.json))
      dispatch(GetSubscriptionsAction())
      history.push(`${subscriptionsNewPath}/${response.json.id}`)
    }

    setLoading(false)
  }

  return (
    <section className={`price-card ${price.active ? 'active' : ''}`}>
      <header>
        <h5>{resolvePath(price, 'product.name', 'Free')}</h5>
      </header>
      <section>
        <h1 className='cost mt-4 mb-4 text-center fw-normal'>
          <Money amount={price.unit_amount} currency={price.currency} />
          <span className='fw-lighter fs-5'>
            /{resolvePath(price, 'recurring.interval', 'month')}
          </span>
        </h1>
        <p className='description'>
          {resolvePath(price, 'product.description', '')}
        </p>
        <div className='options'>
          <PriceOptions meta={resolvePath(price, 'product.metadata', {})} />
        </div>
      </section>
      {submiteable && (
        <footer className='d-grid gap-2 mt-4 mb-2'>
          {price.active ? (
            <button
              className='btn btn-success'
              disabled={true}
            >
              Your current plan
            </button>
          ) : (
            <LoadingButton
              loadingChild='Changing plan ...'
              loading={loading}
              onClick={createSubscription}
              className='btn btn-primary btn-block'
            >
              Choose this plan
            </LoadingButton>
          )}
        </footer>
      )}
    </section>
  )
}

export function PriceOptions ({ meta = {} }) {
  if (meta === null) {
    return null
  }
  return (
    <div className="s-quotes">
      {Object.keys(meta).map((key) => (
        <div key={key} className='s-quotes-row row justify-content-center'>
          <span className='s-quotes--label col-6'>
            {capitalize(key)}
          </span>
          <span className='s-quotes--value col-6'>
            {meta[key]}
          </span>
        </div>
      ))}
    </div>
  )
}
