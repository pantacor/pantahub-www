import React from 'react'
import { TrackedButton } from '../../atoms/Tracker/Tracker'

import './style.scss'

export default class DropdownLinks extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      open: false
    }
    this.wrapperRef = React.createRef()
  }

  toggleSubMenu = (event) => {
    event.preventDefault()
    this.setState({ open: !this.state.open })
  }

  componentDidMount () {
    document.addEventListener('mousedown', this.handleClickOutside)
  }

  componentWillUnmount () {
    document.removeEventListener('mousedown', this.handleClickOutside)
  }

  handleClickOutside = (event) => {
    if (
      this.wrapperRef &&
      this.wrapperRef.current &&
      !this.wrapperRef.current.contains(event.target)
    ) {
      this.setState({ open: false })
    }
  }

  render () {
    const text = this.props.text || 'Select your device type'
    return (
      <div
        ref={this.wrapperRef}
        className="download-pantavisor dropdown"
      >
        <TrackedButton
          className="btn btn-info  dropdown-toggle"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded={this.state.open ? 'true' : 'false'}
          onClick={this.toggleSubMenu}
        >
          {text}
        </TrackedButton>
        <div className={`${this.state.open ? 'show' : ''} dropdown-menu`}>
          {this.props.images.map((image) => (
            <a
              key={`${image.name}-${image.rev}`}
              className="dropdown-item"
              href={image.url}
            >
              {image.name}
              {image.type && image.type !== 'default' && (
                ` - ${image.type}`
              )}
            </a>
          ))}
        </div>
      </div>
    )
  }
}
