/*
 * Copyright (c) 2021 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { TrackedButton } from '../../atoms/Tracker/Tracker'
import UserDeviceNavigatorMetadataRow from '../../organisms/UserDeviceNavigatorMetadataRow/UserDeviceNavigatorMetadataRow'

export function EditableMeta (props) {
  const {
    title,
    meta,
    editedMeta,
    loading,
    editing,
    newKey,
    newValue,
    globalMeta,
    saveHandler = () => {},
    editHandler = () => {},
    deleteHandler = () => {},
    addHandler = () => {},
    newKeyHandler = () => {},
    newValueHandler = () => {}
  } = props

  const mergedMeta = {
    ...(meta || {}),
    ...(editedMeta || {})
  }
  const metaEntries = Object.entries(mergedMeta).sort((a, b) => {
    if (a[1].type && b[1].type) {
      return a[1].type > b[1].type
    }
    if (a[0] > b[0]) {
      return 1
    }
    if (a[0] < b[0]) {
      return -1
    }
    // a must be equal to b
    return 0
  })
  const metaEntriesCount = metaEntries.length

  return (
    <div className="device-meta">
      {title && (<h3 className="mb-3 meta-title font-weight-light">{title}</h3>)}
      <div className="table-responsive">
        <table className="table table-borderless table-hover table-responsive table-lg">
          <thead>
            <tr>
              {globalMeta && (<th style={{ width: '10px' }}>Source</th>)}
              <th>Key</th>
              <th>Value</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {metaEntries.map(([key, value]) => {
              const inProgress =
                (editedMeta && !!editedMeta[key] && !meta[key]) ||
                (editedMeta && !editedMeta[key] && !!meta[key])
              return (
                <UserDeviceNavigatorMetadataRow
                  key={key}
                  inProgress={inProgress}
                  globalMeta={globalMeta}
                  currentKey={key}
                  type={value.type}
                  value={value.value ? value.value : value}
                  loading={loading}
                  disabled={editing === true}
                  onEdit={editHandler}
                  saveHandler={saveHandler}
                  onDelete={deleteHandler}
                />
              )
            })}
            {metaEntriesCount === 0 && (
              <tr className="bg-light">
                <th className="text-center" colSpan={3}>
                  Metadata is empty
                </th>
              </tr>
            )}
            <tr className="new-entry-header">
              <td colSpan={3}>
                <label>Add a new entry</label>
              </td>
            </tr>
            <tr className="new-entry-form">
              <td colSpan={2}>
                <input
                  type="text"
                  className="form-control my-1 mr-sm-2"
                  disabled={loading}
                  aria-label="Metadata Field Label"
                  onChange={newKeyHandler}
                  value={newKey}
                />
              </td>
              <td colSpan={1}>
                <textarea
                  className="form-control my-1 mr-sm-2 textarea"
                  disabled={loading}
                  aria-label="Metadata Field Value"
                  onChange={newValueHandler}
                  rows={Math.max(newValue.split('\n').length, 1)}
                  value={newValue}
                />
              </td>
              <td className="pt-2 text-end">
                <TrackedButton
                  type="button"
                  className={`btn btn-primary btn-sm my-1 mr-sm-2 ${
                    loading ? 'disabled' : ''
                  }`}
                  disabled={loading}
                  onClick={addHandler}
                  title="Add"
                >
                  <span className="mdi mdi-plus" />
                </TrackedButton>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  )
}
