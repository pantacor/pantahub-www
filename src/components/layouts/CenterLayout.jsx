import React from "react";
import { getPlatform } from "../../lib/native";
import { Helmet } from "react-helmet";
import Nav from "../molecules/Nav/Nav";
import Footer from "../molecules/Footer/Footer";
import { GetEnv } from "../../lib/const.helpers";

export const CenterLayout = ({ children }) => {
	return (
		<div className={getPlatform()} style={{ overflow: "hidden" }}>
			<Helmet>
				<title>{GetEnv("REACT_APP_TITLE", "PantaHub")}</title>
			</Helmet>
			<div className="minimal-height">{children}</div>
			<footer>
				<Footer />
			</footer>
		</div>
	);
};
