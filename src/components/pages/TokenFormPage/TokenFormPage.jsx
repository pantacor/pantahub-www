/*
 * Copyright (c) 2017-2024 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { Component, useCallback, useState } from 'react'
import { tokens, userDashboardPath } from '../../../router/routes'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { getScopes } from '../../../store/applications/actions'
import { validate } from './validation'
import AddFromList from '../../molecules/AddFromList/AddFromList'
import { TrackedButton } from '../../atoms/Tracker/Tracker'
import TextInput from '../../atoms/TextInput/TextInput'
import { CreateToken } from '../../../services/tokens.service'
import { TimeSelect } from '../../atoms/TimeSelect/TimeSelect'

const STATES = {
  IN_PROGRESS: 'IN_PROGRESS',
  SUCCESS: 'SUCCESS',
  FAILURE: 'FAILURE',
  INITIAL: 'INITIAL'
}

class TokenFormPage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      status: STATES.INITIAL,
      secret: '',
      name: '',
      error: null
    }
  }
  async componentDidMount () {
    this.props.getScopes()
  }

  createToken = async (token) => {
    this.setState({ status: STATES.IN_PROGRESS, secret: '' })
    try {
      const tokenPayload = {
        name: token.name,
        scopes: token.scopes.map(s => `${s.service}/${s.id}`),
        'expire-at': token.expireAt
      }
      const response = await CreateToken(this.props.token, tokenPayload)
      if (!response.ok) {
        this.setState({ status: STATES.FAILURE, error: response.json, secret: '' })
        return response.json
      }
      this.setState({ status: STATES.SUCCESS, secret: response.json.secret, name: response.json.name })
      return null
    } catch (error) {
      this.setState({ status: STATES.FAILURE, error: error, secret: '' })
      return error
    }
  }

  render () {
    return (
      <div className="tokenform-detail">
        <div
          key="breadcrumbs"
          className="breadcrumb-sector row align-items-center"
        >
          <div className="col-8">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to={`${userDashboardPath}`}>Dashboard</Link>
              </li>
              <li className="breadcrumb-item">
                <Link to={`${tokens}`}>Tokens</Link>
              </li>
              <li className="breadcrumb-item active">Create token</li>
            </ol>
          </div>
        </div>
        <div className='row justify-content-center'>
          <div className='col-md-8'>
            <h1 className='text-center'>Create personal token</h1>
            {this.state.secret !== '' && this.state.status === STATES.SUCCESS && (
              <div className='mt-40'>
                <Success name={this.state.name} secret={this.state.secret} />
              </div>
            )}
            {this.state.status === STATES.FAILURE && (
              <div className='mt-40'>
                <Error {...this.state.error} />
              </div>
            )}
            <div className='mt-40'>
              <TokenForm
                saveToken={this.createToken}
                scopes={this.props.scopes}
                status={this.state.status}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

/**
 * Renders a form for creating or updating a token.
 *
 * @param {Object} props - The props object containing the following properties:
 *   @param {Function} props.saveToken - The function to save the token.
 *   @param {string} props.status - The status of the token creation process.
 *   @param {Array} props.scopes - The available scopes for the token.
 * @return {JSX.Element} The rendered token form component.
 */
function TokenForm ({ saveToken, status, scopes }) {
  const [state, setState] = useState({
    name: '',
    scopes: [],
    expireAt: (new Date(Date.now() + 31536000000)).toISOString(),
    wasValidated: false,
    errors: []
  })

  const onChange = (value) => (event) => {
    let input = event.target.value
    if (value === 'name') {
      input = input.replace(' ', '_')
    }
    if (value === 'expireAt') {
      input = new Date(input).toISOString()
    }
    setState({
      ...state,
      [value]: input
    })
  }

  const submit = useCallback((event) => {
    event.preventDefault()
    const form = validate(state)
    if (form.valid) {
      saveToken(state, state.id).then((error) => {
        if (!error) {
          setState({ ...state, wasValidated: true, errors: [], name: '', scopes: [] })
        }
      })
      return
    }
    const errors = Object.values(form.errors).reduce((acc, v) => [...acc, ...v], [])
    setState({ ...state, errors: errors })
  }, [state, saveToken])

  const updateScope = (id, scope) => { state.scopes[id] = scope; setState(state) }
  const removeScope = id => { setState({ ...state, scopes: [...state.scopes.slice(0, id), ...state.scopes.slice(id + 1)] }) }

  const loading = status === STATES.IN_PROGRESS
  const wasValidatedClass = state.wasValidated ? 'was-validated' : ''
  const saveCreateMessage = state.id ? 'Save app' : 'Create app'

  return (
    <div className="app-form row justify-content-center">
      <form
        onSubmit={submit}
        noValidate={true}
        className={wasValidatedClass}
      >
        <TextInput
          label="Name"
          name="name"
          type="text"
          placeholder="Name"
          className="form-control"
          value={state.name}
          onChange={onChange('name')}
        />
        <TimeSelect
          label="Expire at"
          name="expire-at"
          placeholder="Expire at"
          className="form-control"
          value={state.expireAt}
          onChange={onChange('expireAt')}
        />
        <AddFromList
          values={state.scopes}
          optionsMap={{ getLabel: (o) => `${o.description} (${o.id})` }}
          onAdd={(v) => setState({ ...state, scopes: [...state.scopes, v] })}
          onUpdate={updateScope}
          onRemove={removeScope}
          getId={(v) => `${v.service}/${v.id}`}
          options={scopes}
          label="Scopes"
          name="scopes"
          always_required={true}
        />
        { state.errors.length > 0 && (
          <React.Fragment>
            <h6 className="text-danger">
              Sorry! We found 1 or more errors:
            </h6>
            <ul className="text-danger">
              {state.errors.map((e, i) => (
                <li key={`${i}-${e.type}`}>{e.message}</li>
              ))}
            </ul>
          </React.Fragment>
        )}
        <TrackedButton
          type="submit"
          className="btn btn-primary btn-block mt-4"
          disabled={loading}
        >
          {loading
            ? (
              <span className="mdi mdi-refresh pantahub-loading" />
            )
            : (
              saveCreateMessage
            )}
        </TrackedButton>
      </form>
    </div>
  )
}

/**
 * Renders an error message component with the given error code, error message, and optional custom message.
 *
 * @param {Object} props - The props object containing the following properties:
 *   @param {number} props.cod - The error code.
 *   @param {string} props.error - The error message.
 *   @param {string} [props.msg] - An optional custom message.
 * @return {JSX.Element} The rendered error message component.
 */
function Error ({ cod, error, msg }) {
  if (!msg) {
    return (
      <div className="alert alert-danger">
        {error} ({cod})
      </div>
    )
  }
  return (
    <div className="alert alert-danger">
      {msg} ({error} - {cod})
    </div>
  )
}

/**
 * Renders a success message component with the given name and secret.
 *
 * @param {Object} props - The props object containing the following properties:
 *   @param {string} props.name - The name of the created token.
 *   @param {string} props.secret - The secret of the created token.
 * @return {JSX.Element} The rendered success message component.
 */
function Success ({ name, secret }) {
  return (
    <div className='alert alert-success'>
      <h6>Token created!</h6>
      <span>name:</span>
      <pre className='bg-light pl-2 pr-2 pt-2 pb-2'>{name}</pre>
      <span>secret:</span>
      <pre className='bg-light pl-2 pr-2 pt-2 pb-2'>{secret}</pre>
    </div>
  )
}

export default connect(
  state => ({
    scopes: state.applications.scopes,
    token: state.auth.token
  }),
  {
    getScopes
  }
)(TokenFormPage)
