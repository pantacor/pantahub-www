/*
 * Copyright (c) 2017-2024 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import { tokenNew, userDashboardPath } from '../../../router/routes'
import { Link } from 'react-router-dom'
import { DeleteToken, GetTokens } from '../../../services/tokens.service'
import { connect } from 'react-redux'
import DeleteConfirmButton from '../../atoms/ConfirmButton/ConfirmButton'
import Time from '../../atoms/Time/Time'

/**
 * @typedef {import('../../../services/tokens.service').Token} Token
 */

const STATES = {
  IN_PROGRESS: 'IN_PROGRESS',
  SUCCESS: 'SUCCESS',
  FAILURE: 'FAILURE',
  INITIAL: 'INITIAL'
}

class TokenListPage extends Component {
  /**
   * @type {{
   *   tokens: Map<string,Token>,
   *   status: typeof STATES,
   *   error: string | null
   * }}
   */
  state = {
    tokens: new Map([]),
    status: STATES.INITIAL,
    error: null
  }

  async componentDidMount () {
    await this.getTokens()
  }

  getTokens = async () => {
    this.setState({ status: STATES.IN_PROGRESS })
    try {
      const response = await GetTokens(this.props.token)
      if (!response.ok) {
        this.setState({ status: STATES.FAILURE, error: response.statusText })
        return
      }
      this.setState({
        tokens: new Map(response.json.items.map((t) => [t.id, t])),
        status: STATES.SUCCESS
      })
    } catch (error) {
      this.setState({ status: STATES.FAILURE, error: error })
    }
  }

  deleteToken = async (id) => {
    this.setState({ status: STATES.IN_PROGRESS })
    try {
      const response = await DeleteToken(this.props.token, id)
      if (!response.ok) {
        this.setState({ status: STATES.FAILURE, error: response.statusText })
        return
      }
      this.state.tokens.delete(id)
      this.setState({ status: STATES.SUCCESS, tokens: this.state.tokens })
    } catch (error) {
      this.setState({ status: STATES.FAILURE, error: error })
    }
  }

  render () {
    return (
      <div className='thrid-app'>
        <div
          key='breadcrumbs'
          className='breadcrumb-sector row align-items-center'
        >
          <div className='col-8'>
            <ol className='breadcrumb'>
              <li className='breadcrumb-item'>
                <Link to={`${userDashboardPath}`}>Dashboard</Link>
              </li>
              <li className='breadcrumb-item active'>Tokens</li>
            </ol>
          </div>
        </div>
        <div className='d-flex justify-content-end justify-content-end mt-4 mb-4'>
          <div className='d-flex justify-content-end mb-4'>
            <Link
              to={`${tokenNew}`}
              className='btn btn-primary'
            >
              Create new token
            </Link>
          </div>
        </div>
        <TokenList tokens={this.state.tokens} deleteToken={this.deleteToken} />
      </div>
    )
  }
}

const sortableFields = [
  ['nick', 'Token Name'],
  ['scopes', 'Scopes'],
  ['expire-at', 'Expiration Date']
]

/**
 * Function component for displaying a list of tokens.
 *
 * @param {Object} props - React props object
 * @param {Array<Token>} props.tokens - Array of tokens to display
 * @return {JSX.Element} JSX element displaying the token list
 */
function TokenList (props) {
  const { tokens } = props

  if (tokens.size === 0) {
    return <NoTokens />
  }

  return (
    <section>
      <table className="table table-borderless table-hover">
        <thead>
          <tr>
            {sortableFields.map(([field, label]) => (
              <th
                key={field}
                className={`${field}`}
              >
                <span>
                  {label}{' '}
                </span>
              </th>
            ))}
            <th className="text-center actions" style={{ width: '180px' }}>Actions</th>
          </tr>
        </thead>
        <tbody>
          {[...tokens.values()].map((token) => (
            <TokenListItem key={token.id} token={token} deleteToken={props.deleteToken} />
          ))}
        </tbody>
      </table>
    </section>
  )
}

/**
 * Function component for rendering a single token item.
 *
 * @param {Object} props - React props object
 * @param {Token} props.token - Array of tokens to display
 * @return {JSX.Element} JSX element displaying the token item
 */
function TokenListItem (props) {
  const { token } = props
  return (
    <tr key={token.id} className="token-row">
      <td>
        {token.name}
      </td>
      <td>
        {token['parse-scopes'].map((s) => (
          <div key={s.id}>{s.description} ({s.id})</div>
        ))}
      </td>
      <td>
        <Time when={token['expire-at']} />
      </td>
      <td style={{ paddingBottom: '0.5em', display: 'flex', justifyContent: 'center' }}>
        <div
          style={{ paddingLeft: '1em', flex: 1, display: 'inline-flex', flexGrow: 0, justifyContent: 'center' }}
          className="btn-group"
          role="group"
          aria-label="Token actions"
        >
          <DeleteConfirmButton
            style={{ position: 'fixed' }}
            onDelete={() => props.deleteToken(token.id)}
          />
        </div>
      </td>
    </tr>
  )
}

function NoTokens () {
  return (
    <div className="d-flex justify-content-center w-auto">
      <div className="shadow-sm p-4 mb-3 mt-3 rounded border-bottom text-muted small" style={{ color: '#4D4D4D' }}>
        Looks like there&apos;s nothing to show here. Create your first personal token now!
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  token: state.auth.token
})

export default connect(mapStateToProps)(TokenListPage)
