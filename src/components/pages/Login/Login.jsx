/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { useCallback, useEffect } from "react";

import { connect } from "react-redux";

import { Link } from "react-router-dom";

import {
  setUsername,
  setPassword,
  getToken,
  getTokenSuccess,
} from "../../../store/auth/actions";

import { signUpPath, requestNewPassword } from "../../../router/routes";
import useFormValidation from "../../../hooks/useFormValidation";
import OAuthButtons from "../../molecules/OAuthButtons/OAuthButtons";
import TextSeparator from "../../atoms/TextSeparator/TextSeparator";
import { parseImplicitData } from "../../../lib/api.helpers";
import { useLocation } from "react-router";
import { SetFlash } from "../../../store/general-flash/actions";
import { TrackedButton } from "../../atoms/Tracker/Tracker";
import { Track } from "../../../lib/analytics.helper";
import { GetEnv } from "../../../lib/const.helpers";

function Login(props) {
  const {
    dispatch,
    username,
    password,
    loading,
    getTokenError,
    labels = false,
  } = props;
  const { formState, onSubmit, onInput, getError } = useFormValidation();

  const location = useLocation();

  useEffect(() => {
    const urlSearch = new URLSearchParams(location.search);
    const err = urlSearch.get("error");
    const token = parseImplicitData(location.hash).token;

    if (!!err && err !== "") {
      dispatch(SetFlash("general", err));
      return;
    }

    if (!!token && token !== "") {
      Track("Action", { action: "login-success-oauth" });
      dispatch(getTokenSuccess(token));
    }
  }, [location, location.hash, dispatch]);

  const submit = useCallback(
    (event) => {
      const { isValid } = onSubmit(event);
      if (isValid) {
        dispatch(getToken(username, password));
      }
    },
    [username, password, dispatch, onSubmit],
  );

  const wasValidatedClass = formState.wasValidated ? "was-validated" : null;

  if (
    getTokenError !== null &&
    (getTokenError.Error ||
      (typeof getTokenError.message === "string" && getTokenError.message))
  ) {
    Track("Action", { action: "login-error" });
  }

  const disableEmailPasswordLogin =
    GetEnv("REACT_APP_DISABLE_EMAIL_PASSWORD_LOGIN", "false") === "true";

  return (
    <div className="login container index-2 pt-6">
      <div className="row justify-content-center">
        <div className="col-lg-4 col-sm-10 col-md-8">
          <div className="form-registration-box">
            <div className="card">
              <div className="card-body">
                <h1>Log In</h1>
                {getTokenError !== null && (
                  <React.Fragment>
                    <p key="errors" className="alert alert-danger">
                      You have entered an invalid username or password
                      <br />
                      <small style={{ fontSize: "0.7em" }}>
                        Tracking details: &nbsp;
                        {getTokenError.error ||
                          getTokenError.Error ||
                          (typeof getTokenError.message === "string" &&
                            getTokenError.message) ||
                          `${
                            getTokenError.error ||
                            getTokenError.Error ||
                            getTokenError.message
                          }`}
                      </small>
                    </p>
                  </React.Fragment>
                )}
                {!disableEmailPasswordLogin && (
                  <>
                    <form
                      onSubmit={submit}
                      noValidate={true}
                      className={wasValidatedClass}
                    >
                      <div className="form-group pb-2 pb-2 ">
                        {labels && <label htmlFor="username">Username</label>}
                        <input
                          type="text"
                          autoCapitalize="none"
                          className="form-control"
                          id="username"
                          name="username"
                          required="required"
                          value={username}
                          onInput={onInput}
                          onChange={(evt) =>
                            dispatch(setUsername(evt.target.value))
                          }
                          placeholder="Username"
                        />
                        <div className="invalid-feedback">
                          {" "}
                          {getError("username")}{" "}
                        </div>
                      </div>
                      <div className="form-group pb-2 pb-2 ">
                        {labels && <label htmlFor="password">Password</label>}
                        <input
                          type="password"
                          className="form-control"
                          id="password"
                          name="password"
                          required="required"
                          value={password}
                          onInput={onInput}
                          onChange={(evt) =>
                            dispatch(setPassword(evt.target.value))
                          }
                          placeholder="Password"
                        />
                        <div className="invalid-feedback">
                          {" "}
                          {getError("password")}{" "}
                        </div>
                      </div>
                      <div className="form-group pb-2 pb-2 d-flex flex-row-reverse">
                        <TrackedButton
                          type="submit"
                          event="Action"
                          payload={{ action: "login" }}
                          className="btn btn-primary  btn-block"
                          disabled={loading}
                        >
                          {loading ? (
                            <span className="mdi mdi-refresh pantahub-loading" />
                          ) : (
                            ""
                          )}
                          Login
                        </TrackedButton>
                      </div>
                    </form>

                    <p className="text-center fs-7 mb-0">
                      <Link to={requestNewPassword}>Forgot your password?</Link>
                    </p>
                    <p className="text-center fs-7 mb-0">
                      {"Don't have an account? "}
                      <Link to={signUpPath}>Sign Up now!</Link>
                    </p>
                    <TextSeparator backgroundColor="white">or</TextSeparator>
                  </>
                )}
                <OAuthButtons text="Sign in with" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <br />
      <br />
    </div>
  );
}

export default connect((state) => state.auth)(Login);
