import dayjs from 'dayjs'
import React from 'react'
import { useSelector } from 'react-redux'
import { resolvePath } from '../../../lib/object.helpers'
import { capitalize } from '../../../lib/utils'

function getTimestampInSeconds () {
  return Math.floor(Date.now() / 1000)
}

export function SubscriptionDayleft () {
  const { subscription, s } = useSelector((state) => {
    return {
      subscription: resolvePath(state, 'dash.data.subscription', {}) || {},
      s: resolvePath(state, 'billing.subscriptions.current', {}) || {}
    }
  })
  const billingPeriod = subscription.billing || {}
  if (s.current_period_end) {
    billingPeriod.Type = 'Monthly'
    billingPeriod.Method = resolvePath(s, 'collection_method', 'charge_automatically')
    billingPeriod.AmountDue = 0
    billingPeriod.Currency = resolvePath(s, 'payment.currency', 'USD')
    billingPeriod.VatRegion = 'World'
    billingPeriod.Max = s.current_period_end
    billingPeriod.Actual = getTimestampInSeconds()
  }

  const daysLeft = dayjs.unix(billingPeriod.Max || 0)
    .diff(
      dayjs.unix(billingPeriod.Actual || 0),
      'day'
    )

  if (daysLeft <= 0) {
    return null
  }

  return (
    <>
      <span className='small text-muted'>{daysLeft} Days Left</span><br />
      {billingPeriod.Method && (
        <span className='small text-muted'>
          {billingPeriod.Method.split('_').map(a => capitalize(a)).join(' ')}
        </span>
      )}
    </>
  )
}
