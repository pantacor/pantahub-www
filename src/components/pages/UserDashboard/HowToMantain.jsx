/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import cloneImage from '../../../assets/images/clone_example.png'

export default function HowToMantain () {
  return (
    <div className="row hts__steps pb-4">
      <div className="col-md-12 pb-5 pt-4">
        <h1>How to maintain your system?</h1>
        <p>
          There are 3 easy steps to mantain and deploy changes to your PantacorHub devices.
          <br/>If you want more information about
          how to maintain your system, you can read the full documentation&nbsp;
          <a href="https://docs.pantahub.com/clone-your-system/" rel="noopener noreferrer" target="_blank">here</a>.
        </p>
      </div>
      <div className="col-md-12 mt-3 mb-3">
        <div className="row d-flex align-items-center">
          <div className="col-md-2">
            <div className="text-center hts__step-circle hts__step-circle--small">
              <span>1</span>
            </div>
          </div>
          <div className="col-md-10">
            <h3 className="font-weight-light mt-2">Clone your device</h3>
          </div>
          <div className="col-md-12 pt-4">
            <p>
              Pantavisor managed systems automatically will sync their config and runtime factory state on first contact to their PantacorHub instance.
            </p>
            <p>
              In order to clone a system, copy the PVR Clone URL from PantacorHub and use pvr:
            </p>
            <pre className="bg-light pl-2 pr-2 pt-2 pb-2">
              <code>pvr clone https://pvr.pantahub.com/user1/device1</code>
            </pre>
            <p>
              <img src={cloneImage} alt="how to clone a device" className="img-fluid"/>
            </p>
          </div>
        </div>
      </div>
      <div className="col-md-12 mt-3 mb-3">
        <div className="row d-flex align-items-center">
          <div className="col-md-2">
            <div className="text-center hts__step-circle hts__step-circle--small">
              <span>2</span>
            </div>
          </div>
          <div className="col-md-10">
            <h3 className="font-weight-light mt-2">Make a new system revision</h3>
          </div>
          <div className="col-md-12 pt-4">
            <p>
              To see current, not-staged file changes, use pvr status command:
            </p>
            <pre className="bg-light pl-2 pr-2 pt-2 pb-2">
              <code>cd device1</code>
            </pre>
            <pre className="bg-light pl-2 pr-2 pt-2 pb-2">
              <code>pvr status .</code>
            </pre>
            <pre className="bg-light pl-2 pr-2 pt-2 pb-2">
              <code>pvr diff</code>
            </pre>
            <pre className="bg-light pl-2 pr-2 pt-2 pb-2">
              <code>pvr commit</code>
            </pre>
          </div>
        </div>
      </div>
      <div className="col-md-12 mt-3 mb-3">
        <div className="row d-flex align-items-center">
          <div className="col-md-2">
            <div className="text-center hts__step-circle hts__step-circle--small">
              <span>3</span>
            </div>
          </div>
          <div className="col-md-10">
            <h3 className="font-weight-light mt-2">Deploy a new System Revision</h3>
          </div>
          <div className="col-md-12 pt-4">
            <p>
              Once you have done some changes like described in previous section, you can deploy the
              currently staged pvr state to any device for which you have owner permissions.
            </p>
            <pre className="bg-light pl-2 pr-2 pt-2 pb-2">
              <code>pvr post</code>
            </pre>
            <p style={{ paddingBottom: '2em' }}>
              Once post has ben submitted to a device, the device will eventually wake up
              and try to consume the new state. On success, you will see the device going to DONE state.
              If not, it will go to ERROR state.
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}
