/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { userDashboardPath, StartNewDevice } from '../../../router/routes'

export default class DashboardNavigation extends Component {
  render () {
    const { username, className } = this.props
    return (
      <div className={`getting-started ${className || ''}`}>
        <div className="row mt-2 mb-2">
          <div className="btn-group" role="group">
            <Link
              className="btn btn-info"
              to={`${StartNewDevice}`}
            >
              Initialize Device
            </Link>
            <Link
              className="btn btn-info"
              to={`${userDashboardPath}/claim`}
            >
              Claim a Device
            </Link>
            <Link
              className="btn btn-info"
              to={`${userDashboardPath}/${username}/devices`}
            >
              Browse Devices
            </Link>
          </div>
        </div>
      </div>
    )
  }
}
