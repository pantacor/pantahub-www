/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import UserDashboardSubscriptionCharts from './UserDashboardSubscriptionCharts'
import UserDashboardTopDevices from './UserDashboardTopDevices'
import { resolvePath } from '../../../lib/object.helpers'
import { SubscriptionDayleft } from './SubscriptionDayleft'
import { useSelector } from 'react-redux'
import { GetEnv } from '../../../lib/const.helpers'
import { Link } from 'react-router-dom'
import { subscriptionsPath } from '../../../router/routes'
// import { useSelector } from 'react-redux'
// import { DefaultProduct } from '../../organisms/SubscriptionStatus/SubscriptionStatus'
// import { SubscriptionDayleft } from '../UserDashboard/SubscriptionDayleft'

function getTimestampInSeconds () {
  return Math.floor(Date.now() / 1000)
}

function ManagePlan () {
  const { subscription, s } = useSelector((state) => {
    return {
      subscription: resolvePath(state, 'dash.data.subscription', {}) || {},
      s: resolvePath(state, 'billing.subscriptions.current', {}) || {}
    }
  })
  const billingPeriod = subscription.billing || {}
  if (s.current_period_end) {
    billingPeriod.Type = 'Monthly'
    billingPeriod.Method = resolvePath(s, 'collection_method', 'charge_automatically')
    billingPeriod.AmountDue = 0
    billingPeriod.Currency = resolvePath(s, 'payment.currency', 'USD')
    billingPeriod.VatRegion = 'World'
    billingPeriod.Max = s.current_period_end
    billingPeriod.Actual = getTimestampInSeconds()
  }

  let productName = resolvePath(subscription, 'product.name', 'Free')
  if (productName === '') {
    productName = 'Free'
  }

  return (
    <>
      <h5 className='fw-normal mt-2 mb-2'>
        Your are subscribed to: <b>{productName}</b>
      </h5>
      <div className="mt-4 mb-4">
        {GetEnv('REACT_APP_BILLING', 'false') === 'true' ? (
          <p>
            <Link
              exact
              className="btn btn-primary"
              to={subscriptionsPath}
            >
              Manage subscription
            </Link>
          </p>
        ) : (
          <p>
            <a href="mailto:sales@pantacor.com">Contact Sales</a>&nbsp;
            for more information and secure
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://pantacor.com/pricing/"
            > Enterprise</a> grade functionality.
          </p>
        )}
      </div>
    </>
  )
}

export default function UserDashboardData (props) {
  // const state = useSelector((s) => {
  //   const subscription = resolvePath(s, 'billing.subscriptions.current', {}) || {}
  //   return {
  //     subscription: subscription,
  //     plan: mergeObject(DefaultProduct(subscription.meta), subscription.product)
  //   }
  // })
  const { username, loading, topDevices, subscription, className } = props
  const deviceCount = resolvePath(subscription, 'quota-stats.DEVICES.Actual', 0)

  return (
    <div className={`dashboard-sector ${className || ''}`}>
      <div className="row col-md-12">
        <ManagePlan />
      </div>
      <div className="row">
        <div className="col-md-6">
          <div className="card card-with-bg">
            <div className="card-body quota-box">
              <h4 className='card-title'>Your quota</h4>
              <UserDashboardSubscriptionCharts
                subscription={subscription}
                loading={loading}
              />
              <div className="row">
                <div className="col-12 text-center mb-4 mt-2">
                  <SubscriptionDayleft />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <div className="card card-with-bg">
            <div className="card-body top-devices-box">
              {deviceCount > 0 && (
                <React.Fragment>
                  <h4 className='card-title'>Your top devices</h4>
                  <div className="row">
                    <div className="col-md-12 table-top-devices">
                      <UserDashboardTopDevices
                        username={username}
                        topDevices={topDevices}
                        loading={loading}
                      />
                    </div>
                  </div>
                </React.Fragment>
              )}
            </div>
          </div>
        </div>
      </div>
      <div className="row col-md-12 mt-5">
        <p>
          To learn more about how to mantain and deploy changes to your devices,
          please visit our&nbsp;
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://docs.pantahub.com/"
          >
            documentation
          </a>.
        </p>
      </div>
    </div>
  )
}
