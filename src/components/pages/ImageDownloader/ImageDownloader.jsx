/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { connect } from 'react-redux'
import omit from 'lodash.omit'

import { getImagesData, IMAGES_CI_URL } from '../../../services/images.service'
import Loading from '../../atoms/Loading/Loading'
import LoadingButton from '../../atoms/LoadingButton/LoadingButton'
import { DownloadImageWithAuth } from '../../../store/images/actions'
import CheckInput from '../../atoms/CheckInput/CheckInput'
import { Breadcumbs } from '../../atoms/Breadcumbs/Breadcumbs'
import { userDashboardPath } from '../../../router/routes'
import { IMAGE_NAMES } from '../../../data/image_names'

const STATUSES = {
  LOADING: 'l',
  NONE: 'n',
  SUCESS: 's',
  FAILURE: 'f',
  DOWNLOADING: 'd'
}

const NETWORK_TYPES = {
  ETHERNET: 'ethernet',
  WIFI: 'wifi'
}

const wifiPartToCmd = {
  'ssid': 'pvnet_ssid',
  'password': 'pvnet_pass',
  'ip': 'pvnet_ipv4'
}

const getImageName = (o) => {
  if (o.id) {
    return o.id
  }
  return o.name && o.name !== 'default'
    ? `${o.target} - ${o.name} (${o.devicenick})`
    : `${o.target} (${o.devicenick})`
}

const defaultIPForLocal = '192.168.222.2/24'

const localExperienceCmdValues = (ip = defaultIPForLocal, type = NETWORK_TYPES.WIFI) => ([
  'pv_remote.control=0',
  'pvrsdk_httpd_listen=0.0.0.0',
  `pvnet_type=${type}`,
  `pvnet_ipv4=${ip}`
])

class ImageDownloader extends React.Component {
  constructor (props) {
    super(props)

    const searchParams = new URLSearchParams(window.location.search)

    const source = searchParams.get('source')
      ? searchParams.get('source')
      : IMAGES_CI_URL

    const meta = searchParams.get('meta')
      ? JSON.parse(searchParams.get('meta') || '{}')
      : null

    const cmdline = searchParams.get('cmdline')
      ? [...new Set(searchParams.get('cmdline').split(' '))]
      : undefined

    this.state = {
      source: source,
      channels: ['Loading channels...'],
      advanceMode: cmdline && cmdline !== '',
      channel: '',
      devices: [],
      meta: meta,
      cmdline: cmdline,
      selectedDevice: null,
      error: null,
      status: STATUSES.NONE,
      network: {
        ssid: null,
        password: null,
        ip: undefined,
        type: 'wifi'
      },
      localExperience: false,
      autoClaimDevice: !(searchParams.get('autoclaim-device') === 'false') || searchParams.get('seteable-meta') === 'true',
      seteableMeta: searchParams.get('seteable-meta') === 'true',
      disableMeta: false,
      seteableWifi: searchParams.get('seteable-wifi') === 'true'
    }
  }

  componentDidMount () {
    this.getChannels(this.state.source)
  }

  getChannels = async (src) => {
    try {
      this.setState({ status: STATUSES.LOADING })
      const resp = await getImagesData(src)
      if (!resp.ok) {
        throw Error('Error getting source json')
      }

      const channelDevices = Object.keys(resp.json).reduce((acc, key) => {
        if (resp.json[key].length > 0) {
          acc.channels.push(key)
          acc.devices[key] = []
          resp.json[key][0].devices.forEach(d => {
            d.images.forEach(i => {
              acc.devices[key].push({
                ...omit(d, ['images']),
                ...i,
                'id': IMAGE_NAMES[`${d.job}-${i.name}`]
              })
            })
          })

          acc.devices[key].sort((a, b) => a.id > b.id)
        }
        return acc
      }, { channels: [], devices: {} })

      const channel = channelDevices.channels.length > 0
        ? channelDevices.channels.find((c) => c === 'stable') || channelDevices.channels[0]
        : ''

      const selectedDevice = channel !== '' && channelDevices.devices[channel][0]

      return this.setState({
        status: STATUSES.SUCESS,
        channels: channelDevices.channels,
        devices: channelDevices.devices,
        selectedDevice: selectedDevice,
        channel: channel,
        source: src
      })
    } catch (e) {
      return this.setState({
        status: STATUSES.FAILURE,
        channel: '',
        error: e
      })
    }
  }

  setSource = (event) => {
    this.getChannels(event.target.value)
  }

  setChannel = (event) => {
    const value = event.target.value
    this.setState(state => ({ ...state, channel: value }))
  }

  selectDevice = (event) => {
    const index = event.target.value
    const selectedDevice = this.state.devices[this.state.channel][index]
    this.setState(state => ({ ...state, selectedDevice }))
  }

  toggleAdvanceMode = () => {
    this.setState((state) => ({ ...state, advanceMode: !this.state.advanceMode }))
  }

  getCmdline = () => {
    return this.state.cmdline ? this.state.cmdline.join('\n') : ''
  }

  setCmdline = (event) => {
    event.preventDefault()
    const cmdline = event.target.value || ''
    return this.setState(state => ({ ...state, cmdline: cmdline.split(' ') }))
  }

  getMeta = () => {
    try {
      if (!this.state.meta) {
        return ''
      }
      return JSON.stringify(this.state.meta, null, '\t')
    } catch (e) {
      return ''
    }
  }

  setMeta = (event) => {
    try {
      if (event.target.value.trim() === '') {
        this.setState(state => ({ ...state, meta: null }))
        return
      }
      const meta = JSON.parse(event.target.value)
      this.setState(state => ({ ...state, meta }))
    } catch (e) {
      console.warn(e)
    }
  }

  downloadImage = (event) => {
    event.preventDefault()
    this.setState({ status: STATUSES.DOWNLOADING })
    const bucket = this.state.source.match(/https:\/\/(.*?)\./)[1]
    const project = this.state.source.match(/https:\/\/[^/]*\/(.*)\/[^/]*\.json/)[1]
    const releaseindex = this.state.source.match(/https:\/\/[^/]*\/.*\/([^/]*)\.json/)[1]
    const variant = this.state.selectedDevice.name.indexOf('default') < 0
      ? this.state.selectedDevice.name
      : undefined

    const payload = this.state.meta && this.state.autoClaimDevice && this.state.seteableMeta
      ? { 'default-user-meta': this.state.meta }
      : null

    this.props.DownloadImageWithAuth({
      device: this.state.selectedDevice.target,
      devicenick: this.state.selectedDevice.devicenick,
      channel: this.state.channel,
      bucket: bucket,
      project: project,
      releaseindex: releaseindex,
      payload: payload,
      variant: variant,
      wifiConfig: undefined,
      cmdline: this.state.cmdline ? this.state.cmdline.join(' ') : undefined,
      autoClaim: this.state.autoClaimDevice,
      source: this.state.source
    })

    setTimeout(() => {
      this.setState({ status: STATUSES.NONE })
    }, 400)
  }

  updateSeteableWifi = (val) => {
    let cmdline = this.state.cmdline
    if (!cmdline) {
      cmdline = []
    }

    const typeIndex = cmdline.findIndex((v) => v.indexOf('pvnet_type') >= 0)
    let type = NETWORK_TYPES.WIFI
    switch (true) {
      case !val && this.state.localExperience:
        type = NETWORK_TYPES.ETHERNET
        break
      case val:
        type = NETWORK_TYPES.WIFI
        break
      default:
        type = null
        break
    }

    if (type) {
      const value = `pvnet_type=${type}`
      typeIndex >= 0 ? cmdline[typeIndex] = value : cmdline.push(value)
    }

    this.setState({
      seteableWifi: val,
      network: {
        ...this.state.network,
        type
      },
      cmdline
    })
  }

  updateLocalExperience = (val) => {
    let cmdline = this.state.cmdline
    if (!cmdline) {
      cmdline = []
    }

    const type = val ? NETWORK_TYPES.ETHERNET : this.state.network.type
    const ip = val ? defaultIPForLocal : 'dhcp'
    localExperienceCmdValues(ip, type).forEach((value) => {
      const key = value.split('=')[0]
      const partIndex = cmdline.findIndex((v) => v.indexOf(key) >= 0)
      if (partIndex >= 0 && !val) {
        cmdline = [...cmdline.slice(0, partIndex), ...cmdline.slice(partIndex + 1)]
      } else {
        partIndex >= 0 ? cmdline[partIndex] = value : cmdline.push(value)
      }
    })

    this.setState({
      localExperience: val,
      disableMeta: val,
      autoClaimDevice: !val,
      network: {
        ...this.state.network,
        ip,
        type
      },
      cmdline
    })
  }

  updateAutoClaim = (val) => {
    const newState = { autoClaimDevice: val }

    if (!val) {
      newState.disableMeta = true
    } else {
      newState.disableMeta = false
    }

    this.setState(newState)
  }

  updateSeteableMeta = (val) => {
    const newState = { seteableMeta: val }

    if (val && !this.state.autoClaimDevice) {
      newState.autoClaimDevice = true
    }
    this.setState(newState)
  }

  setWifi = (part) => (evnt) => {
    let cmdline = this.state.cmdline
    if (!cmdline) {
      cmdline = []
    }
    const partIndex = cmdline.findIndex((v) => v.indexOf(wifiPartToCmd[part]) >= 0)
    const value = `${wifiPartToCmd[part]}=${evnt.target.value}`
    if (evnt.target.value === '' && partIndex >= 0) {
      cmdline = [...cmdline.slice(0, partIndex), ...cmdline.slice(partIndex + 1)]
    } else {
      partIndex >= 0 ? cmdline[partIndex] = value : cmdline.push(value)
    }

    this.setState((state) => ({
      ...state,
      cmdline: cmdline,
      wifi: {
        ...state.wifi,
        [part]: evnt.target.value
      }
    }))
  }

  render () {
    const username = this.props?.auth?.username

    if (this.state.status === STATUSES.LOADING) {
      return (
        <div className="row">
          <header className="col-12">
            <Breadcumbs
              steps={[
                {
                  to: `${userDashboardPath}/${username}`,
                  title: 'Dashboard'
                },
                {
                  to: '',
                  title: 'Download pantavisor'
                }
              ]}
            />
          </header>
          <section className="col-12 mt-10">
            <div className="row">
              <div className="col-md-6">
                <div className="form-group pb-2">
                  <label
                    htmlFor="source"
                  >
                    Image Source
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="source"
                    aria-describedby="sourceHelp"
                    placeholder="Enter your custom image source"
                    value={this.state.source}
                    onChange={this.setSource}
                  />
                  <small
                    id="sourceHelp"
                    className="form-text text-muted pl-1"
                  >
                    {'Any other Pantacor CI image json compatible URL.'}
                  </small>
                </div>
                <div className="pt-90 pb-90">
                  <Loading />
                </div>
              </div>
            </div>
          </section>
        </div>
      )
    }
    return (
      <div className="row">
        <header className="col-12">
          <Breadcumbs
            steps={[
              {
                to: `${userDashboardPath}/${username}`,
                title: 'Dashboard'
              },
              {
                to: '',
                title: 'Download pantavisor'
              }
            ]}
          />
        </header>
        <section className="col-12 mt-10">
          <div className="row">
            <div className="col-md-6">
              <div className="form-group pb-2">
                <label
                  htmlFor="source"
                >
                  Image Source
                </label>
                <input
                  type="url"
                  className="form-control"
                  id="source"
                  aria-describedby="sourceHelp"
                  placeholder="Enter your custom image source"
                  value={this.state.source}
                  onChange={this.setSource}
                />
                <small
                  id="sourceHelp"
                  className="form-text text-muted pl-1"
                >
                  {'You can use any other Pantacor CI image json compatible URL.'}
                </small>
              </div>
              <div className="form-group pb-2">
                <label
                  htmlFor="channelSelect"
                >
                  Select channel
                </label>
                <select
                  className="form-control"
                  id="channelSelect"
                  value={this.state.channel}
                  onChange={this.setChannel}
                >
                  {this.state.channels.map((o) => (
                    <option key={o}>
                      {o}
                    </option>
                  ))}
                </select>
              </div>
              {this.state.channel !== '' && this.state.devices[this.state.channel] && (
                <div className="form-group pb-2">
                  <label
                    htmlFor="deviceSelect"
                  >
                    Select device
                  </label>
                  <select
                    className="form-control"
                    id="deviceSelect"
                    onChange={this.selectDevice}
                  >
                    {this.state.devices[this.state.channel].map((o, index) => (
                      <option value={index} key={index}>
                        {getImageName(o)}
                      </option>
                    ))}
                  </select>
                </div>
              )}
              <CheckInput
                value={this.state.localExperience}
                onChange={this.updateLocalExperience}
              >
                {`Local experience enabled (see advanced cmdline configuration for more details)`}
              </CheckInput>
              <CheckInput
                value={this.state.autoClaimDevice}
                disabled={this.state.localExperience}
                onChange={this.updateAutoClaim}
              >
                Pair with this account on first boot
              </CheckInput>
              <CheckInput
                value={this.state.seteableMeta}
                disabled={this.state.disableMeta}
                onChange={this.updateSeteableMeta}
              >
                Preload device configuration (user-meta)
              </CheckInput>
              {this.state.seteableMeta && (
                <div className="form-group pb-3 pt-2 pl-25 pr-25">
                  <label
                    htmlFor="devicemeta"
                    className='label-with-pl-0'
                  >
                    Set device user-meta
                  </label>
                  <textarea
                    className="form-control"
                    id="devicemeta"
                    onChange={this.setMeta}
                    disabled={this.state.disableMeta}
                    defaultValue={this.getMeta()}
                  />
                  <div id="devicemetaHelp" className="form-text">
                    Write a full json for configuration
                  </div>
                </div>
              )}
              <>
                <CheckInput
                  value={this.state.seteableWifi}
                  onChange={this.updateSeteableWifi}
                >
                  Configure Wifi connection
                </CheckInput>
                <>
                </>
                {this.state.seteableWifi && (
                  <>
                    <div className="form-group pb-2 pt-2 pl-25 pr-25">
                      <label
                        htmlFor="ssid"
                        className='label-with-pl-0'
                      >
                        Wifi SSID
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="ssid"
                        aria-describedby="ssidHelp"
                        placeholder="Enter you Wifi SSID"
                        value={this.state.network.ssid}
                        onChange={this.setWifi('ssid')}
                      />
                    </div>
                    <div className="form-group pb-4 pl-25 pr-25">
                      <label
                        htmlFor="password"
                        className='label-with-pl-0'
                      >
                        Wifi Password
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="password"
                        aria-describedby="passwordHelp"
                        placeholder="Enter you Wifi password"
                        value={this.state.network.password}
                        onChange={this.setWifi('password')}
                      />
                    </div>
                    <div className="form-group pb-4 pl-25 pr-25">
                      <label
                        htmlFor="ip"
                        className='label-with-pl-0'
                      >
                        Wifi Ip Address (IP/MASK)
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="ip"
                        aria-describedby="ipHelp"
                        placeholder="Set your ip address"
                        value={this.state.network.ip}
                        onChange={this.setWifi('ip')}
                      />
                      <small id="sourceHelp" className="form-text text-muted pl-1">
                        {`Possible values: "dhcp" or "IP_ADDRESS/IP_MASK" (default: dhcp)`}
                      </small>
                    </div>
                  </>
                )}
              </>
              <CheckInput
                value={this.state.advanceMode}
                onChange={this.toggleAdvanceMode}
              >
                Advance cmdline configuration
              </CheckInput>
              {this.state.advanceMode && (
                <div className="form-group pb-3 pt-2 pl-25 pr-25">
                  <label
                    htmlFor="devicemeta"
                    className='label-with-pl-0'
                  >
                    Configure cmdline
                  </label>
                  <textarea
                    className="form-control"
                    id="devicemeta"
                    rows={4}
                    onChange={this.setCmdline}
                    value={this.getCmdline()}
                  />
                  <div id="devicemetaHelp" className="form-text">
                    One configuration per line
                  </div>
                </div>
              )}
              {!!this.state.selectedDevice && (
                <div className="form-group pb-2 mt-60">
                  <LoadingButton
                    loading={this.state.status === STATUSES.DOWNLOADING}
                    loadingChild={'Starting download...'}
                    className="btn btn-primary"
                    onClick={this.downloadImage}
                  >
                    Download {this.state.selectedDevice.target}
                  </LoadingButton>
                </div>
              )}
            </div>
          </div>
        </section>
      </div>
    )
  }
}

export default connect(
  (state) => state.auth,
  { DownloadImageWithAuth }
)(ImageDownloader)
