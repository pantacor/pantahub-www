/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from "react";
import SignUpForm from "../../molecules/SignUpForm/SignUpForm";
class SignUpPage extends Component {
  render() {
    return (
      <div className="signup container index-2 pt-6">
        <div className="row justify-content-center">
          <div className="col-lg-4 col-sm-10 col-md-8">
            <div className="form-registration-box">
              <div className="card">
                <div className="card-body">
                  <h1 key="title">Create Account</h1>
                  <h5 key="subtitle" className="text-muted">
                    Join PantacorHub and start rocking your device!
                  </h5>
                  <br />
                  <p key="help_text">
                    Please complete the following information to create your
                    account. You will get a confirmation email with a link you
                    should follow to finish the process.
                  </p>
                  <SignUpForm labels={false} size="default" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <br />
        <br />
      </div>
    );
  }
}

export default SignUpPage;
