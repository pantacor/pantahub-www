/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import { getApp } from '../../../store/applications/actions'
import { userDashboardPath } from '../../../router/routes'
import { GetEnv } from '../../../lib/const.helpers'

const getAppNameFromURL = (urlString) => {
  const url = new window.URL(urlString)

  return url.hostname
}
class Application extends Component {
  async componentDidMount () {
    await this.props.getApp(this.props.match.params.id)
  }

  getBase64Config = () => {
    const app = this.props.applications.currentApp

    const key = getAppNameFromURL(GetEnv('REACT_APP_API_URL'))
    const config = {
      key: key,
      client_id: app.prn,
      client_secret: app.secret,
      scopes: app.scopes.map(s => `${s.service}/${s.id}`),
      web_url: GetEnv('REACT_APP_WWW_URL'),
      api_url: GetEnv('REACT_APP_API_URL')
    }

    return window.btoa(JSON.stringify(config))
  }

  render () {
    const app = this.props.applications.currentApp
    return (
      <div className="thridpartyapp-detail">
        <div
          key="breadcrumbs"
          className="breadcrumb-sector row align-items-center"
        >
          <div className="col-8">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to={`${userDashboardPath}`}>Dashboard</Link>
              </li>
              <li className="breadcrumb-item">
                <Link to={`${userDashboardPath}/apps/`}>Applications</Link>
              </li>
              <li className="breadcrumb-item active">{app.nick}</li>
            </ol>
          </div>
        </div>
        <div className="row align-items-center">
          <div className="col-sm-10">
            <h1>
              {app.nick}
            </h1>
            {app.logo && app.logo !== '' && (
              <p className='mt-4 mb-4'>
                <img src={app.logo} style={{ maxWidth: '250px' }} alt="" />
              </p>
            )}
          </div>
          <div className="col-sm-2 text-right">
            <Link
              to={`${userDashboardPath}/apps/edit/${app.id}`}
              className="btn btn-primary"
            >
              Edit
            </Link>
          </div>
        </div>
        <div className="row mt-4">
          <div className="col-md-12">
            <p><b>ID:</b> {app.id}</p>
            <p><b>Name:</b> {app.name}</p>
            <p><b>Nick:</b> {app.prn}</p>
            <p><b>Type:</b> {app.type}</p>
            {app.secret && (<p><b>Secret:</b> {app.secret}</p>)}
            {app.secret && (
              <React.Fragment>
                <p><b>Base64 Encoded configuration:</b></p>
                <pre>
                  <code>
                    {this.getBase64Config()}
                  </code>
                </pre>
              </React.Fragment>
            )}
            <p>
              <b>Callback URLs:</b>
            </p>
            <ul>
              {app.redirect_uris.map((url, id) => (
                <li
                  key={id}
                >
                  {url}
                </li>
              ))}
            </ul>
            <p>
              <b>Scopes:</b>
            </p>
            <ul>
              {app.scopes.sort(s => s.required).map(scope => (
                <li
                  key={`${scope.service}${scope.id}`}
                >
                  {scope.service}/{scope.id}{' '}
                  {scope.description && (<span>({scope.description})</span>)}{' '}
                  {scope.required && (<b>required</b>)}
                </li>
              ))}
            </ul>
            {app.type === 'confidential' && app.exposed_scopes.length > 0 && (
              <React.Fragment>
                <p>
                  <b>Exposed Scopes:</b>
                </p>
                <ul>
                  {app.exposed_scopes.map(scope => (
                    <li
                      key={`${scope.service}${scope.id}`}
                    >
                      {scope.service}/{scope.id}{' '}
                      {scope.description && (<span>({scope.description})</span>)}{' '}
                      {scope.required && (<b>required</b>)}
                    </li>
                  ))}
                </ul>
              </React.Fragment>
            )}
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  state => ({
    applications: state.applications
  }),
  {
    getApp
  }
)(Application)
