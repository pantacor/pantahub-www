/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'

import { resolvePath } from '../../../lib/object.helpers'
import { TextInputWithClipboard } from '../../atoms/ClipboardFields/ClipboardFields'
import TruncatedText from '../../atoms/TruncatedText/TruncatedText'
import { GetPvrURL, GetWwwURL } from '../../../lib/const.helpers'

function UserDeviceInfo (props) {
  const { deviceId, device, disabled } = props
  const { history = {}, summary = {} } = device

  const currentStep = history.currentStep
  const sha = currentStep
    ? resolvePath(currentStep, 'state-sha', '')
    : resolvePath(summary, 'state-sha', '')
  const revision = currentStep ? currentStep.rev : null

  const cloneURL = history.lastRevision === revision
    ? `${GetPvrURL()}/${device['owner-nick']}/${device.nick}`
    : `${GetPvrURL()}/${device['owner-nick']}/${device.nick}/${revision}`

  const shareURL = history.lastRevision === revision
    ? `${GetWwwURL()}/u/${device['owner-nick']}/devices/${deviceId}`
    : `${GetWwwURL()}/u/${device['owner-nick']}/devices/${deviceId}/step/${revision}`

  return (
    <div className="device-detail-main">
      <div>
        <div className="row">
          <div className="col-md-12">
            <section className='pt-2 pb-2'>
              <header className='mb-2 fs-6 fw-light'>Device ID</header>
              <div>
                <span className="fw-bolder">{deviceId}</span>
              </div>
            </section>
            <section className='pt-2 pb-2'>
              <header className='mb-2 fs-6 fw-light'>Commit ID (Rev)</header>
              <div>
                <span className="fw-bolder">
                  {sha
                    ? (
                      [
                        <span className="progress-revision-commit" key="progress-revision-commit">
                          <TruncatedText text={sha} size={8} appendText=" "/>
                        </span>,
                        <span className="progress-revision-rev text-muted" key="progress-revision-rev">
                        ({revision})
                        </span>
                      ]
                    )
                    : (
                      <span className="progress-revision-rev">
                        {revision}
                      </span>
                    )}
                </span>
              </div>
            </section>
            <section className='pt-2 pb-2'>
              <header className='mb-2 fs-6 fw-light'>Clone URL</header>
              <div>
                <TextInputWithClipboard
                  value={cloneURL}
                  label="Clone URL"
                  disabled={disabled}
                />
              </div>
            </section>
            <section className='pt-2 pb-2'>
              <header className='mb-2 fs-6 fw-light'>Share URL</header>
              <div>
                <TextInputWithClipboard
                  value={shareURL}
                  label="Share URL"
                  disabled={disabled}
                />
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  )
}

export default UserDeviceInfo
