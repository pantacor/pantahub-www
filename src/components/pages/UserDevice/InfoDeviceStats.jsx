/*
 * Copyright (c) 2023 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { resolvePath } from '../../../lib/object.helpers'
import { SystemInfo } from '../../molecules/SystemInfo/SystemInfo'
import { StorateDetails } from '../../molecules/StorageDetails/StorageDetails'
import DynamicProgressMessages from '../../molecules/DynamicProgressMessages/DynamicProgressMessages'
import DeviceDeleteButton from '../../molecules/DeviceDeleteButton/DeviceDeleteButton'
import DevicePublishButton from '../../molecules/DevicePublishButton/DevicePublishButton'
import IsAuthorized from '../../molecules/IsAuthorized/IsAuthorized'
import RelativeTime from '../../atoms/RelativeTime/RelativeTime'
import RenderIf from '../../atoms/RenderIf/RenderIf'

function UserDeviceStats (props) {
  const { deviceId, device, username, token, disabled } = props
  const { history = {}, summary = {} } = device
  const currentStep = history.currentStep
  const status = currentStep
    ? resolvePath(currentStep, 'progress.status', '')
    : resolvePath(summary, 'status', '')
  const statusMsg = currentStep
    ? resolvePath(currentStep, 'progress.status-msg', '')
    : resolvePath(summary, 'status-msg', '')

  const lastSeen = summary.timestamp
  const lastModified = summary['step-time']

  return (
    <div className="device-detail-main">
      <div className="row">
        <IsAuthorized owner={device.owner}>
          <section className="col-md-12 pt-2 pb-2">
            <div className="row">
              <header className='col-md-6 fs-6 fw-light'>Actions</header>
              <div className="col-md-6 d-flex justify-content-start btn-group" >
                <DevicePublishButton
                  username={username}
                  token={token}
                  deviceId={deviceId}
                  isPublic={device.public || false}
                  disabled={disabled}
                />
                <DeviceDeleteButton
                  username={username}
                  token={token}
                  deviceId={deviceId}
                  label={'Delete'}
                  disabled={disabled}
                />
              </div>
            </div>
          </section>
        </IsAuthorized>
        <RenderIf condition={(status !== null || statusMsg !== null) && currentStep !== null }>
          <section className="col-md-12 pt-2 pb-2">
            <div className="row">
              <header className='col-md-6 fs-6 fw-light'>Status</header>
              <div
                className="col-md-6 d-flex justify-content-start"
              >
                <DynamicProgressMessages status={status} statusMsg={statusMsg} currentStep={currentStep} />
              </div>
            </div>
          </section>
        </RenderIf>
        <section className="col-md-12 pt-2 pb-2">
          <div className="row">
            <header className='col-md-6 fs-6 fw-light'>Last Modified</header>
            <div className="col-md-6 d-flex justify-content-start" >
              <strong>
                <RelativeTime when={lastModified} />
              </strong>
            </div>
          </div>
        </section>
        <section className="col-md-12 pt-2 pb-2">
          <div className="row">
            <header className='col-md-6 fs-6 fw-light'>Last Seen</header>
            <div className="col-md-6 d-flex justify-content-start" >
              <strong>
                <RelativeTime when={lastSeen} />
              </strong>
            </div>
          </div>
        </section>
      </div>
      <div className='row pt-1'>
        <div className='col-md-12 mb-3'>
          <StorateDetails className="row pl--10 pr--10" storage={{ ...props.device['device-meta'].storage }} />
        </div>
        <div className='col-md-12 mb-3'>
          <SystemInfo className="row pl--10 pr--10" info={{ ...props.device['device-meta'].sysinfo }} />
        </div>
      </div>
    </div>
  )
}

export default UserDeviceStats
