import React from 'react'
import { mount } from 'enzyme'
import { RequestNewPassword } from './RequestNewPassword'
// import createStore from '../../store/index'

describe('RequestNewPassword', () => {
  const onValid = jest.fn()
  it('Loading component', () => {
    const vm = mount(<RequestNewPassword onSave={onValid} />)
    expect(vm.exists()).toBe(true)
    expect(vm.find('.rnp .rnp__email').first().exists()).toBe(true)
    expect(vm.find('.rnp .rnp__submit').first().exists()).toBe(true)
    expect(vm.find('.rnp .rnp__submit').first().text()).toBe('Send it')

    vm.setProps({ loading: true })
    vm.render()

    expect(vm.find('.rnp .rnp__submit').first().html())
      .toBe('<TrackedButton type="submit" class="btn btn-primary rnp__submit" disabled=""><span class="mdi mdi-refresh pantahub-loading"></span>Send it</TrackedButton>')
  })

  it('Show error if email is empty', () => {
    const vm = mount(<RequestNewPassword onSave={onValid} />)
    vm.find('.rnp form').first().simulate('submit')
    vm.render()
    const error = vm.find('.rnp__email .invalid-feedback').first().text()
    const button = vm.find('.rnp .rnp__submit').first()

    expect(error).toBe(' Constraints not satisfied ')
    expect(button.props().disabled).toBe(true)
  })

  it('Show error if email is not a correct email', () => {
    const vm = mount(<RequestNewPassword onSave={onValid} email="not and email" />)
    vm.find('.rnp form').first().simulate('submit')
    vm.render()
    const error = vm.find('.rnp__email .invalid-feedback').first().text()
    const button = vm.find('.rnp .rnp__submit').first()

    expect(error).toBe(' Constraints not satisfied ')
    expect(button.props().disabled).toBe(true)
  })

  it('Pass and call onValid is email is correct', () => {
    const vm = mount(<RequestNewPassword onSave={onValid} email="test@email.com" />)
    vm.find('.rnp form').first().simulate('submit')
    vm.render()
    const error = vm.find('.rnp__email .invalid-feedback').first().text()
    const button = vm.find('.rnp .rnp__submit').first()

    expect(error.trim()).toBe('')
    expect(button.props().disabled).toBe(false)
    expect(onValid.mock.calls.length).toBe(1)
  })
})
