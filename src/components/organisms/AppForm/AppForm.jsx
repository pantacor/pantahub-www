/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { useCallback, useState, useEffect } from 'react'
import { STATES, initialState } from '../../../store/applications/reducers'
import { validate } from './validation'
import TextInput from '../../atoms/TextInput/TextInput'
import AddFromList from '../../molecules/AddFromList/AddFromList'
import SelectInput from '../../atoms/SelectInput/SelectInput'
import AddList from '../../molecules/AddList/AddList'
import ImageLinkOrBase64 from '../../molecules/ImageLinkOrBase64/ImageLinkOrBase64'
import { TrackedButton } from '../../atoms/Tracker/Tracker'

const ownScopesFields = [
  { name: 'id', label: 'Name', type: 'text', className: 'col-3' },
  { name: 'description', label: 'Description', type: 'text', className: 'col-8' }
]

const APP_LOGO_MAX_SIZE_KB = Number(
  window.env.REACT_APP_3RD_PARTY_APP_LOGO_MAX_SIZE_KB || '15'
)

export default function AppForm ({ currentApp, updateApp, status, scopes }) {
  const [state, setState] = useState({ ...initialState.currentApp, wasValidated: false, errors: [] })

  useEffect(() => {
    setState(s => ({
      ...s,
      ...currentApp
    }))
    return () => {}
  }, [currentApp])

  const onChange = (value) => (event) => {
    const input = event.target.value
    setState({
      ...state,
      [value]: input
    })
  }

  const submit = useCallback((event) => {
    event.preventDefault()
    const form = validate(state)
    if (form.valid) {
      updateApp(state, state.id)
      return
    }
    const errors = Object.values(form.errors).reduce((acc, v) => [...acc, ...v], [])
    setState({ ...state, errors: errors })
  }, [state, updateApp])

  const updateScope = (id, scope) => { state.scopes[id] = scope; setState(state) }
  const removeScope = id => { setState({ ...state, scopes: [...state.scopes.slice(0, id), ...state.scopes.slice(id + 1)] }) }

  const loading = status === STATES.IN_PROGRESS
  const wasValidatedClass = state.wasValidated ? 'col-md-8 was-validated' : 'col-md-8'
  const saveCreateMessage = state.id ? 'Save app' : 'Create app'

  return (
    <div className="app-form row justify-content-center">
      <form
        onSubmit={submit}
        noValidate={true}
        className={wasValidatedClass}
      >
        <ImageLinkOrBase64
          label="Logo (Optional)"
          name="logo"
          type="text"
          placeholder="Insert your application logo URL"
          className="form-control"
          value={state.logo}
          onChange={onChange('logo')}
          maximunSize={APP_LOGO_MAX_SIZE_KB}
        />
        <TextInput
          label="Application name"
          name="name"
          type="text"
          placeholder="Application Name"
          className="form-control"
          value={state.name}
          onChange={onChange('name')}
        />
        <TextInput
          label="Nickname"
          name="nick"
          type="text"
          placeholder="Application nick name (lowercase and with -)"
          className="form-control"
          value={state.nick}
          onChange={onChange('nick')}
        />
        <SelectInput
          label="Application Type"
          name="type"
          className="form-control"
          options={[{ value: '', label: 'Select One' }, { value: 'public', label: 'Public' }, { value: 'confidential', label: 'Confidential' }]}
          value={state.type}
          onChange={onChange('type')}
        />
        <hr/>
        <AddFromList
          values={state.scopes}
          optionsMap={{ getLabel: (o) => `${o.description} (${o.id})` }}
          onAdd={(v) => setState({ ...state, scopes: [...state.scopes, v] })}
          onUpdate={updateScope}
          onRemove={removeScope}
          getId={(v) => `${v.service}/${v.id}`}
          options={scopes}
          label="Reading scopes"
          name="scopes"
        />
        {state.type === 'confidential' && (
          <React.Fragment>
            <hr/>
            <AddList
              fields={ownScopesFields}
              onAdd={(v) => setState(s => ({ ...state, exposed_scopes: [...state.exposed_scopes, v] }))}
              onUpdate={(v) => setState(s => ({ ...state, exposed_scopes: v }))}
              onRemove={id => setState({ ...state, exposed_scopes: [...state.exposed_scopes.slice(0, id), ...state.exposed_scopes.slice(id + 1)] })}
              values={state.exposed_scopes}
              label="Exposed Scopes"
            />
          </React.Fragment>
        )}
        <hr/>
        <AddList
          fields={[{ name: null, label: 'URL', placeholder: 'New callback URL', type: 'text', className: 'col-11' }]}
          onAdd={(v) => { setState({ ...state, redirect_uris: [...state.redirect_uris, v.null] }) }}
          onUpdate={v => setState({ ...state, redirect_uris: v })}
          onRemove={id => setState({ ...state, redirect_uris: [...state.redirect_uris.slice(0, id), ...state.redirect_uris.slice(id + 1)] })}
          values={state.redirect_uris}
          label="Callback whitelist"
        />
        { state.errors.length > 0 && (
          <React.Fragment>
            <h6 className="text-danger">
              Sorry! We found 1 or more errors:
            </h6>
            <ul className="text-danger">
              {state.errors.map((e, i) => (
                <li key={`${i}-${e.type}`}>{e.message}</li>
              ))}
            </ul>
          </React.Fragment>
        )}
        <TrackedButton
          type="submit"
          className="btn btn-primary  btn-block mt-4"
          disabled={loading}
        >
          {loading
            ? (
              <span className="mdi mdi-refresh pantahub-loading" />
            )
            : (
              saveCreateMessage
            )}
        </TrackedButton>
      </form>
    </div>
  )
}
