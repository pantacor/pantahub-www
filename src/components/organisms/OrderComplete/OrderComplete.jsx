import React, { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { subscriptionsPath } from '../../../router/routes'
import { GetSubscriptionsAction, RefreshPaymentAction, RefreshSubscriptionAction } from '../../../store/billing/actions'
import Loading from '../../atoms/Loading/Loading'

export function OrderComplete () {
  const [loading, setLoading] = useState(true)
  const ref = useRef()
  const dispatch = useDispatch()
  const params = new URLSearchParams(window.location.search)
  const paymentID = params.get('payment_intent')
  const status = params.get('redirect_status')
  const payment = useSelector((state) => state.billing.payment)
  const subscription = useSelector((state) => state.billing.subscription)

  useEffect(() => {
    if (!payment && ref.current && !ref.current.loading) {
      setLoading(true)
      dispatch(RefreshPaymentAction(paymentID))
        .then((r) => r.ok && dispatch(RefreshSubscriptionAction(r.json.payment_of_id)))
        .then(() => dispatch(GetSubscriptionsAction()))
        .then(() => setLoading(false))
    }
  }, [paymentID])

  if (loading || !payment) {
    return (
      <div
        style={{ height: '70vh' }}
        className='d-flex justify-content-center align-items-center'>
        <Loading ref={ref} />
        <h6 className='text-center'>
          Loading your invoice...
        </h6>
      </div>
    )
  }

  return (
    <section ref={ref} className='pt-5 pb-5'>
      <header>
        <h1>Order {status}</h1>
      </header>
      <section>
        <header>
          <p>
            You have successfully subscribed to:&nbsp;
            <b>{subscription.product.name}</b>
          </p>
          <p>
            Your new plan will activate immediately so you can take full advantage of its benefits.
          </p>
          <p>Thank you for your order. We have emailed you the invoice - you can also download it on this page.</p>
        </header>
        <p className='mt-4'>
          <Link
            to={subscriptionsPath}
            className='btn btn-secondary mr-1'
            tabIndex="-1"
            role="button"
            aria-disabled="true"
          >
            View subscription
          </Link>
          <a
            href={payment.invoice.hosted_invoice_url}
            className='btn btn-primary'
            tabIndex="-1"
            role="button"
            aria-disabled="true"
            rel="noreferrer"
            target="_blank"
          >
            See invoice
          </a>
        </p>
      </section>
    </section>
  )
}
