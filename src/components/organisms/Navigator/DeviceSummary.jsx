/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'

import ReactMarkdown from 'react-markdown'

import Loading from '../../atoms/Loading/Loading'

import { deviceFetchFile } from '../../../store/devices/actions'
import UserDeviceInfo from '../../pages/UserDevice/Info'
import Parts from '../../molecules/Parts/Parts'
import { BspInfoWithTitle } from '../../molecules/BspInfo/BspInfo'
import { resolvePath } from '../../../lib/object.helpers'
import UserDeviceStats from '../../pages/UserDevice/InfoDeviceStats'
import UserNewDeviceMetadata from './NewMetadata'

class DeviceSummary extends Component {
  state = {
    filename: ''
  };

  componentDidMount () {
    const { device, token } = this.props
    const { state, rawState } = (device.history || {}).currentStep || {}
    // FIXME: improve readme filename regex
    const [readmeRawKey] =
      Object.entries(rawState || {}).find(
        ([key, value]) => key.match(/README\.md/) && value
      ) || []

    if (readmeRawKey) {
      const readmeState = state[readmeRawKey]
      if (readmeState && readmeState.url) {
        this.setState({ filename: readmeRawKey })
        this.props.deviceFetchFile(token, device.id, readmeState.url, readmeRawKey)
      }
    }
  }

  render () {
    const { navigator, device, deviceId, username, token, disabled } = this.props
    const { filename } = this.state
    const { files } = navigator
    const { loading, content, error } = (files[device.id] || {})[filename] || {}
    const { state } = resolvePath(device, 'history.currentStep', {})

    return loading
      ? (
        <Loading />
      )
      : (
        <React.Fragment>
          <div className='col-md-12 mb-4'>
            <div className="row">
              <div className="col-md-7">
                <UserDeviceInfo
                  device={device}
                  deviceId={deviceId}
                  username={username}
                  token={token}
                  disabled={disabled}
                />
              </div>
              <div className="col-md-5">
                <UserDeviceStats
                  device={device}
                  deviceId={deviceId}
                  username={username}
                  token={token}
                  disabled={disabled}
                />
              </div>
            </div>
          </div>
          {!error && content && (
            <div className="readme-wrapper">
              <div className="readme-inner-wrapper">
                <ReactMarkdown source={content} />
              </div>
            </div>
          )}
          <BspInfoWithTitle state={state} />
          <section className='mt-4'>
            <header>
              <h3>Components</h3>
            </header>
            <Parts {...this.props} />
          </section>
          <UserNewDeviceMetadata type="device-meta" flatten={true} {...this.props} />
        </React.Fragment>
      )
  }
}

export default connect(null, { deviceFetchFile })(DeviceSummary)
