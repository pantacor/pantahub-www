/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component, PureComponent } from 'react'
import { connect } from 'react-redux'
import uniqBy from 'lodash.uniqby'
import { FixedSizeList as List } from 'react-window'

import { resolvePath } from '../../../lib/object.helpers'
import { getStateKeys } from '../../molecules/Tiles/Tiles'

import {
  deviceSetLogsFilter,
  deviceFetchNewLogs
} from '../../../store/devices/actions'
import { GetEnv } from '../../../lib/const.helpers'
import { withRouter } from 'react-router'
import RenderIf from '../../atoms/RenderIf/RenderIf'

const extendedPalette = {
  PantacorPurple: {
    className: 'PantacorPurpleBg',
    color: 0x414262
  },
  Strikemaster: {
    className: 'StrikemasterBg',
    color: 0x7F587D
  },
  TurkishRose: {
    className: 'TurkishRoseBg',
    color: 0xBF6B85
  },
  Apricot: {
    className: 'ApricotBg',
    color: 0xF08B7C
  },
  Rajah: {
    className: 'RajahBg',
    color: 0xF5BB71
  },
  Amulet: {
    className: 'AmuletBg',
    color: 0x76A678
  },
  Valencia: {
    className: 'ValenciaBg',
    color: 0xD33D48
  }
}

const MAX_COLOR_RANDOMNESS = 0.3
const LOG_WRAPPER_CHECK_REFS_RATE = 100
const LOG_WRAPPER_CHECK_OVERSCAN_RATE = 500
const LOG_WRAPPER_MIN_HEIGHT = 400
const LOG_WRAPPER_ITEM_HEIGHT = 17
const LOG_WRAPPER_ENTRIES_OVERSCAN = 3000

function Circle (props) {
  const { radius, outline, color, className } = props
  const style = {
    width: `${2 * radius}px`,
    height: `${2 * radius}px`,
    borderRadius: `${radius}px`,
    display: 'inline-block'
  }
  if (outline) {
    style.border = `1px solid ${color}`
  } else {
    style.backgroundColor = color
  }
  return <i style={style} className={className}></i>
}

class LogEntry extends PureComponent {
  render () {
    const { entry, style, filters } = this.props
    const colorStyle = filters.plat.options[entry.plat].color.style
    return (
      <div key={entry.id} style={style} className={`log-row-header level-${entry.lvl.toLowerCase()}`}>
        <Circle radius={4} color={colorStyle.color} className='log-row-circle' aria-hidden='true'/>
        <span className='log-row-detail'>{entry.msg}</span>
      </div>
    )
  }
}

const PARAMS = {
  plat: { name: 'plat', title: 'Platforms', visible: true },
  src: { name: 'src', title: 'Sources', visible: true },
  lvl: { name: 'lvl', title: 'Levels', visible: false } // Not fully implemented
}

class ConsoleFilterParamOption extends Component {
  constructor (props) {
    super(props)
    const { optionState } = props
    const childrenOptions = optionState.options ? Object.entries(optionState.options) : null

    this.state = {
      // if the option has children and any child checked state is different than the parent, then the filter will be expanded by default
      expanded: childrenOptions && childrenOptions.some(([childOptionName, childOptionState]) => optionState.checked !== childOptionState.checked)
    }
  }

  onExpandClick = (e) => {
    this.setState(state => ({ expanded: !state.expanded }))
  }

  onCheckedChange = (e, optionState, childOptionState = null) => {
    const { onConsoleFilterChanged } = this.props
    onConsoleFilterChanged(e, optionState, childOptionState)
  }

  render () {
    const { optionName, optionState } = this.props
    const { expanded } = this.state
    const childrenOptions = optionState.options ? Object.entries(optionState.options) : null
    return <div className='console-filter-option'>
      <div className={'console-filter-option-header'} style={optionState.color.style}>
        <span onClick={e => this.onCheckedChange(e, optionState)}>
          <Circle radius={6} color={optionState.color.style.color} outline={!optionState.checked} aria-hidden='true'/>
          <span className={'console-filter-option-header-name'}>{` ${optionName}`}</span>
        </span>
        {childrenOptions.length > 0 && <span onClick={e => this.onExpandClick(e)} className='float-right'>
          <i className={`mdi mdi-${expanded ? 'chevron-up' : 'chevron-down'}`} aria-hidden='true' />
        </span>}
      </div>
      {expanded && childrenOptions.length > 0 &&
        <div className='console-filter-option-children'>
          {childrenOptions.map(([childOptionName, childOptionState]) =>
            <div key={childOptionName} onClick={e => this.onCheckedChange(e, optionState, childOptionState)} >
              <Circle radius={6} color={optionState.color.style.color} outline={!childOptionState.checked} aria-hidden='true'/>
              <span className='console-filter-option-child-name'>{` ${childOptionName}`}</span>
            </div>)}
        </div>}
    </div>
  }
}

class ConsoleFilterParam extends Component {
  render () {
    const { paramState, onConsoleFilterChanged } = this.props
    const options = (paramState.options ? Object.entries(paramState.options) : null)
    if (options.length > 0) {
      return <React.Fragment>
        {options.map(([optionName, optionState], index) =>
          <ConsoleFilterParamOption key={optionName} optionName={optionName} optionState={optionState} onConsoleFilterChanged={onConsoleFilterChanged}/>
        )}
      </React.Fragment>
    } else {
      return null
    }
  }
}

class ConsoleFilters extends Component {
  render () {
    const { filters, onConsoleFilterChanged } = this.props
    return (
      <React.Fragment>
        {Object.entries(filters).map(([paramName, paramState]) =>
          <ConsoleFilterParam key={paramName} paramTitle={PARAMS[paramName].title} paramName={paramName} paramState={paramState}
            onConsoleFilterChanged={onConsoleFilterChanged} />
        )}
      </React.Fragment>
    )
  }
}

class ConsoleLogWrapper extends PureComponent {
  constructor (props) {
    super(props)
    this.state = {
      filtersHeight: 0
    }
    this.wrapperRef = React.createRef()
    this.checkRefsInterval = null
    this.checkOverscanInterval = null
    this.firstVisibleItem = null
    this.firstVisibleItemIndex = null
    this.firstVisibleItemScrollTop = null
    this.checkTopOverscanFirst = true
  }

  // Prepare html events after the 1st render has been done
  componentDidMount () {
    this.checkRefsInterval = setInterval(this.checkRefs, LOG_WRAPPER_CHECK_REFS_RATE)
    this.checkOverscanInterval = setInterval(this.checkOverscan, GetEnv('REACT_APP_REFRESH_RATE', LOG_WRAPPER_CHECK_OVERSCAN_RATE))
    const wrapper = this.wrapperRef.current
    wrapper.onwheel = this.onWheel
    wrapper.onscroll = this.onScroll
  }

  // Remove existing intervals
  componentWillUnmount () {
    if (this.checkRefsInterval !== null) {
      clearInterval(this.checkRefsInterval)
    }
    this.checkRefsInterval = null
    if (this.checkOverscanInterval !== null) {
      clearInterval(this.checkOverscanInterval)
    }
    this.checkOverscanInterval = null
  }

  checkRefs = () => {
    // Checks if the Visible height of the filters panel is different from the last known to trigger a resize to the log wrapper height
    const { filtersRef } = this.props
    const filtersHeight = filtersRef && filtersRef.current ? filtersRef.current.clientHeight : 0
    if (this.state.filtersHeight !== filtersHeight) {
      this.setState({ filtersHeight })
    }
  }

  checkOverscan = () => {
    const { logs, device, getData } = this.props
    const { loading, entries } = logs
    const wrapper = this.wrapperRef.current
    // device should be loaded and app should not be fetching log entries
    if (!device.hasOwnProperty('history') || loading) {
      return
    }
    if (!entries || entries.length === 0) {
      getData(true, false) // start getting data
    } else if (wrapper && wrapper.clientHeight > 0) {
      // alternate between checking the top or bottom overscan priority
      this.checkTopOverscanFirst = !this.checkTopOverscanFirst
      if (this.checkTopOverscanFirst) {
        if (!this.checkTopOverscan()) {
          this.checkBottomOverscan()
        }
      } else {
        if (!this.checkBottomOverscan()) {
          this.checkTopOverscan()
        }
      }
    }
  }

  checkTopOverscan = () => {
    const { getData } = this.props
    if (this.firstVisibleItemIndex < LOG_WRAPPER_ENTRIES_OVERSCAN) {
      getData(true, false) // get past data
      return true
    } else {
      return false
    }
  }

  checkBottomOverscan = () => {
    const { logs, getData } = this.props
    const { entries } = logs
    if (entries.length - this.firstVisibleItemIndex < LOG_WRAPPER_ENTRIES_OVERSCAN) {
      getData(true, true) // get future data
      return true
    } else {
      return false
    }
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    const wrapper = this.wrapperRef.current
    const { filteredLogs, stickToTop, stickToBottom } = this.props
    const fastScrollToPreviousFirstVisibleItem = () => {
      if (this.firstVisibleItem) {
        const newfirstVisibleItemIndex = filteredLogs.findIndex(item => item.id === this.firstVisibleItem.id)
        if (newfirstVisibleItemIndex > -1 && newfirstVisibleItemIndex !== this.firstVisibleItemIndex) {
          wrapper.scrollTop = newfirstVisibleItemIndex * LOG_WRAPPER_ITEM_HEIGHT + this.firstVisibleItemScrollTop
        }
      }
    }
    fastScrollToPreviousFirstVisibleItem()
    if (stickToTop) {
      wrapper.scrollTop = 0
    } else if (stickToBottom) {
      wrapper.scrollTop = wrapper.scrollHeight
    }
  }

  onScroll = (event) => {
    const { filteredLogs } = this.props
    const wrapper = this.wrapperRef.current
    // Find currently show item
    if (wrapper) {
      this.firstVisibleItemIndex = Math.floor(wrapper.scrollTop / LOG_WRAPPER_ITEM_HEIGHT)
      this.firstVisibleItemScrollTop = wrapper.scrollTop % LOG_WRAPPER_ITEM_HEIGHT
      this.firstVisibleItem = (this.firstVisibleItemIndex >= 0 && this.firstVisibleItemIndex < filteredLogs.length ? filteredLogs[this.firstVisibleItemIndex] : null)
    }
  }

  onWheel = (event) => {
    const { stickToTop, stickToBottom, onUnstick } = this.props
    const wrapper = this.wrapperRef.current
    if (wrapper && wrapper.clientHeight > 0) {
      if (stickToTop || stickToBottom) {
        onUnstick()
      }
    }
  }

  render () {
    const { className, filteredLogs, filters } = this.props
    const { filtersHeight } = this.state
    const listHeight = Math.max(LOG_WRAPPER_MIN_HEIGHT, filtersHeight)
    const itemHeight = LOG_WRAPPER_ITEM_HEIGHT

    // Overscanning slightly can reduce or prevent a flash of empty space when a user first starts scrolling.
    const overscanCount = 2 * Math.floor(listHeight / itemHeight)

    return (
      <div className={`${className} log-wrapper-container`}>
        <List
          className={'log-wrapper'}
          outerRef={this.wrapperRef}
          height={listHeight}
          width={'100%'}
          itemCount={filteredLogs.length}
          itemSize={itemHeight}
          overscanCount={overscanCount}
        >
          {({ index, style }) => (
            <LogEntry entry={filteredLogs[index]} style={style} filters={filters} />
          )}
        </List>
      </div>
    )
  }
}

class UserDeviceNavigatorLogs extends PureComponent {
  constructor (props) {
    super(props)
    this.state = {
      // initialize the state as an object with empty objects
      filters: ['plat'].reduce((acc, paramName) => ({ ...acc, [paramName]: { paramName, options: {}, parent: null } }), {}),
      showFilters: false,
      stickToTop: false,
      stickToBottom: true
    }
    this.filtersRef = React.createRef()
    this.filterLogs(props.logs.entries)
    this.colorPalette = Object.entries(extendedPalette).map(([name, colorDef]) =>
      ({
        intColor: colorDef.color,
        rgbColor: this.intToRgb(colorDef.color),
        style: this.createStyle(colorDef.color)
      }))
    this.paletteIndex = -1
  }

  filterLogs (logsEntries, force = false) {
    const { filters } = this.state
    const params = Object.entries(filters)
    this.uniqueLogs = uniqBy(
      logsEntries || [],
      (element) => element.id
    )

    if (force) {
      this.filteredLogs = this.uniqueLogs
    } else {
      // apply state filters
      this.filteredLogs = this.uniqueLogs.filter(logEntry => {
        // level 0 filter
        return params.every(([paramName, paramState]) => {
          // If a param has options, the corresponding option must be selected
          const checked = Object.entries(paramState.options).some(([optionName, optionState]) => {
            let innerChecked = !optionState.options
            if (optionState.options) {
              innerChecked = Object.entries(optionState.options).some(([childOptionName, childOptionState]) =>
                logEntry[optionState.paramName] === childOptionName && childOptionState.checked
              )
            }
            return logEntry[paramName] === optionName &&
              optionState.checked &&
              innerChecked
          })
          return !paramState.options || checked
        })
      })
    }

    return this.filteredLogs
  }

  createStyle (intColor) {
    return {
      color: `#${intColor.toString(16)}`
    }
  }

  nextPaletteColor () {
    this.paletteIndex++
    while (this.paletteIndex >= this.colorPalette.length) {
      // create a new color. use the extended palette as template
      const paletteColor = this.colorPalette[this.paletteIndex % Object.keys(extendedPalette).length]
      const newRgbColor = this.randomizeColor(paletteColor.rgbColor)
      const newIntColor = this.rgbToInt(newRgbColor)
      this.colorPalette.push({ intColor: newIntColor, rgbColor: newRgbColor, style: this.createStyle(newIntColor) })
    }
    return this.colorPalette[this.paletteIndex]
  }

  intToRgb (int) {
    return { r: (int & 0xFF0000) >> 16, g: (int & 0xFF00) >> 8, b: int & 0xFF }
  }

  rgbToInt ({ r, g, b }) {
    return (r << 16) + (g << 8) + b
  }

  randomizeColorChannel (channelValue) {
    return Math.max(0, Math.min(255, Math.floor(channelValue + (Math.random() > 0.5 ? 1 : -1) * (Math.random() * MAX_COLOR_RANDOMNESS * channelValue))))
  }

  randomizeColor (rgbColor) {
    return Object.entries(rgbColor).reduce((acc, [key, value]) => ({ ...acc, [key]: this.randomizeColorChannel(value) }), {})
  }

  async componentDidMount () {
    this.onLogsFilterChange(this.state.filters)
    const now = new Date()
    await this.getData(false, false, now.toISOString())
    await this.updateFilters(true)
  }

  getData = async (refresh, fromFuture, since) => {
    const { logs } = this.props
    const { loading } = logs
    if (loading) {
      return
    }
    const { token, device } = this.props
    this.props.deviceFetchNewLogs(token, device.id, refresh, fromFuture, since)
  }

  updateFilters = (force = false) => {
    this.filterLogs(this.props.logs.entries, force)
    const uniqueLogs = this.uniqueLogs
    const rawState = resolvePath(this.props, 'device.history.currentStep.rawState', {})
    let updateState = false
    const platformsNames = getStateKeys(rawState).map(stateKey => stateKey.title)
    const newFilters = { ...this.state.filters }
    const platformsState = newFilters.plat
    const SRC_PARAM = PARAMS.src
    const addPlatform = (platformName) => {
      if (!platformsState.options.hasOwnProperty(platformName)) {
        updateState = true
        platformsState.options[platformName] = {
          optionName: platformName,
          checked: true,
          color: this.nextPaletteColor(),
          paramName: SRC_PARAM.name,
          options: {},
          level: 0
        }
      }
    }
    platformsNames.forEach(addPlatform)
    uniqueLogs.forEach(logEntry => {
      const { plat, src } = logEntry
      let platState = null
      addPlatform(plat)
      platState = platformsState.options[plat]
      if (!platState.options.hasOwnProperty(src)) {
        updateState = true
        platState.options[src] = { optionName: src, checked: true, level: 1 }
      }
    })
    // only update state if new options where found
    if (updateState && !force) {
      const newState = { filters: newFilters }
      this.setState(newState)
    }
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if (prevProps.logs.entries !== this.props.logs.entries || prevState.filters !== this.state.filters) {
      this.updateFilters()
    }
  }

  setCheckedRecursive = (node, checked) => {
    if (node) {
      if (node.hasOwnProperty('checked')) {
        node.checked = checked
      }
      if (typeof node === 'object') {
        Object.entries(node).forEach(([key, value]) => {
          this.setCheckedRecursive(value, checked)
        })
      }
    }
  }

  onConsoleFilterChanged = (e, optionState, childOptionState = null) => {
    const newFilters = { ...this.state.filters }
    let updateApiFilters = false
    if (childOptionState === null) {
      // a parent option state was changed. make its children match its parent
      optionState.checked = !optionState.checked
      this.setCheckedRecursive(optionState, optionState.checked)
      // Only level 0 nodes affect api filters. level 1 nodes are made in the frontend
      updateApiFilters = true
    } else {
      // a children option state was changed
      childOptionState.checked = !childOptionState.checked

      if (childOptionState.checked && !optionState.checked) {
        // if child is checked and parent is not checked, then check the parent
        optionState.checked = true
        updateApiFilters = true
      } else if (!childOptionState.checked && optionState.options &&
        !Object.entries(optionState.options).some(([childOptionName, childOptionState]) => childOptionState.checked)) {
        // if child is unchecked and no other sibling is checked, then uncheck the parent
        optionState.checked = false
        updateApiFilters = true
      }
    }
    // update internal state
    this.setState({ filters: newFilters })
    // update api filters state if needed
    if (updateApiFilters) {
      this.onLogsFilterChange(newFilters)
    }
  }

  onClearSelectionClick = (event) => {
    const newFilters = { ...this.state.filters }
    const unCheckRecursive = node => {
      if (node) {
        if (node.hasOwnProperty('checked')) {
          node.checked = false
        }
        if (typeof node === 'object') {
          Object.entries(node).forEach(([key, value]) => {
            unCheckRecursive(value)
          })
        }
      }
    }
    unCheckRecursive(newFilters)
    this.setState({ filters: newFilters })
    this.onLogsFilterChange(newFilters)
  }

  onLogsFilterChange (newFilters) {
    const rev = resolvePath(this.props, 'match.params.revision', null)
    const apiFilters = Object.entries(newFilters).reduce((filtersAcc, [paramName, paramState]) => {
      const options = paramState && paramState.options ? Object.entries(paramState.options) : []
      if (options.every(([optionName, optionState]) => optionState.checked)) {
        // for api call, if every option is selected, the parameter is omitted
        return filtersAcc
      } else {
        // for api call, if not every option is selected, then the parameter must be sent with all selected options in comma separated string
        return {
          ...filtersAcc,
          [paramName]: options.filter(([optionName, optionState]) => optionState.checked)
            .reduce((optionsAcc, [optionName, optionState]) => optionsAcc + (optionsAcc ? ',' : '') + optionName, '')
        }
      }
    }, {})

    if (rev) {
      apiFilters.rev = rev
    }
    // request a new API query with new filters
    this.props.deviceSetLogsFilter(apiFilters)
    // request new data inmediatly
    this.getData(false, false)
  }

  onToggleFiltersClick = (event) => {
    this.setState(state => ({ showFilters: !state.showFilters }))
  }

  onStickToTopClick = () => {
    this.setState(state => ({
      stickToTop: !state.stickToTop,
      stickToBottom: false
    }))
  }

  onStickToBottomClick = () => {
    this.setState(state => ({
      stickToTop: false,
      stickToBottom: !state.stickToBottom
    }))
  }

  onUnstick = () => {
    this.setState({
      stickToTop: false,
      stickToBottom: false
    })
  }

  render () {
    const { showFilters, filters, stickToTop, stickToBottom } = this.state
    const filteredLogs = this.filteredLogs
    return (
      <div className='console'>
        <div className='row console-top'>
          <div className='col-8'>
            {showFilters &&
                <div onClick={this.onClearSelectionClick} className='console-top-dark-button'>
                  <i className='mdi mdi-close-circle' aria-hidden='true'></i>
                  <span>{' clear selection'}</span>
                </div>}
            <div onClick={this.onToggleFiltersClick} className='console-top-light-button'><span>{`${showFilters ? 'hide' : 'show'} filter`}</span></div>
          </div>
          <div className='col-4 d-flex justify-content-end'>
            <i className={`mdi mdi-chevron-double-up log-wrapper-btn ${stickToTop ? 'active' : ''}`}
              onClick={this.onStickToTopClick} aria-hidden='true' />
            <i className={`mdi mdi-chevron-double-down log-wrapper-btn ${stickToBottom ? 'active' : ''}`}
              onClick={this.onStickToBottomClick} aria-hidden='true' />
          </div>
        </div>
        <div className='row'>
          <RenderIf condition={showFilters}>
            <div ref={this.filtersRef} className='col-lg-3 col-md-4 log-filters'>
              <ConsoleFilters
                filteredLogs={filteredLogs}
                filters={filters}
                onConsoleFilterChanged={this.onConsoleFilterChanged}
                {...this.props}
              />
            </div>
          </RenderIf>
          <ConsoleLogWrapper
            className={`${showFilters ? 'col-lg-9 col-md-8' : 'col-12'}`}
            filteredLogs={filteredLogs}
            filtersRef={this.filtersRef}
            getData={this.getData}
            onUnstick={this.onUnstick}
            {...this.props}
            {...this.state}
          />
        </div>
      </div>
    )
  }
}

export default connect(
  state => ({
    logs: state.devs.logs
  }),
  {
    deviceFetchNewLogs,
    deviceSetLogsFilter
  }
)(withRouter(UserDeviceNavigatorLogs))
