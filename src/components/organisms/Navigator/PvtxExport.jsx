/*
 * Copyright (c) 2024 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component, memo } from "react";
import { PartDescription } from "../../molecules/PartDescription/PartDescription";
import { CreateFsDescription, FindByID } from "../../../lib/state";
import { Button } from "../../atoms/Button/Button";
import { connect } from "react-redux";
import { STATES } from "../../../store/exports/reducer";
import { EXPORTS_FETCH_SELECTION } from "../../../store/exports/types";
import { GetExportedPartsAction } from "../../../store/exports/actions";
import { FileSize } from "../Files/FilesFromState";
import { resolvePath } from "../../../lib/object.helpers";
import RenderIf from "../../atoms/RenderIf/RenderIf";

export const PartDescriptionMemo = memo(
	PartDescription,
	(prevProps, nextProps) => {
		return (
			JSON.stringify(resolvePath(prevProps, "desc", {})) ===
			JSON.stringify(resolvePath(nextProps, "desc", {}))
		);
	},
);

export const setToArray = (_key, value) =>
	value instanceof Set ? [...value] : value;

class PvtxExportFactory extends Component {
	state = {
		selected: new Set(),
		all: false,
		parts: {},
	};

	constructor(props) {
		super(props);

		this.partsRef = React.createRef();
	}

	componentDidMount() {
		this.setState({
			...this.state,
			parts: CreateFsDescription(
				this.props.device.history.currentStep.rawState,
			),
		});
	}

	shouldComponentUpdate(nextProps, nextState, nextContext) {
		const sameDeviceState =
			JSON.stringify(this.props.device.history.currentStep.rawState) ===
			JSON.stringify(nextProps.device.history.currentStep.rawState);
		const sameExports =
			JSON.stringify(this.props.exports) ===
			JSON.stringify(nextProps.exports);

		return !sameExports || !sameDeviceState || this.state !== nextState;
	}

	onRemove = () => undefined;
	onSelect = (id) => (add) => {
		const pkg = FindByID(this.state.parts, id);
		const selected = this.state.selected;
		if (!pkg) {
			return;
		}
		if (add) {
			selected.add(id);
			(pkg.included || []).forEach((p) => selected.add(p));
		} else {
			selected.delete(id);
			(pkg.included || []).forEach((p) => selected.delete(p));
		}
		this.setState({ ...this.state, selected });
	};

	toggleSelectAll = (event) => {
		event.preventDefault();
		if (this.partsRef.current) {
			const allInputs = this.partsRef.current.querySelectorAll("input");
			if (allInputs) {
				allInputs.forEach((e) => {
					if (!this.state.all && !e.checked) {
						e.click();
					}
					if (this.state.all && e.checked) {
						e.click();
					}
				});
				this.setState({ ...this.state, all: !this.state.all });
			}
		}
	};

	downloadSelection = (event) => {
		event.preventDefault();
		this.props.GetExportedPartsAction(
			this.props.device["owner-nick"],
			this.props.device.nick,
			this.props.device.history.currentStep.rev,
			`${this.props.device.nick}.tar.gz`,
			this.state.selected.size > 0
				? { parts: [...this.state.selected] }
				: undefined,
		);

		this.setState({ ...this.state, selected: new Set() });
	};

	render() {
		if (
			this.props.exports.status ===
			STATES[EXPORTS_FETCH_SELECTION].IN_PROGRESS
		) {
			return (
				<section className="pt-6 pb-6">
					<header className="d-flex flex-column align-items-center">
						<h4>Generating compressed file</h4>
						<p>
							<small>This can take a while...</small>
						</p>
					</header>
					<div className="d-flex justify-content-center align-items-center">
						<div
							className="spinner-grow spinner-grow-sm"
							role="status"
						>
							<span className="visually-hidden">Loading...</span>
						</div>
					</div>
					<div className="d-flex flex-column justify-content-center align-items-center mt-4">
						<small>Downloaded</small>
						<br />
						<RenderIf
							condition={
								this.props.exports.progress &&
								this.props.exports.progress.received
							}
						>
							<p>
								<FileSize
									bytes={
										this?.props?.exports?.progress?.received
									}
								/>
							</p>
						</RenderIf>
					</div>
				</section>
			);
		}

		return (
			<div>
				<RenderIf condition={!!this.state.parts}>
					<header className="d-flex justify-content-end mb-4">
						<Button
							className="btn btn-link"
							onClick={this.toggleSelectAll}
						>
							{this.state.all ? "Remove selection" : "Select all"}
						</Button>
						<Button
							className="btn btn-primary"
							onClick={this.downloadSelection}
						>
							{this.state.selected.size > 0
								? `Export selected parts`
								: `Export all`}
						</Button>
					</header>
					<section ref={this.partsRef}>
						{Object.keys(this.state.parts).map((key) => (
							<PartDescriptionMemo
								key={key}
								title={key}
								desc={this.state.parts[key]}
								onDelete={this.onRemove}
								onSelect={this.onSelect}
							/>
						))}
					</section>
					<footer className="d-flex justify-content-end mt-4">
						<Button
							className="btn btn-link"
							onClick={this.toggleSelectAll}
						>
							{this.state.all ? "Remove selection" : "Select all"}
						</Button>
						<Button
							className="btn btn-primary"
							onClick={this.downloadSelection}
						>
							{this.state.selected.size > 0
								? `Export selected parts`
								: `Export all`}
						</Button>
					</footer>
				</RenderIf>
			</div>
		);
	}
}

export const PvtxExport = connect((state) => ({ exports: state.exports }), {
	GetExportedPartsAction,
})(PvtxExportFactory);
