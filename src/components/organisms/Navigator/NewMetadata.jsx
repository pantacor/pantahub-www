/*
 * Copyright (c) 2023 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { flatObject } from '../../../lib/object.helpers'
import { capitalize } from '../../../lib/utils'
import { GetFileSize } from '../Files/FilesFromState'

const metasToSize = [
  'storage.free',
  'storage.real_free',
  'storage.reserved',
  'storage.total',
  'sysinfo.bufferram',
  'sysinfo.freehigh',
  'sysinfo.freeram',
  'sysinfo.freeswap',
  'sysinfo.sharedram',
  'sysinfo.totalram',
  'sysinfo.totalhigh',
  'sysinfo.totalswap'
]

function UserNewDeviceNavigatorMetadataCol (props) {
  let { currentKey: key, value, convertStorage = false } = props

  if (convertStorage && metasToSize.indexOf(key) >= 0) {
    value = GetFileSize(value)
  }

  return (
    <div className="col-sm-12 col-md-4">
      <section>
        <div className="row">
          <h6 className="fw-bold fs-9em">{key}</h6>
        </div>
        <div>
          <p className="fw-normal font-monospace">{value.toString()}</p>
        </div>
      </section>
    </div>
  )
}

function UserNewDeviceMetadata (props) {
  const {
    device,
    type = 'user-meta'
  } = props
  // const loading = navigator.setMeta.loading;
  const meta = device[type] || {}
  const title = capitalize(type.replace('-', ' '))
  const flattenMeta = flatObject(meta)

  const metaEntries = Object.entries(flattenMeta).sort((a, b) => {
    if (a[0] > b[0]) {
      return 1
    }
    if (a[0] < b[0]) {
      return -1
    }
    // a must be equal to b
    return 0
  })
  const metaEntriesCount = metaEntries.length

  return (
    <div>
      <h3>{title}</h3>
      <div className='row mt-4'>
        {metaEntries.map(([key, value], index) => {
          return (
            <UserNewDeviceNavigatorMetadataCol
              convertStorage={true}
              key={key}
              currentKey={key} className="meta-title"
              value={value}
            />
          )
        })}
        {metaEntriesCount === 0 && (
          <div className="bg-light">
            <div className="text-center" >
              Metadata is empty
            </div>
          </div>
        )}
      </div>
    </div>
  )
}

export default UserNewDeviceMetadata
