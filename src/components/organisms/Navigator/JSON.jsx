/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { devicePostEdit } from '../../../store/devices/actions'
import RenderIf from '../../atoms/RenderIf/RenderIf'
import { JSONDisplayEditable } from '../../atoms/ClipboardFields/JSONDisplayEditable'
import Loading from '../../atoms/Loading/Loading'

class UserDeviceNavigatorJSON extends Component {
  newValue = (state = {}, commit = '') => {
    const payload = {
      'state': state,
      'commit-msg': commit,
      'meta': {},
      'rev': -1
    }
    this.props.devicePostEdit(payload)
  }

  render () {
    const { device, disabled } = this.props
    const { rawState } = (device.history || {}).currentStep || {}

    if (this.props.stepPosting.loading) {
      return (
        <>
          <Loading />
          <div className='text-center'>
            Deploying your changes
          </div>
        </>
      )
    }

    return (
      <React.Fragment>
        <RenderIf condition={this.props.stepPosting.error}>
          <div>Error</div>
        </RenderIf>
        <JSONDisplayEditable value={rawState} disabled={disabled} saveValue={this.newValue} />
      </React.Fragment>
    )
  }
}

export default connect(
  (state) => ({
    stepPosting: state.devs.stepPosting
  }),
  { devicePostEdit }
)(UserDeviceNavigatorJSON)
