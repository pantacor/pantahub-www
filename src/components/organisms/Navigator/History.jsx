/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from "react";
import sortBy from "lodash.sortby";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import TruncatedText from "../../atoms/TruncatedText/TruncatedText";
import { userDashboardPath } from "../../../router/routes";
import { classForDevStatus } from "../../../lib/utils";
import {
  devicePostRev,
  getDeviceStepsAction,
  markDeviceStepAsCancelled,
  markDeviceStepAsWontGo,
} from "../../../store/devices/actions";
import { STATES } from "../../../store/devices/reducer";
import { TrackedButton } from "../../atoms/Tracker/Tracker";
import {
  ReduceSignatures,
  getDeviceStep,
  markStepAsCancel,
  markStepAsWontGo,
} from "../../../services/devices.service";
import { SignedIcon } from "../../atoms/SignedIcon/SignedIcon";
import { GetEnv } from "../../../lib/const.helpers";

function Release({
  step,
  device,
  username,
  loadingState,
  deployStep,
  markStepAsWontGo,
  markStepCancelled,
}) {
  const current = step.rev === ((device.history || {}).currentStep || {}).rev;
  // const deployed = step.rev === array[0].rev
  const status = (step.progress || {}).status || "";
  const signatures = Object.entries(step.state).reduce(ReduceSignatures, {
    include: [],
    exclude: [],
  });
  const timestamp = new Date(step["step-time"]).toLocaleString();
  return (
    <tr
      style={{ verticalAlign: "middle" }}
      className={current ? "bg-light font-weight-bold" : ""}
    >
      <td>
        {signatures.include.length > 0 && (
          <Link
            to={`${userDashboardPath}/${username}/devices/${device.id}/step/${step.rev}/files`}
            className="btn btn-sm btn-default"
            title="View revision details"
          >
            <SignedIcon />
          </Link>
        )}
      </td>
      <td>
        <TruncatedText text={step["state-sha"]} size={8} appendText=" " />(
        {step.rev}){current ? "*" : ""}
      </td>
      <td>{timestamp}</td>
      <td>
        <span className={`badge badge-${classForDevStatus(status)}`}>
          {status}
        </span>
      </td>
      <td style={{ maxWidth: "calc(100vw - 60vw)" }}>
        <pre style={{ marginBottom: 0 }}>{step["commit-msg"] || ""}</pre>
      </td>
      <td>
        <div className="btn-group" role="group" aria-label="Actions">
          <TrackedButton
            className="btn btn-sm btn-outline-dark"
            disabled={loadingState === STATES.IN_PROGRESS}
            onClick={deployStep(step)}
            title="Deploy this revision"
          >
            <i className="mdi mdi-rocket mdi-rotate-315" aria-hidden="true" />
          </TrackedButton>
          <TrackedButton
            className="btn btn-sm btn-outline-dark"
            disabled={loadingState === STATES.IN_PROGRESS}
            onClick={markStepCancelled(step)}
            title="Cancel this revision"
          >
            <i className="mdi mdi-cancel" aria-hidden="true" />
          </TrackedButton>
          <TrackedButton
            className="btn btn-sm btn-outline-dark"
            disabled={loadingState === STATES.IN_PROGRESS}
            onClick={markStepAsWontGo(step)}
            title="Mark this revision as won't go"
          >
            <i className="mdi mdi-alert-circle-outline" aria-hidden="true" />
          </TrackedButton>
          <Link
            to={`${userDashboardPath}/${username}/devices/${device.id}/step/${step.rev}`}
            className="btn btn-sm btn-outline-dark"
            title="View revision details"
          >
            <i className="mdi mdi-chevron-right" aria-hidden="true" />
          </Link>
        </div>
      </td>
    </tr>
  );
}
class UserDeviceNavigatorHistory extends Component {
  constructor(props) {
    super(props);
    this.timerId = null;
    this.previousLoading = false;
    this.previousPolling = false;
  }

  componentDidMount() {
    window.requestAnimationFrame(() => {
      this.stopPulling();
      this.getData().then(() => this.startPulling());
    });
  }

  componentWillUnmount() {
    this.stopPulling();
  }

  deployStep = (step) => async () => {
    const payload = {
      state: step.state,
      meta: step.meta,
      rev: -1,
      "commit-msg": `Redeploy ${step["commit-msg"]}`,
    };
    const resp = await getDeviceStep(
      this.props.token,
      this.props.device.id,
      step.rev,
    );
    if (resp.ok) {
      payload.state = resp.json.state;
    }

    return this.props.devicePostRev(this.props.device.id, payload);
  };

  markStepAsWontGo = (step) => async () => {
    const resp = await this.props.markDeviceStepAsWontGo(
      this.props.device.id,
      step.rev,
    );

    setTimeout(() => {
      this.getData();
    }, 1000);

    return resp.json;
  };

  markStepCancelled = (step) => async () => {
    const resp = await this.props.markDeviceStepAsCancelled(
      this.props.device.id,
      step.rev,
    );

    setTimeout(() => {
      this.getData();
    }, 1000);

    return resp.json;
  };

  getData = () => {
    return this.props.getDeviceStepsAction(this.props.device.id);
  };

  startPulling = () => {
    const intervalSize = Number(GetEnv("REACT_APP_REFRESH_RATE", "3000"));
    this.timerId = setInterval(() => {
      this.getData(true);
    }, intervalSize);
  };

  stopPulling = () => {
    if (this.timerId !== null) {
      window.clearInterval(this.timerId);
    }
    this.timerId = null;
  };

  render() {
    const { device, username, loadingState } = this.props;
    const sortedReleases = sortBy((device.history || {}).steps || [], [
      (s) => -1 * s.rev,
    ]);
    return (
      <div className="table-responsive">
        <table className="table table-borderless table-striped table-responsive table-no-first-border">
          <thead>
            <tr>
              <th />
              <th>Commit ID (Rev)</th>
              <th>Timestamp</th>
              <th>Status</th>
              <th>Commit Message</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {sortedReleases.map((step) => (
              <Release
                key={step.rev}
                step={step}
                device={device}
                username={username}
                loadingState={loadingState}
                deployStep={this.deployStep}
                markStepAsWontGo={this.markStepAsWontGo}
                markStepCancelled={this.markStepCancelled}
              />
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default connect(
  (state, ownProps) => ({
    loadingState: state.devs.deploy[ownProps.device.id],
    token: state.auth.token,
  }),
  {
    devicePostRev,
    markDeviceStepAsWontGo,
    markDeviceStepAsCancelled,
    getDeviceStepsAction,
  },
)(UserDeviceNavigatorHistory);
