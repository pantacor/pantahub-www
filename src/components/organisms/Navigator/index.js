/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from "react";

import UserDeviceNavigatorFiles from "./Files";
import UserDeviceNavigatorJSON from "./JSON";
import UserDeviceNavigatorMetadata from "./EditableMetada";
import UserDeviceNavigatorHistory from "./History";
import UserDeviceNavigatorLogs from "./Logs";
import { useRouteMatch, Route, Switch } from "react-router";
import { NavLink } from "react-router-dom";
import DeviceSummary from "./DeviceSummary";
import { Pvtx } from "./Pvtx";

export const tabs = [
	{
		id: "",
		label: "Summary",
		title: null,
		exact: true,
		components: [DeviceSummary],
	},
	{
		id: "/files",
		label: "Files",
		title: null,
		exact: false,
		components: [UserDeviceNavigatorFiles],
	},
	// {
	// 	id: "/json",
	// 	label: "State Description",
	// 	title: null,
	// 	exact: true,
	// 	components: [UserDeviceNavigatorJSON],
	// },
	{
		id: "/configuration",
		label: "Configuration",
		title: null,
		exact: true,
		components: [UserDeviceNavigatorMetadata],
	},
	{
		id: "/releases",
		label: "Releases",
		title: null,
		exact: true,
		components: [UserDeviceNavigatorHistory],
	},
	{
		id: "/logs",
		label: "Logs",
		title: null,
		exact: true,
		components: [UserDeviceNavigatorLogs],
	},
	{
		id: "/manage",
		label: "Manage",
		title: null,
		exact: true,
		components: [Pvtx],
	},
];

function UserDeviceNavigator(props) {
	const { path, url } = useRouteMatch();
	const { navigator, activeTab, device, username, token, dispatch } = props;

	return (
		<div className="device-navigator">
			<header>
				<h3>Device Navigator</h3>
			</header>
			<div className="row">
				<div className="col-md-12">
					<ul
						className="nav nav-tabs nav-fill flex-column flex-sm-row"
						role="tablist"
					>
						{tabs.map((tab) => (
							<li className="nav-item" key={tab.id}>
								<NavLink
									exact={tab.exact}
									to={`${url}${tab.id}`}
									className="btn btn-link btn-block nav-link"
									activeClassName="active"
									id={`tab-${tab.id}`}
									event="pageview"
									payload={{
										"Page Path":
											"user/device/tab-navigation",
										value: `${tab.id}`,
									}}
									aria-controls={`tab-${tab.id}`}
									aria-expanded={tab.id === activeTab}
								>
									{tab.label}
								</NavLink>
							</li>
						))}
					</ul>

					<div className="tab-content">
						<Switch>
							{tabs.map((tab) => (
								<Route
									exact={tab.exact}
									path={`${path}${tab.id}`}
									key={tab.id}
									id={`tab-content-${tab.id}`}
									className={`tab-pane fade show ${
										tab.id === activeTab ? "active" : ""
									}`}
								>
									{tab.components.map((TabComponent, key) => {
										return (
											<TabComponent
												key={key}
												device={device}
												username={username}
												token={token}
												dispatch={dispatch}
												navigator={navigator}
											/>
										);
									})}
								</Route>
							))}
						</Switch>
					</div>
				</div>
			</div>
		</div>
	);
}

export default React.memo(UserDeviceNavigator);
