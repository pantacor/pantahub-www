/*
 * Copyright (c) 2024 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component, memo, useRef } from "react";
import { PartDescription } from "../../molecules/PartDescription/PartDescription";
import { CreateFsDescription, FindByID } from "../../../lib/state";
import { Button } from "../../atoms/Button/Button";
import { connect } from "react-redux";
import { STATES } from "../../../store/exports/reducer";
import { EXPORTS_FETCH_SELECTION } from "../../../store/exports/types";
import { GetExportedPartsAction } from "../../../store/exports/actions";
import { FileSize } from "../Files/FilesFromState";
import { resolvePath } from "../../../lib/object.helpers";
import RenderIf from "../../atoms/RenderIf/RenderIf";
import FileUpload from "../../molecules/FileUpload/FileUpload";
import { parseTar } from "../../../lib/tarparser";
import { safeAwait } from "../../../lib/safeawait";
import { mergeState, removeState } from "../../../lib/statemerger";
import ReactDiffViewer, { DiffMethod } from "react-diff-viewer";
import { createObject, putObject } from "../../../services/objects.service";
import { AlertMessage } from "../../atoms/AlertMessage/AlertMessage";
import { postTrails } from "../../../services/devices.service";
import TextInput from "../../atoms/TextInput/TextInput";

export const PartDescriptionMemo = memo(
	PartDescription,
	(prevProps, nextProps) => {
		return (
			JSON.stringify(resolvePath(prevProps, "desc", {})) ===
			JSON.stringify(resolvePath(nextProps, "desc", {}))
		);
	},
);

export const setToArray = (_key, value) =>
	value instanceof Set ? [...value] : value;

const STATUSES = {
	UPLOAD_PROCESS: "UPLOAD_PROCESS",
	UPLOAD_DONE: "UPLOAD_DONE",
	UPLOAD_ERROR: "UPLOAD_ERROR",
	CREATE_REVISION: "CREATE_REVISION",
	REVISION_CREATED: "REVISION_CREATED",
};

class PvtxFactory extends Component {
	state = {
		selected: new Set(),
		all: false,
		parts: {},
		transaction: null,
		objects: new Map(),
		status: null,
		message: "",
	};

	constructor(props) {
		super(props);

		this.partsRef = React.createRef();
	}

	componentDidMount() {
		this.setState({
			...this.state,
			parts: CreateFsDescription(
				this.props.device.history.currentStep.rawState,
			),
		});
	}

	shouldComponentUpdate(nextProps, nextState, nextContext) {
		const sameDeviceState =
			JSON.stringify(this.props.device.history.currentStep.rawState) ===
			JSON.stringify(nextProps.device.history.currentStep.rawState);
		const sameExports =
			JSON.stringify(this.props.exports) ===
			JSON.stringify(nextProps.exports);

		return !sameExports || !sameDeviceState || this.state !== nextState;
	}

	onRemove = (part) => () => {
		const new_transaction = removeState(this.state.transaction, part);
		this.setState({
			...this.state,
			transaction: new_transaction,
			parts: CreateFsDescription(new_transaction),
		});
	};

	onAdd = async (bin) => {
		const [files, err] = await safeAwait(parseTar(bin));
		if (err != null) {
			return {
				error: err.toString(),
			};
		}

		const objects = new Map();
		let new_transaction = {};
		for (const file of files) {
			if (file.name.includes("objects/")) {
				file.sha = file.name.replace("objects/", "");
				objects.set(file.sha, file);
			}

			if (file.name === "json") {
				try {
					const patch_string = new TextDecoder().decode(file.data);
					const patch = JSON.parse(patch_string);
					new_transaction = mergeState(this.state.transaction, patch);
				} catch (e) {
					console.error(e);
				}
			}
		}

		for (const key in new_transaction) {
			if (objects.has(new_transaction[key])) {
				const file = objects.get(new_transaction[key]);
				file.file_name = key;
				objects.set(file.sha, file);
			}
		}

		this.setState({
			...this.state,
			objects: objects,
			transaction: new_transaction,
			parts: CreateFsDescription(new_transaction),
		});

		return files;
	};

	onSelect = (id) => (add) => {
		const pkg = FindByID(this.state.parts, id);
		const selected = this.state.selected;
		if (!pkg) {
			return;
		}
		if (add) {
			selected.add(id);
			(pkg.included || []).forEach((p) => selected.add(p));
		} else {
			selected.delete(id);
			(pkg.included || []).forEach((p) => selected.delete(p));
		}
		this.setState({ ...this.state, selected });
	};

	toggleSelectAll = (event) => {
		event.preventDefault();
		if (this.partsRef.current) {
			const allInputs = this.partsRef.current.querySelectorAll("input");
			if (allInputs) {
				allInputs.forEach((e) => {
					if (!this.state.all && !e.checked) {
						e.click();
					}
					if (this.state.all && e.checked) {
						e.click();
					}
				});
				this.setState({ ...this.state, all: !this.state.all });
			}
		}
	};

	downloadSelection = (event) => {
		event.preventDefault();
		this.props.GetExportedPartsAction(
			this.props.device["owner-nick"],
			this.props.device.nick,
			this.props.device.history.currentStep.rev,
			`${this.props.device.nick}.tar.gz`,
			this.state.selected.size > 0
				? { parts: [...this.state.selected] }
				: undefined,
		);

		this.setState({ ...this.state, selected: new Set() });
	};

	begingTransaction = () => {
		this.setState({
			...this.state,
			transaction: JSON.parse(
				JSON.stringify(this.props.device.history.currentStep.rawState),
			),
		});
	};

	cancelTransaction = () => {
		this.setState({
			...this.state,
			transaction: null,
			parts: CreateFsDescription(
				this.props.device.history.currentStep.rawState,
			),
		});
	};

	commitTransaction = async () => {
		if (this.state.objects.size == 0) {
			this.createRevision();
			return;
		}

		this.setState({
			...this.state,
			status: STATUSES.UPLOAD_PROCESS,
		});

		for (const [_, object] of this.state.objects) {
			const payload = {
				size: object.size.toString(),
				sha256sum: object.sha,
				objectname: object.file_name,
				"mime-type": "application/octet-stream",
			};
			// if the device already exists it will respond
			// with the existing object
			let [response, err] = await safeAwait(
				createObject(
					this.props.token,
					this.props.deviceId,
					this.props.device.history.lastRevision,
					payload,
				),
			);
			if (response.status === 409) {
				const sizeint = response.json?.sizeint;
				if (
					sizeint !== undefined &&
					(sizeint === 0 || sizeint !== object.size)
				) {
					response.ok = true;
				} else {
					object.percentage = 100;
					object.done = true;
					let alldone = true;
					this.state.objects.forEach((o) => {
						alldone = alldone && !!o.done;
					});

					if (alldone) {
						this.createRevision();
						return;
					}

					this.setState({
						...this.state,
						objects: this.state.objects,
					});
					continue;
				}
			}

			if (!response.ok) {
				this.setState({ ...this.state, err: response.json });
				return;
			}

			if (err != null) {
				this.setState({ ...this.state, err: err });
				return;
			}

			putObject(
				response.json["signed-puturl"],
				object.size,
				object.data,
				(percentage) => {
					object.percentage = percentage;
					this.setState({
						...this.state,
						objects: this.state.objects,
					});
				},
				() => {
					object.done = true;
					let alldone = true;
					this.state.objects.forEach((o) => {
						alldone = alldone && o.done;
					});

					if (alldone) {
						this.createRevision();
					} else {
						this.setState({
							...this.state,
							objects: this.state.objects,
						});
					}
				},
				(err) => {
					console.log("err ", err);
					this.setState({
						...this.state,
						err: err,
					});
				},
			);
		}
	};

	createRevision = async () => {
		this.setState({
			...this.state,
			status: STATUSES.CREATE_REVISION,
		});
		const payload = {
			state: this.state.transaction,
			meta: {
				build: "hub pvtx revision",
			},
			rev: -1,
			"commit-msg": this.state.message,
		};

		const [resp, err] = await safeAwait(
			postTrails(this.props.token, this.props.deviceId, payload),
		);
		if (err != null) {
			console.log(err);
			return;
		}

		if (!resp.ok) {
			console.log(resp.json);
			return;
		}

		this.setState({
			...this.state,
			transaction: null,
			status: STATUSES.REVISION_CREATED,
		});
	};

	isDownloading = () => {
		return (
			this.props.exports.status ===
			STATES[EXPORTS_FETCH_SELECTION].IN_PROGRESS
		);
	};

	isUploading = () => {
		return this.state.status === STATUSES.UPLOAD_PROCESS;
	};

	isRevisionCreated = () => {
		return this.state.status === STATUSES.REVISION_CREATED;
	};

	isCreatingRevision = () => {
		return this.state.status === STATUSES.CREATE_REVISION;
	};

	updateMessage = (event) => {
		this.setState({
			message: event.target.value,
		});
	};

	render() {
		if (this.isDownloading()) {
			return <DownloadProgress {...this.props.exports} />;
		}

		if (this.isUploading()) {
			return <UploadingObjects {...this.state} />;
		}

		if (this.isCreatingRevision()) {
			return <CreatingRevision />;
		}

		return (
			<div>
				<RenderIf condition={this.isRevisionCreated()}>
					<section className="row">
						<section className="col-12">
							<AlertMessage msg="Revision created!" />
						</section>
					</section>
				</RenderIf>
				<header className="d-flex justify-content-between mb-4">
					<section className="d-flex justify-content-start">
						<RenderIf condition={!this.state.transaction}>
							<Button
								className="btn btn-info"
								onClick={this.begingTransaction}
							>
								Begin Transaction
							</Button>
						</RenderIf>
					</section>
					<section className="d-flex justify-content-end ">
						<Menu
							all={this.state.all}
							size={this.state.selected.size}
							hasTransaction={!!this.state.transaction}
							toggleSelectAll={this.toggleSelectAll}
							downloadSelection={this.downloadSelection}
							begingTransaction={this.begingTransaction}
							cancelTransaction={this.cancelTransaction}
							commitTransaction={this.commitTransaction}
							updateMessage={this.updateMessage}
						/>
					</section>
				</header>
				<RenderIf
					condition={!!this.state.parts && !this.state.transaction}
				>
					<section ref={this.partsRef}>
						{Object.keys(this.state.parts).map((key) => (
							<PartDescriptionMemo
								key={key}
								title={key}
								desc={this.state.parts[key]}
								onSelect={this.onSelect}
							/>
						))}
					</section>
				</RenderIf>
				<RenderIf condition={!!this.state.transaction}>
					<PvtxTransaction
						json={this.props.device.history.currentStep.rawState}
						transaction={this.state.transaction}
						parts={this.state.parts}
						partsRef={this.partsRef}
						onRemove={this.onRemove}
						onSelect={this.onSelect}
						onAdd={this.onAdd}
					/>
				</RenderIf>
				<footer className="d-flex justify-content-end mt-4">
					<Menu
						all={this.state.all}
						size={this.state.selected.size}
						hasTransaction={!!this.state.transaction}
						toggleSelectAll={this.toggleSelectAll}
						downloadSelection={this.downloadSelection}
						begingTransaction={this.begingTransaction}
						cancelTransaction={this.cancelTransaction}
						commitTransaction={this.commitTransaction}
						updateMessage={this.updateMessage}
					/>
				</footer>
			</div>
		);
	}
}

const PvtxTransaction = ({
	parts = {},
	json = {},
	transaction = {},
	partsRef,
	onRemove,
	onSelect,
	onAdd,
}) => {
	const json_string = JSON.stringify(json, null, 2);
	const transaction_string = JSON.stringify(transaction, null, 2);

	return (
		<section>
			<div className="pb-4 pt-4">
				<Upload onFile={onAdd} />
			</div>
			<section ref={partsRef}>
				{Object.keys(parts).map((key) => (
					<PartDescriptionMemo
						key={key}
						title={key}
						desc={parts[key]}
						onDelete={onRemove}
					/>
				))}
			</section>
			<RenderIf condition={json_string !== transaction_string}>
				<section className="pt-2 pb-2">
					<h4 className="pt-2 pb-2">Transaction diff</h4>
					<div className="row">
						<div className="col-md-12 overflow-scroll">
							<ReactDiffViewer
								compareMethod={DiffMethod.WORDS_WITH_SPACE}
								oldValue={json_string}
								newValue={transaction_string}
								splitView={false}
							/>
						</div>
					</div>
				</section>
			</RenderIf>
		</section>
	);
};

const Upload = ({ onFile }) => {
	return (
		<div className="d-flex flex-column align-items-center justify-content-center">
			<FileUpload
				onFile={onFile}
				name="parts"
				type="file"
				message="Drag or click to upload new part"
				pictureStyle={{
					cursor: "pointer",
					width: "400px",
					padding: "1em",
					display: "flex",
					flexDirection: "column",
					alignItems: "center",
					justifyContent: "center",
				}}
				style={{
					backgroundColor: "rgba(0,0,0,0.05)",
					border: "rgba(0,0,0,0.2) 1px solid",
					display: "flex",
					flexDirection: "column",
					alignItems: "center",
					justifyContent: "center",
					padding: "1em",
					marginTop: "0",
					overflow: "hidden",
				}}
			/>
		</div>
	);
};

const DownloadProgress = (props) => {
	return (
		<section className="pt-6 pb-6">
			<header className="d-flex flex-column align-items-center">
				<h4>Generating compressed file</h4>
				<p>
					<small>This can take a while...</small>
				</p>
			</header>
			<div className="d-flex justify-content-center align-items-center">
				<div className="spinner-grow spinner-grow-sm" role="status">
					<span className="visually-hidden">Loading...</span>
				</div>
			</div>
			<div className="d-flex flex-column justify-content-center align-items-center mt-4">
				<small>Downloaded</small>
				<br />
				<RenderIf condition={props.progress && props.progress.received}>
					<p>
						<FileSize bytes={props.progress?.received} />
					</p>
				</RenderIf>
			</div>
		</section>
	);
};

const Menu = ({
	hasTransaction,
	size,
	all,
	toggleSelectAll,
	downloadSelection,
	begingTransaction,
	cancelTransaction,
	commitTransaction,
	updateMessage,
}) => {
	return (
		<>
			<RenderIf condition={!hasTransaction}>
				<Button className="btn btn-link" onClick={toggleSelectAll}>
					{all ? "Remove selection" : "Select all"}
				</Button>
				<Button className="btn btn-primary" onClick={downloadSelection}>
					{size > 0 ? `Export selected parts` : `Export device`}
				</Button>
			</RenderIf>
			<RenderIf condition={hasTransaction}>
				<Button
					className="btn btn-outline-danger"
					onClick={cancelTransaction}
				>
					Cancel Transaction
				</Button>{" "}
				<CommitMessageBtn
					className="ml-1 btn btn-primary"
					title="Enter a commit message"
					confirmBtnTxt="Commit Transaction"
					onConfirm={commitTransaction}
					onChange={updateMessage}
				>
					Commit Transaction
				</CommitMessageBtn>
			</RenderIf>
		</>
	);
};

const CreatingRevision = () => {
	return (
		<section className="pt-6 pb-6">
			<header className="d-flex flex-column align-items-center">
				<h4>Creating Revision</h4>
				<p>
					<small>This can take a while...</small>
				</p>
			</header>
			<div className="d-flex justify-content-center align-items-center">
				<div className="spinner-grow spinner-grow-sm" role="status">
					<span className="visually-hidden">Loading...</span>
				</div>
			</div>
		</section>
	);
};

const UploadingObjects = ({ objects }) => {
	if (objects.size == 0) {
		return (
			<section className="pr-2 pt-4 pb-4">
				<header className="d-flex flex-column">
					<h4>Posting revision...</h4>
				</header>
			</section>
		);
	}
	return (
		<section className="pr-2 pt-4 pb-4">
			<header className="d-flex flex-column align-items-center">
				<h4>Uploading objects</h4>
				<p>
					<small>This can take a while...</small>
				</p>
			</header>
			<div className="d-flex justify-content-center align-items-center">
				<div className="spinner-grow spinner-grow-sm" role="status">
					<span className="visually-hidden">Loading...</span>
				</div>
			</div>
			<div className="row mt-4">
				{[...objects.values()].map((o) => (
					<div key={o.sha} className="col-12 mt-2">
						<p>
							{o.file_name}
							&nbsp;&nbsp; (<FileSize bytes={o.size} />)
						</p>
						<div
							className="progress"
							role="progressbar"
							aria-label={`Uploading ${o.file_name}`}
							aria-valuenow={o.percentage}
							aria-valuemin="0"
							aria-valuemax="100"
						>
							<div
								className="progress-bar"
								style={{ width: `${o.percentage}%` }}
							>
								{o.percentage}%
							</div>
						</div>
					</div>
				))}
			</div>
		</section>
	);
};

const CommitMessageBtn = (props) => {
	const {
		delay = 400,
		closeBtnTxt = "Close",
		confirmBtnTxt = "Confirm",
		btnClassName = "btn btn-sm btn-light btn-link",
		closeClasses = "btn btn-secondary btn-sm",
		confirmClases = "btn btn-primary btn-sm",
		title = "Remove",
		onClose = () => {},
		onConfirm = () => {},
		onChange = () => {},
		...extras
	} = props;

	const [open, setOpen] = React.useState(false);
	const formRef = React.useRef();

	const onCloseHandler = () => {
		setOpen(false);
		const timeout = setTimeout(() => {
			onClose();
		}, delay);
		return () => {
			clearTimeout(timeout);
		};
	};

	const onConfirmHandler = () => {
		if (!formRef.current) {
			return;
		}
		const form = formRef.current;
		const valid = form.checkValidity();
		if (!valid) {
			return;
		}
		setOpen(false);
		const timeout = setTimeout(() => {
			onConfirm();
		}, delay);
		return () => {
			clearTimeout(timeout);
		};
	};

	if (!open) {
		return (
			<Button
				title={title}
				type="button"
				className={btnClassName}
				aria-label="Commit Message"
				onClick={() => setOpen(true)}
				{...extras}
			>
				{props.children}
			</Button>
		);
	}

	return (
		<div
			className={`modal fade ${open ? "show" : ""}`}
			tabIndex={-1}
			style={{ display: open ? "block" : "none" }}
		>
			<div className="modal-dialog modal-dialog-centered">
				<div className="modal-content">
					<div className="modal-header">
						<h5 className="modal-title">{props.title}</h5>
						<button
							type="button"
							className="btn-close"
							data-bs-dismiss="modal"
							onClick={onCloseHandler}
							aria-label="Close"
						></button>
					</div>
					<div className="modal-body">
						<form ref={formRef}>
							<TextInput
								label="Commit message"
								name="message"
								onChange={onChange}
								required={true}
							/>
						</form>
					</div>
					<div className="modal-footer">
						<button
							type="button"
							onClick={onCloseHandler}
							className={closeClasses}
							data-bs-dismiss="modal"
						>
							{closeBtnTxt}
						</button>
						<button
							type="button"
							onClick={onConfirmHandler}
							className={confirmClases}
						>
							{confirmBtnTxt}
						</button>
					</div>
				</div>
			</div>
		</div>
	);
};

export const Pvtx = connect((state) => ({ exports: state.exports }), {
	GetExportedPartsAction,
})(PvtxFactory);
