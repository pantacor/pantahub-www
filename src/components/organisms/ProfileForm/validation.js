/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import ValidaJs from 'valida-js'
import { FileSize } from '../../../lib/validators.helpers'

const PROFILE_PICTURE_MAX_SIZE_KB = Number(
  window.env.REACT_APP_PROFILE_PICTURE_MAX_SIZE_KB || '1000'
)

ValidaJs.validators.size = FileSize

const rules = ValidaJs.rulesCreator(ValidaJs.validators, [
  {
    name: 'bio',
    type: 'maxLength',
    stateMap: 'bio',
    compareWith: 100
  },
  {
    name: 'company',
    type: 'maxLength',
    stateMap: 'company',
    compareWith: 50
  },
  {
    name: 'website',
    type: 'maxLength',
    stateMap: 'website',
    compareWith: 50
  },
  {
    name: 'github',
    type: 'maxLength',
    stateMap: 'github',
    compareWith: 50
  },
  {
    name: 'gitlab',
    type: 'maxLength',
    stateMap: 'gitlab',
    compareWith: 50
  },
  {
    name: 'picture',
    type: 'size',
    stateMap: 'picture',
    compareWith: PROFILE_PICTURE_MAX_SIZE_KB
  }
])

export const validate = (state) => ValidaJs.validate(rules, state)
