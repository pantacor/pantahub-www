/*
 * Copyright (c) 2021 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { describe } from 'jest-circus'
import { DeviceState } from '../../../../mocks/device.state'
import { GetExtension, ReadStateFolder } from '../functions'

describe('Files from device state', () => {
  describe('Get Extension', () => {
    it('if file doesn\'t have extension', () => {
      const ext = GetExtension('_config/blah')
      expect(ext).toBe('')
    })

    it('If file has extension', () => {
      const ext = GetExtension('_config/blah.json')
      expect(ext).toBe('json')
    })

    it('If file has serveral extension', () => {
      const ext = GetExtension('_config/blah.my.type.of.json')
      expect(ext).toBe('json')
    })
  })

  describe('Convertion Functions', () => {
    it('Get root', () => {
      const folder = ReadStateFolder(DeviceState, '')
      expect(folder).toStrictEqual([
        { key: '_config', isFolder: true },
        { key: '_hostconfig', isFolder: true },
        { key: 'adguard', isFolder: true },
        { key: 'awconnect', isFolder: true },
        { key: 'bitwardenrs', isFolder: true },
        { key: 'bsp', isFolder: true },
        { key: 'openvpn', isFolder: true },
        { key: 'pv-avahi', isFolder: true },
        { key: 'pvr-sdk', isFolder: true },
        { key: 'tailscale', isFolder: true },
        { key: 'wireguard-vpn', isFolder: true },
        { key: 'awall.json', isFolder: false },
        { key: 'network-mapping.json', isFolder: false },
        { key: 'storage-mapping.json', isFolder: false },
        { key: 'awall2pvmwall', isFolder: false }
      ])
    })

    it('Get _config folder content', () => {
      const folder = ReadStateFolder(DeviceState, '_config')
      expect(folder).toStrictEqual([
        { key: 'awconnect', isFolder: true },
        { key: 'bitwardenrs', isFolder: true },
        { key: 'openvpn', isFolder: true },
        { key: 'pv-avahi', isFolder: true },
        { key: 'tailscale', isFolder: true }
      ])
    })

    it('Get pvr-sdk folder content', () => {
      const folder = ReadStateFolder(DeviceState, 'pvr-sdk')
      expect(folder).toStrictEqual([
        { key: 'lxc.container.conf', isFolder: false },
        { key: 'root.squashfs', isFolder: false },
        { key: 'root.squashfs.docker-digest', isFolder: false },
        { key: 'run.json', isFolder: false },
        { key: 'src.json', isFolder: false }
      ])
    })
  })
})
