/*
 * Copyright (c) 2021 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import sortBy from 'lodash.sortby'
import { ReduceSignatures } from '../../../services/devices.service'

export function ReadStateFolder (state = {}, route = '') {
  const signatures = Object.entries(state).reduce(ReduceSignatures, { include: [], exclude: [] })
  const folder = Array.from(Object.keys(state).reduce((acc, key) => {
    if (key.indexOf('#') >= 0) { return acc }

    const reg = new RegExp(`^${route}.*`)
    if (reg.test(key)) {
      const rest = key.replace(route, '').split('/')
      const value = rest[0] === '' ? rest[1] : rest[0]
      const isFolder = rest[rest.length - 1] !== value
      const mapKey = !value ? route : value
      const sigs = signatures.include.reduce((acc, e) => {
        if (e.pattern(key)) {
          acc.add(e.sig)
        }
        return acc
      }, new Set())
      const f = acc.get(mapKey) || { key: mapKey, isFolder: isFolder, sigs: sigs }
      if (sigs.size > 0) {
        f.sigs = f.sigs.add(...sigs)
      }
      acc.set(mapKey, f)
    }

    return acc
  }, new Map()))
  const folderElements = Array.from(folder).map(([_, value]) => {
    value.sigs = Array.from(value.sigs)
    return value
  })
  return sortBy(folderElements, [
    (f = { key: '', isFolder: false }) => f.isFolder ? -1 : 1,
    (f = { key: '' }) => f.key.indexOf('.') < 0,
    (f = { key: '', isFolder: false }) => f.key
  ])
}

export function GetExtension (route = '') {
  return route.slice((route.lastIndexOf('.') - 1 >>> 0) + 2)
}
