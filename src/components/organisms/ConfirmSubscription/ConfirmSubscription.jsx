import React, { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link, useHistory, useRouteMatch } from 'react-router-dom'
import { resolvePath } from '../../../lib/object.helpers'
import { subscriptionsNewPath, subscriptionsPath } from '../../../router/routes'
import { CreateSubscriptions } from '../../../services/billing.service'
import {
  GetSubscriptionsAction,
  RefreshSubscriptionAction,
  updateSubscriptionActions
} from '../../../store/billing/actions'
import { SetFlash } from '../../../store/general-flash/actions'
import { DateFromStamp } from '../../atoms/DateFromStamp/DateFromStamp'

import Loading from '../../atoms/Loading/Loading'
import LoadingButton from '../../atoms/LoadingButton/LoadingButton'
import { PriceDetails } from '../../molecules/PriceDetails/PriceDetails'

export function ConfirmSubscription () {
  const match = useRouteMatch()
  const history = useHistory()
  const [loading, setLoading] = useState(true)
  const ref = useRef()
  const dispatch = useDispatch()
  const paymentID = match.params.paymentid
  const prices = useSelector((state) => state.billing.prices.items)
  const subscription = useSelector((state) => {
    const subsciptions = state.billing.subscriptions
    return subsciptions.current
  })
  const customerID = useSelector((state) => state.billing.customer.id)
  const price = prices.find((p) => p.id === paymentID)
  const token = useSelector((state) => state.auth.token)

  useEffect(() => {
    if (subscription && (!price || (ref.current && !ref.current.loading))) {
      setLoading(true)
      dispatch(RefreshSubscriptionAction(subscription.id))
        .then(() => dispatch(GetSubscriptionsAction()))
        .then(() => setLoading(false))
      return
    }
    setLoading(false)
  }, [paymentID])

  if (loading || !price || !subscription) {
    return (
      <div
        style={{ height: '70vh' }}
        className='d-flex justify-content-center align-items-center'>
        <Loading ref={ref} />
        <h6 className='text-center'>
          Loading your new plan information...
        </h6>
      </div>
    )
  }

  let currentPrice = {}
  if (subscription.product) {
    currentPrice = prices
      .find((price) =>
        JSON.stringify(price.product.metadata) === JSON.stringify(subscription.product.metadata)
      )
  }

  const setMessage = (message, type = 'success') => {
    dispatch(SetFlash('general', message, type))
  }

  const createSubscription = async (e) => {
    setLoading(true)
    const response = await CreateSubscriptions(token, {
      'customer_id': customerID,
      'price_id': price.id
    })

    if (!response.ok) {
      dispatch(updateSubscriptionActions.failure(response.json))
      setMessage(`Something whent wrong, please contact the support team support@pantacor.com`, 'error')
    }

    if (response.ok) {
      dispatch(updateSubscriptionActions.success(response.json))
      dispatch(GetSubscriptionsAction())

      history.push(`${subscriptionsNewPath}/${response.json.id}`)
    }

    setLoading(false)
  }

  const isDowngrade = price.unit_amount < currentPrice.unit_amount

  return (
    <section ref={ref} className='pt-5 pb-5'>
      <header>
        <h1>Review</h1>
      </header>
      <section>
        <header>
          <p>
            You are choosing to subscribe to:&nbsp;
            <b>{resolvePath(price, 'product.name', 'Free')}</b>
          </p>
          {isDowngrade ? (
            <p>
              Your <b>{subscription.product.name}</b> benefits will still be available to you until:&nbsp;
              <DateFromStamp when={subscription.current_period_end} />.
              After that the new benefits will start.
            </p>
          ) : (
            <p>
              Your new plan will activate immediately so you can take full advantage of its benefits.
            </p>
          )}
          <div className="d-flex">
            <div className="">
              <Link
                to={subscriptionsPath}
                className="btn btn-secondary"
              >
                Cancel
              </Link>
            </div>
            <div className="pl-2">
              <LoadingButton
                loadingChild={() => (
                  <Loading style={{ color: 'white' }} size='1em' />
                )}
                loading={loading}
                onClick={createSubscription}
                className='btn btn-primary'
              >
                Subscribe
              </LoadingButton>
            </div>
          </div>
        </header>
        <div className="row">
          <div className="row pt-4">
            <div className='col-md-3'>
              <PriceDetails price={price} />
            </div>
          </div>
        </div>
      </section>
    </section>
  )
}
