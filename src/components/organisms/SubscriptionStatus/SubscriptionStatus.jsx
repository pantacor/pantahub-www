import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { mergeObject, resolvePath } from '../../../lib/object.helpers'
import Loading from '../../atoms/Loading/Loading'
import { initializeDashboard } from '../../../store/dashboard/actions'
import { PriceSelector } from '../../molecules/PriceSelector/PriceSelector'
import { SetFlash } from '../../../store/general-flash/actions'
import { DateFromStamp } from '../../atoms/DateFromStamp/DateFromStamp'
import { Money } from '../../atoms/Money/Money'
import { Link } from 'react-router-dom'
import { subscriptionChargesPath } from '../../../router/routes'

const defaultMeta = {
  DEVICES: '10'
}

const INTERVALS = {
  MONTHLY: 'month',
  YEARLY: 'year'
}

export const DefaultProduct = (meta = defaultMeta) => ({
  name: 'Custom',
  description: `Custom plan with up to ${meta.DEVICES} devices`,
  unit_amount: 0,
  currency: 'USD'
})

function AvailablePrices ({ prices = [], subscription }) {
  const d = useDispatch()
  const setFlash = (message, type = 'success') => {
    d(SetFlash('general', message, type))
  }
  let priceWithSameMeta
  let hasSameMeta = false

  if (subscription?.product) {
    priceWithSameMeta = prices
      .find((price) =>
        JSON.stringify(price.product.metadata) === JSON.stringify(subscription.product.metadata)
      )
    hasSameMeta = priceWithSameMeta && priceWithSameMeta.unit_amount === 0
  }
  const allPrices = prices.reduce((acc, price) => {
    if (subscription && !hasSameMeta && subscription.product && !acc.has(`${subscription.product.id}-${price.recurring.interval}`)) {
      acc.set(`${subscription.product.id}-${price.recurring.interval}`, {
        id: subscription.product.id,
        active: true,
        unit_amount: 0,
        currency: resolvePath(subscription, 'price.currency', 'USD').toUpperCase(),
        product: mergeObject(DefaultProduct(subscription.meta), subscription.product)
      })
    }

    price.active = false
    if (
      subscription &&
      subscription.product &&
      (
        (
          hasSameMeta &&
          priceWithSameMeta.id === price.id
        ) ||
        price.product.id === subscription.product.id
      )
    ) {
      price.active = true
    }
    acc.set(`${price.product.id}-${price.recurring.interval}`, price)

    return acc
  }, new Map())

  const sortedValues = Array
    .from(allPrices.values())
    .sort((a, b) => a.unit_amount - b.unit_amount)
    .map((a, i) => { a.index = i; return a })

  return (
    <section>
      <header>
      </header>
      <div className='row'>
        {sortedValues.map((p) => (
          <div key={p.id} className='col-sm-3 d-flex align-items-stretch'>
            <PriceSelector
              setMessage={setFlash}
              currentPrice={priceWithSameMeta}
              price={p}
              pricesLength={sortedValues.length}
            />
          </div>
        ))}
      </div>
    </section>
  )
}

function SubscriptionDetail (props) {
  if (!props.subscription) {
    return null
  }

  const dayUntilDue = resolvePath(props, 'subscription.current_period_end', 0)
  const priceAmount = resolvePath(props, 'subscription.payment.amount', 0)
  const priceCurrency = resolvePath(props, 'subscription.payment.currency', '').toUpperCase()
  return (
    <section>
      <p className='mb-0'>
        Cost of your subscription:
        &nbsp;
        <Money amount={priceAmount} currency={priceCurrency} />
        &nbsp;
        <Link to={subscriptionChargesPath} className='fw-light' >More details</Link>
      </p>
      {dayUntilDue > 0 && (
        <p className=''>
          Your next renewal date is:&nbsp;
          <DateFromStamp
            when={dayUntilDue}
          />
        </p>
      )}
    </section>
  )
}

export function SubscriptionStatus () {
  const dispatch = useDispatch()
  const [currency, setCurrency] = useState('usd')
  const [chargeInt, setChargeInt] = useState(INTERVALS.MONTHLY)

  useEffect(() => {
    dispatch(initializeDashboard())
  }, [])

  const data = useSelector((state) => ({
    profile: state.profile.current,
    subscription: state.billing.subscriptions.current,
    customer: state.billing.customer,
    prices: state.billing.prices.items,
    error: state.billing.error,
    status: state.billing.status
  }))

  const currencies = [...new Set(data.prices.map(p => p.currency))]

  const handleOnChange = (e) => {
    setCurrency(e.target.value)
  }

  const onIntervalChange = (int) => (e) => {
    setChargeInt(int)
  }

  if (!!data.error && data.status === 'PAYMENTS_GET_FAILURE') {
    return (
      <div
        style={{ height: '70vh' }}
        className='d-flex flex-column justify-content-center align-items-center'
      >
        <>
          <h6 className='text-center'>
            We can&#39t get your subscription information, please contact us...
          </h6>
        </>
      </div>
    )
  }

  if (
    (
      data.status === 'SUBSCRIPTION_GET_INPROGRESS' ||
      data.status === 'SUBSCRIPTION_GET_SUCCESS'
    ) &&
    resolvePath(data, 'subscription.meta', []).length === 0) {
    return (
      <div
        style={{ height: '70vh' }}
        className='d-flex flex-column justify-content-center align-items-center'
      >
        <>
          <Loading />
          <h6 className='text-center'>
            Loading your subscription data, please wait...
          </h6>
        </>
      </div>
    )
  }

  const filterPrices = data.prices.filter((p) => {
    return p.currency.toLowerCase() === currency.toLowerCase() &&
      p.recurring.interval === chargeInt &&
      resolvePath(p, 'product.metadata.PUBLIC', 'true') === 'true'
  })

  let productName = resolvePath(data, 'subscription.product.name', 'Free')
  if (productName === '') {
    productName = 'Free'
  }

  // const currentPrice = resolvePath(data, 'subscription.price', {})

  return (
    <section id="subscription-info" className='pb-5'>
      <header className='row'>
        <div className='col-12 content-header '>
          <h1>Subscriptions</h1>
        </div>
      </header>
      <section>
        <h5 className='fw-normal mt-2'>
          Your are subscribed to: <b>{productName}</b>
        </h5>
        <SubscriptionDetail subscription={data.subscription} />
        <p className=''>
          To change plan, select one from the options below.
          Or to find out more about <b>custom plans</b>,
          please <a href="mailto:sales@pantacor.com">contact</a> our sales team.
        </p>
      </section>
      <section className='mt-4'>
        <div className="row pt-0 pb-0">
          <div className="col-12">
            <div className="row">
              <div className='col-3 text-start'>
                <select
                  onChange={handleOnChange}
                  value={currency}
                >
                  {currencies.map(c => (
                    <option key={c} value={`${c}`}>
                      {(c || '').toUpperCase()}
                    </option>
                  ))}
                </select>
              </div>
            </div>
          </div>
        </div>
        <div className="row pt-4 pb-5">
          <div className="col-12">
            <div className='d-flex justify-content-center'>
              <div
                className="btn-group"
                role="group"
                aria-label="Basic radio toggle button group"
              >
                <input
                  type="radio"
                  className="btn-check"
                  name="charge-interval"
                  id={`charge-interval-${INTERVALS.MONTHLY}`}
                  autoComplete="off"
                  checked={chargeInt === INTERVALS.MONTHLY}
                />
                <label
                  className="btn btn-outline-primary price-interval-selector"
                  htmlFor={`charge-interval-${INTERVALS.MONTHLY}`}
                  onClick={onIntervalChange(INTERVALS.MONTHLY)}
                >
                  Pay Monthly
                </label>

                <input
                  type="radio"
                  className="btn-check"
                  name="charge-interval"
                  id={`charge-interval-${INTERVALS.YEARLY}`}
                  autoComplete="off"
                  checked={chargeInt === INTERVALS.YEARLY}
                />
                <label
                  className="btn btn-outline-primary price-interval-selector"
                  htmlFor={`charge-interval-${INTERVALS.YEARLY}`}
                  onClick={onIntervalChange(INTERVALS.YEARLY)}
                >
                  Pay Yearly and Save
                </label>
              </div>
            </div>
          </div>
        </div>
        <AvailablePrices
          subscription={data.subscription}
          prices={filterPrices}
        />
      </section>
      {/* <section className='mt-4'>
        <p>
          To find out more about <b>custom plan</b>, please contact our sales team.
        </p>
        <p>
          <a className='btn btn-info' href="mailto:sales@pantacor.com">Contact Sales</a>
        </p>
      </section> */}
    </section>
  )
}
