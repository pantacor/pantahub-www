/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { userDashboardPath } from '../../../router/routes'
import { getDevices } from '../../../services/devices.service'
import Loading from '../../atoms/Loading/Loading'
import dayjs from 'dayjs'
import orderBy from 'lodash.orderby'
import RelativeTime from '../../atoms/RelativeTime/RelativeTime'
import DevicesBreadcumbs from '../../atoms/DevicesBreadcumbs/DevicesBreadcumbs'

import '../../molecules/Tiles/tiles.scss'

const sortByTime = v =>
  dayjs().second(0).millisecond(0) - dayjs(v['time-modified']).second(0).millisecond(0)

const STATUS = {
  NONE: null,
  LOADING: 'LOADING',
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR'
}

function Device (props) {
  const { device, username } = props

  const deviceDataExist =
    (device['device-meta'] || {})['pantavisor.cpumodel'] &&
    (device['device-meta'] || {})['pantavisor.dtmodel']

  return (
    <section className="public-device device-tiles mb-4 mt-4 col-12 col-sm-4">
      <div className="tile card">
        <div className="card-header">
          <Link
            to={`${userDashboardPath}/${username}/devices/${device.id}`}
          >
            {device.nick}
          </Link>
        </div>
        <div className="card-body">
          {deviceDataExist && (
            <ul className="small list-unstyled">
              <li>{(device['device-meta'] || {})['pantavisor.cpumodel']}</li>
              <li>{(device['device-meta'] || {})['pantavisor.dtmodel']}</li>
            </ul>
          )}
          <div className="small">
            Modified <b><RelativeTime when={device['time-modified']}/></b>
          </div>
        </div>
      </div>
    </section>
  )
}

class PublicDevices extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      status: STATUS.LOADING,
      err: null,
      err_type: null,
      devices: {}
    }
  }

  async componentDidMount () {
    try {
      const devicesResp = await getDevices(
        this.props.auth.token,
        { 'owner-nick': this.props.username }
      )

      if (!devicesResp.ok) {
        return this.setState({
          status: STATUS.ERROR,
          err: new Error('Account not found'),
          err_type: 404
        })
      }

      if (!devicesResp.json || devicesResp.json.length === 0) {
        return this.setState({
          status: STATUS.ERROR,
          err: new Error('Account doesn\'t have public devices yet'),
          err_type: 404
        })
      }

      if (devicesResp.json.err) {
        return this.setState({
          status: STATUS.ERROR,
          err: new Error('Account not found'),
          err_type: 500
        })
      }

      const devices = orderBy(
        devicesResp.json.filter(d => !d.garbage),
        [sortByTime, 'nick'],
        ['asc']
      )

      this.setState({
        status: STATUS.SUCCESS,
        devices: devices
      })
    } catch (e) {
      this.setState({
        status: STATUS.ERROR,
        err: e
      })
    }
  }

  render () {
    const {
      username
    } = this.props

    return (
      <React.Fragment>
        <DevicesBreadcumbs username={username} />
        <div className="row">
          <div className="col-12">
            <h1>{username} <span className="small text-muted">Devices</span></h1>
          </div>
        </div>
        <div key="devicesInner" className="row">
          {this.state.status === STATUS.LOADING && (
            <div className="col-12 pt-8">
              <Loading />
            </div>
          )}
          {this.state.status === STATUS.SUCCESS && this.state.devices.map(device => (
            <Device key={device.id} device={device} username={this.props.username} />
          ))}
          {this.state.status === STATUS.ERROR && (
            <div className="col-12 text-center pt-9">
              <h2>{this.state.err.message}</h2>
            </div>
          )}
        </div>
      </React.Fragment>
    )
  }
}

export default connect(
  state => ({
    auth: state.auth
  }),
  null
)(PublicDevices)
