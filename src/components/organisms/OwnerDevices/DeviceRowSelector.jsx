/*
 * Copyright (c) 2017-2023 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import Loading from '../../atoms/Loading/Loading'
import DeviceRow from './DeviceRow'
import orderBy from 'lodash.orderby'
import dayjs from 'dayjs'

const _sortFuncs = {
  'step-time': v => dayjs().second(0).millisecond(0) - dayjs(v['step-time']).second(0).millisecond(0),
  timestamp: v => dayjs().second(0).millisecond(0) - dayjs(v.timestamp).second(0).millisecond(0)
}

function sortDevices (devices, sortField, sortDirection, search) {
  const afterSearch = devices.filter(dev => {
    return ((search && dev['device-nick']) || '').indexOf(search) >= 0 ||
      ((search && dev.deviceid) || '').indexOf(search) >= 0
  })

  return orderBy(
    afterSearch,
    [_sortFuncs[sortField] || sortField, 'device-nick'],
    [sortDirection ? 'asc' : 'desc', 'asc']
  )
}

export function DeviceRowSelector (props) {
  if (!props.loaded) {
    return (
      <tr>
        <td colSpan={7}>
          <Loading />
        </td>
      </tr>
    )
  }

  if (props.error !== null) {
    return (
      <tr>
        <td colSpan={7}>
          <span className="error">
            An error occurred while loading the devices information. Please try
            again later.
          </span>
        </td>
      </tr>
    )
  }

  if (props.devices.length === 0 && props.loaded) {
    return (
      <tr>
        <td colSpan="7" className="align-center shadow-sm p-4 mb-3 mt-3 rounded text-center text-muted small" style={{ color: '#4D4D4D' }}>
          Uh-oh! It seems there are no devices to display. Let&apos;s get you started
          on claiming your very first device to experience the magic!.<br/>
          <a
            href='https://docs.pantahub.com/choose-device/'
            target="_blank"
            rel="noopener noreferrer"
          >
            See how to prepare your device
          </a>
        </td>
      </tr>
    )
  }

  return (
    (
      sortDevices(props.devices, props.sortField, props.sortDirection, props.search).map(dev => (
        <DeviceRow
          key={dev.deviceid}
          dev={dev}
          token={props.token}
          username={props.username}
          publishTracker={props.publishTracker}
          deleteTracker={props.deleteTracker}
        />
      ))
    )
  )
}
