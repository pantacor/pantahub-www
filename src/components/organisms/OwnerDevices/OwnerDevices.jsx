/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import {
  initializeDevices,
  setDevicesSearch,
  setDevicesSortField
} from '../../../store/devices/actions'

import { userDashboardPath } from '../../../router/routes'
import { resolvePath } from '../../../lib/object.helpers'
import { TrackedButton } from '../../atoms/Tracker/Tracker'
import { useIntervalAfterLoading } from '../../../hooks/useIntervalAfterLoading'
import DevicesBreadcumbs from '../../atoms/DevicesBreadcumbs/DevicesBreadcumbs'
import { DeviceRowSelector } from './DeviceRowSelector'

const sortableFields = [
  ['device-nick', 'Device'],
  ['progress-revision', 'Commit ID (Rev)'],
  ['step-time', 'Modified'],
  ['timestamp', 'Last seen'],
  ['status', 'Status'],
  ['status-msg', 'Message']
]

class UserDevicesHeader extends Component {
  render () {
    const { onSearch, search } = this.props

    return (
      <div className="header-sector">
        <div className="row align-items-center">
          <div className="col-md-3">
            <h1>Devices</h1>
          </div>
          <div className="col-md-9">
            <div className="input-group col-m">
              <input
                type="text"
                className="form-control"
                placeholder="Search in Your Devices"
                aria-label="Search in Your Devices"
                onChange={evt => {
                  onSearch(evt.target.value)
                }}
                value={search || ''}
              />
              <span className="input-group-append">
                <TrackedButton className="btn btn-light" type="button">
                  <i className="mdi mdi-magnify" aria-hidden="true" />
                </TrackedButton>
              </span>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

class UserDevicesTable extends Component {
  render () {
    const {
      toggleSort,
      sortField,
      sortDirection
    } = this.props

    const sortIconForField = field =>
      sortField === field
        ? sortDirection
          ? 'chevron-down'
          : 'chevron-up'
        : 'unfold-more-horizontal'

    const sortFieldActive = field => (sortField === field ? 'active' : null)

    return (
      <div className="list-sector">
        <div className="table-responsive">
          <table className="table table-borderless table-hover">
            <thead>
              <tr>
                {sortableFields.map(([field, label]) => (
                  <th
                    key={field}
                    onClick={toggleSort(field)}
                    className={`${sortFieldActive(field)} ${field}`}
                  >
                    <span>
                      {label}{' '}
                      <i
                        className={`mdi mdi-${sortIconForField(field)}`}
                        aria-hidden="true"
                      />
                    </span>
                  </th>
                ))}
                <th className="text-right actions">Actions</th>
              </tr>
            </thead>
            <tbody>
              <DeviceRowSelector {...this.props}/>
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

class UserDevicesInner extends Component {
  render () {
    const {
      username,
      token,
      devices,
      loading,
      deleteTracker,
      publishTracker,
      onSearch,
      search,
      toggleSort,
      sortField,
      sortDirection,
      error,
      loaded
    } = this.props

    return (
      <React.Fragment>
        <UserDevicesHeader
          onSearch={onSearch}
          loading={loading}
          search={search}
        />
        <UserDevicesTable
          username={username}
          token={token}
          devices={devices}
          loading={loading}
          loaded={loaded}
          search={search}
          toggleSort={toggleSort}
          sortField={sortField}
          sortDirection={sortDirection}
          deleteTracker={deleteTracker}
          publishTracker={publishTracker}
          error={error}
        />
      </React.Fragment>
    )
  }
}

function UserDevices (props) {
  useIntervalAfterLoading(
    () => {
      const { init, auth } = props
      const { token } = auth
      if (!props.devs.search) {
        init(token)
      }
    },
    resolvePath(props, 'devs.initializing', false),
    3000
  )

  const { devs, auth, dash, onSearch, toggleSort } = props
  const { username, token } = auth
  const {
    devices,
    loading,
    loaded,
    search,
    sortField,
    sortDirection,
    error: devsError
  } = devs
  const quotaStats = resolvePath(dash, 'data.subscription.quota-stats', {})
  const devicesQuota = quotaStats.DEVICES || null

  const actualDeviceCount = (devicesQuota || {}).Actual || 0
  const maxDeviceCount = (devicesQuota || {}).Max || 0

  return (
    <React.Fragment>
      <DevicesBreadcumbs username={username} />
      <div className="d-flex justify-content-end mt-4 mb-4">
        <div className="d-flex justify-content-end align-items-center">
          {devicesQuota !== null && (
            <span className="quota-indicator">
              You are using {actualDeviceCount} of {maxDeviceCount} devices
              available &nbsp;
            </span>
          )}
        </div>
        <div className="d-flex justify-content-end">
          <Link
            to={`${userDashboardPath}/claim`}
            className="btn btn-primary btn-sm-block"
          >
            Claim a Device
          </Link>
        </div>
      </div>

      <div key="devicesInner" className="row">
        <div className="col-md-12">
          <UserDevicesInner
            username={username}
            token={token}
            devices={devices}
            loading={loading}
            loaded={loaded}
            onSearch={onSearch}
            search={search}
            toggleSort={toggleSort}
            sortField={sortField}
            sortDirection={sortDirection}
            deleteTracker={devs.delete}
            publishTracker={devs.publish}
            error={devsError}
          />
        </div>
      </div>
    </React.Fragment>
  )
}

export default connect(
  state => state,
  dispatch => ({
    init: token => dispatch(initializeDevices(token)),
    onSearch: search => dispatch(setDevicesSearch(search)),
    toggleSort: field => () => dispatch(setDevicesSortField(field))
  })
)(UserDevices)
