import React, { useEffect, useRef, useState } from 'react'
import {
  GetSubscriptionsAction,
  RefreshPaymentAction,
  RefreshSubscriptionAction
} from '../../../store/billing/actions'
import {
  useDispatch,
  useSelector
} from 'react-redux'
import {
  Link,
  useRouteMatch
} from 'react-router-dom'
import { resolvePath } from '../../../lib/object.helpers'
import { subscriptionsPath } from '../../../router/routes'
import { DateFromStamp } from '../../atoms/DateFromStamp/DateFromStamp'
import Loading from '../../atoms/Loading/Loading'
import { Money } from '../../atoms/Money/Money'

function PaymentLines ({ nextInvoice, lastestInvoice }) {
  return (
    <div className="col-md-7 pr-4">
      {lastestInvoice && lastestInvoice.lines.data.length > 0 && (
        <div className=''>
          <h5 className='fw-normal'>
            Last invoice
          </h5>
          <div className='mb-0'>
            <table className='table'>
              <thead>
                <tr>
                  <th></th>
                  <th>Description</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                {lastestInvoice.lines.data.map((l) => (
                  <tr key={l.id}>
                    <td className='text-muted'>
                      <DateFromStamp when={l.period.start} format="DD MMM" />
                      &nbsp;-&nbsp;
                      <DateFromStamp when={l.period.end} format="DD MMM" />
                    </td>
                    <td>{l.description}</td>
                    <td>
                      <Money amount={l.amount} currency={l.currency} />
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      )}
      {nextInvoice && nextInvoice.length > 0 && (
        <div className='mt-4'>
          <h5 className='fw-normal'>
            Upcoming invoice
          </h5>
          <table className='table'>
            <thead>
              <tr>
                <th></th>
                <th>Description</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
              {nextInvoice.map((l) => (
                <tr key={l.id}>
                  <td className='text-muted'>
                    <DateFromStamp when={l.period.start} format="DD MMM" />
                    &nbsp;-&nbsp;
                    <DateFromStamp when={l.period.end} format="DD MMM" />
                  </td>
                  <td>
                    {l.description}
                  </td>
                  <td>
                    <Money
                      amount={l.amount}
                      currency={l.currency}
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}
    </div>
  )
}

export function PaymentDetail () {
  const match = useRouteMatch()
  const [loading, setLoading] = useState(true)
  const ref = useRef()
  const dispatch = useDispatch()
  const paymentID = match.params.paymentid
  const payment = useSelector((state) => state.billing.payment)
  const subscription = useSelector((state) => state.billing.subscription)
  const nextInvoiceIndex = (resolvePath(payment, 'invoice.lines.data', []) || []).length - 1
  // const lastPaymentLineIndex = resolvePath(payment, 'invoice.lines.data', []).length - 1

  const price = useSelector((state) => {
    const priceID = resolvePath(payment, `invoice.lines.data.${Math.max(0, nextInvoiceIndex)}.price.id`)
    return state.billing.prices.items.find((p) => p.id === priceID)
  })

  const isDowngrade = resolvePath(subscription, 'price.unit_amount') > resolvePath(price, `unit_amount`)

  useEffect(() => {
    if (!payment || (ref.current && !ref.current.loading)) {
      setLoading(true)
      dispatch(RefreshPaymentAction(paymentID))
        .then((r) => r.ok && dispatch(RefreshSubscriptionAction(r.json.payment_of_id)))
        .then(() => dispatch(GetSubscriptionsAction()))
        .then(() => setLoading(false))
    }
  }, [paymentID])

  if (loading || !payment) {
    return (
      <div
        style={{ height: '70vh' }}
        className='d-flex justify-content-center align-items-center'>
        <Loading ref={ref} />
        <h6 className='text-center'>
          Loading your invoice...
        </h6>
      </div>
    )
  }

  return (
    <section ref={ref}>
      <header className='content-header '>
        <h1>Confirmation</h1>
      </header>
      <section>
        <header>
          <p>
            You have successfully subscribed to: <b>{resolvePath(price, 'product.name')}</b>
          </p>
          {isDowngrade ? (
            <p>
              Your <b>{subscription.product.name}</b> benefits will still be available to you until:&nbsp;
              <DateFromStamp when={subscription.current_period_end} />.
              After that the new benefits will start.
            </p>
          ) : (
            <p>
              Your new plan will activate immediately so you can take full advantage of its benefits.
            </p>
          )}
        </header>
        <div className='row mt-5 mb-5'>
          {!isDowngrade && (
            <PaymentLines
              nextInvoice={resolvePath(subscription, 'next_invoice')}
              lastestInvoice={resolvePath(subscription, 'lastest_invoice')}
            />
          )}
        </div>
        <p className='mt-4'>
          <Link
            to={subscriptionsPath}
            className='btn btn-secondary mr-1'
            tabIndex="-1"
            role="button"
            aria-disabled="true"
          >
            View subscription
          </Link>
          <a
            href={payment.invoice.hosted_invoice_url}
            className='btn btn-primary'
            tabIndex="-1"
            role="button"
            aria-disabled="true"
            rel="noreferrer"
            target="_blank"
          >
            Get Invoice
          </a>
        </p>
      </section>
    </section>
  )
}
