import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { resolvePath } from '../../../lib/object.helpers'
import {
  Elements,
  PaymentElement,
  useElements,
  useStripe
} from '@stripe/react-stripe-js'
import { GetWwwURL, LoadStripePromise } from '../../../lib/const.helpers'
import { Link, useHistory, useRouteMatch } from 'react-router-dom'
import { GetSubscriptionAction, GetSubscriptionsAction } from '../../../store/billing/actions'
import Loading from '../../atoms/Loading/Loading'
import { orderCompletePath, paymentDetailsPath, subscriptionsPath } from '../../../router/routes'
import LoadingButton from '../../atoms/LoadingButton/LoadingButton'
import { Path } from 'path-parser'
import { PriceDetails } from '../../molecules/PriceDetails/PriceDetails'

const paymentDetailsURL = Path.createPath(paymentDetailsPath)

const CheckoutForm = () => {
  const stripe = useStripe()
  const elements = useElements()
  const [loading, setLoading] = useState(true)
  const [submiting, setSubmitting] = useState(false)
  const [error, setError] = useState(null)

  const handleOnReady = () => {
    setLoading(false)
  }
  const handleSubmit = async (event) => {
    // We don't want to let default form submission happen here,
    // which would refresh the page.
    event.preventDefault()
    setSubmitting(true)

    if (!stripe || !elements) {
      // Stripe.js has not yet loaded.
      // Make sure to disable form submission until Stripe.js has loaded.
      return
    }

    setLoading(true)

    const result = await stripe.confirmPayment({
      // `Elements` instance that was used to create the Payment Element
      elements,
      confirmParams: {
        return_url: `${GetWwwURL()}${orderCompletePath}`
      }
    })

    setLoading(false)

    if (result.error) {
      // Show error to your customer (for example, payment details incomplete)
      console.info(result.error.message)
      setError(result.error.message)
    } else {
      // Your customer will be redirected to your `return_url`. For some payment
      // methods like iDEAL, your customer will be redirected to an intermediate
      // site first to authorize the payment, then redirected to the `return_url`.
    }
    setSubmitting(false)
  }

  return (
    <form onSubmit={handleSubmit} >
      <div className='mt-4 mb-4'>
        <PaymentElement onReady={handleOnReady}/>
      </div>
      {loading ? (
        <Loading />
      ) : (
        <LoadingButton
          loading={!stripe || submiting}
          loadingChild='Saving your billing information'
          className='btn btn-primary'
        >
          Subscribe
        </LoadingButton>
      )}
      {error !== null && (
        <div className="alert alert-danger mt-4" role="alert">
          Something whent wrong, please contact the support team if your subscriptions was not proceded <a href="mailto:support@pantacor.com">support@pantacor.com</a>.
        </div>
      )}
    </form>
  )
}

export function Payment ({ secret }) {
  const options = {
    clientSecret: secret
  }

  return (
    <Elements stripe={LoadStripePromise()} options={options}>
      <CheckoutForm />
    </Elements>
  )
}

export function SubscriptionCreate () {
  const history = useHistory()
  const match = useRouteMatch()
  const [loading, setLoading] = useState(false)
  const dispatch = useDispatch()
  const data = useSelector((state) => ({
    prices: resolvePath(state, 'billing.prices.items', []),
    subscription: resolvePath(state, 'billing.subscription', {}),
    price: resolvePath(state, 'billing.subscription.price', {}),
    secret: resolvePath(state, 'billing.subscription.payment.client_secret', '')
  }))

  useEffect(() => {
    if (!loading &&
      data.subscription.status === 'active' &&
      (!data.price || data.price.active)
    ) {
      dispatch(GetSubscriptionsAction())
      history.push(paymentDetailsURL.build({ paymentid: data.subscription.payment.id }))
      return
    }
    if (!loading && (!data.price || !data.price.id || data.secret === '')) {
      setLoading(true)
      dispatch(GetSubscriptionAction(match.params.subscription_id))
    }
  }, [data, data.price, data.secret, loading])

  if (!data.price || !data.price.id || data.secret === '') {
    return (
      <div
        style={{ height: '70vh' }}
        className='d-flex justify-content-center align-items-center'
      >
        <Loading />
      </div>
    )
  }

  const price = data.prices.find((p) => p.id === data.price.id)

  return (
    <section
      className='subscription-create pt-5 pb-5'
    >
      <header>
        <h1>Review</h1>
        <p className='fs-4'>
          Your are choosing to subscribe to:&nbsp;
          <b>{resolvePath(price, 'product.name', 'Free')}</b>
        </p>
        <p>
          Your new plan will activate immediately so you can take full advantage of its benefits.
        </p>
        <p>
          <Link className='btn btn-info' to={subscriptionsPath}>
            Cancel
          </Link>
        </p>
      </header>
      <div className="row pt-4">
        <div className='col-md-3'>
          <PriceDetails price={price} />
        </div>
        <div className='col-md-7'>
          <div className='pl-2 pr-2'>
            <Payment secret={data.secret} />
          </div>
        </div>
      </div>
    </section>
  )
}
