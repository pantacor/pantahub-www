/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {
  _getJSON, _putJSON
} from './api.service'

import { GetApiURL } from '../lib/const.helpers'

const PROFILES_URL = `${GetApiURL()}/profiles`

export const getProfileData = async (nick, token) =>
  _getJSON(`${PROFILES_URL}/${nick}`, token)

export const putProfileData = async (payload, token) =>
  _putJSON(`${PROFILES_URL}/`, token, payload)

export const getProfileMetaData = async (token) =>
  _getJSON(`${PROFILES_URL}/config/meta`, token)

export const putProfileMetaData = async (payload, token) =>
  _putJSON(`${PROFILES_URL}/config/meta`, token, payload)
