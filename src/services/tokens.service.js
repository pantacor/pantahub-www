/*
 * Copyright (c) 2017-2024 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import { GetApiURL } from '../lib/const.helpers'
import { _getJSON, _postJSON, _delete } from './api.service'

/**
 * @typedef {Object} Token
 * @property {string} time-created
 * @property {string} time-modified
 * @property {string} id
 * @property {string} name
 * @property {string} type
 * @property {string} prn
 * @property {string} owner
 * @property {Array<string>} scopes
 * @property {Array<Scope>} parse-scopes
 * @property {boolean} deleted
 * @property {string} expire-at
*/

/**
 * @typedef {Object} Scope
 * @property {string} id
 * @property {string} service
 * @property {string} description
}

/**
 * @typedef {Object} TokenListResponse
 * @property {string} resource - The resource URL
 * @property {number} page_size - The page size
 * @property {number} page_offset - The page offset
 * @property {number} current_page - The current page
 * @property {number} total - The total number of items
 * @property {string} next - The next page URL
 * @property {string} prev - The previous page URL
 * @property {Array<Token>} items - The list of tokens
 */

/**
 * @typedef {Object} FetchResponse
 * @property {boolean} ok - Indicates if the request was successful
 * @property {boolean} redirected - Indicates if the request was redirected
 * @property {Headers} headers - The response headers
 * @property {number} status - The response status code
 * @property {TokenListResponse} json - A function to parse the response body as JSON
 */

const serviceTokensUrl = () => `${GetApiURL()}/tokens`

/**
 * Fetches tokens using the provided token.
 *
 * @param {string} token - Auth bearer token
 * @return {Promise<FetchResponse>} A promise that resolves to a FetchResponse object
 */
export const GetTokens = async (token = '') => {
  return _getJSON(`${serviceTokensUrl()}/`, token)
}

/**
 * Creates a token using the provided token and optional body.
 *
 * @param {string} token - Auth bearer token
 * @param {Token} [body={}] - Optional body for the token creation
 * @return {Promise<FetchResponse>} A promise that resolves to a FetchResponse object
 */
export const CreateToken = async (token, body = {}) => {
  return _postJSON(`${serviceTokensUrl()}/`, token, body)
}

/**
 * Deletes a token using the provided token and token ID.
 *
 * @param {string} token - Auth bearer token
 * @param {string} tokenID - Token ID to delete
 * @return {Promise<FetchResponse>} A promise that resolves to a FetchResponse object
 */
export const DeleteToken = async (token, tokenID = '') => {
  return _delete(`${serviceTokensUrl()}/${tokenID}`, token)
}
