/*
 * Copyright (c) 2017-2024 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import { BuildURLWithParams } from '../lib/api.helpers'
import { GetBillingApiUrl } from '../lib/const.helpers'
import { _getJSON, _patchJSON, _postJSON, _putJSON } from './api.service'

const serviceSubscriptionUrl = () => `${GetBillingApiUrl()}/subscriptions`
const serviceSubscriptionUrlPayment = () => `${GetBillingApiUrl()}/payments`
const serviceCustomerUrl = () => `${GetBillingApiUrl()}/customers`
const servicePricesUrl = () => `${GetBillingApiUrl()}/prices`

export const GetPrices = async (token) => {
  return _getJSON(servicePricesUrl(), token)
}

export const GetSubscriptions = async (token, params = {}) => {
  const url = BuildURLWithParams(`${serviceSubscriptionUrl()}`, params)
  return _getJSON(url, token)
}

export const GetSubscription = async (token, id, params = {}) => {
  const url = BuildURLWithParams(`${serviceSubscriptionUrl()}/${id}`, params)
  return _getJSON(url, token)
}

export const UpdateSubscriptions = async (token, id = '', body = {}) => {
  return _patchJSON(`${serviceSubscriptionUrl()}/${id}`, token, body)
}

export const CreateSubscriptions = async (token, body = {}) => {
  return _postJSON(`${serviceSubscriptionUrl()}`, token, body)
}

export const RefreshSubscription = async (token, id = '', params = {}) => {
  const url = BuildURLWithParams(`${serviceSubscriptionUrl()}/${id}/refresh`, params)
  return _putJSON(url, token)
}

export const GetCustomer = async (token, id = '', params = {}) => {
  const url = BuildURLWithParams(`${serviceCustomerUrl()}/${id}`, params)
  return _getJSON(url)
}

export const GetCustomers = async (token, params = {}) => {
  const url = BuildURLWithParams(`${serviceCustomerUrl()}`, params)
  return _getJSON(url, token)
}

export const UpdateCustomer = async (token, id = '', body = {}) => {
  return _patchJSON(`${serviceCustomerUrl()}/${id}`, token, body)
}

export const CreateCustomer = async (token, id = '', body = {}) => {
  return _postJSON(`${serviceCustomerUrl()}`, token, body)
}

export const GetPayment = async (token, id = '', params = {}) => {
  const url = BuildURLWithParams(`${serviceSubscriptionUrlPayment()}/${id}`, params)
  return _getJSON(url, token)
}

export const RefreshPayment = async (token, id = '', params = {}) => {
  const url = BuildURLWithParams(`${serviceSubscriptionUrlPayment()}/${id}/refresh`, params)
  return _putJSON(url, token)
}

export const GetPayments = async (token, params = {}) => {
  const url = BuildURLWithParams(`${serviceSubscriptionUrlPayment()}`, params)
  return _getJSON(url, token)
}

export const UpdatePayment = async (token, id = '', body = {}) => {
  return _patchJSON(`${serviceSubscriptionUrlPayment()}/${id}`, token, body)
}

export const CreatePayment = async (token, id = '', body = {}) => {
  return _postJSON(`${serviceSubscriptionUrlPayment()}`, token, body)
}
