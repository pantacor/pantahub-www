/*
 * Copyright (c) 2024 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React from "react";
import { Route, Switch } from "react-router";
import * as routes from "./routes";

// Application components
import Loading from "../components/atoms/Loading/Loading";
import { withOwnerTracker } from "../store/ga-middleware";
import { userIsAuthenticated, userIsNotAuthenticated } from "../lib/auth";
import { isNative } from "../lib/native";
import { SidebarLayout } from "../components/layouts/SidebarLayout";
import { CenterLayout } from "../components/layouts/CenterLayout";
import HeaderNav from "../components/molecules/Nav/HeaderNav";

// Pages components
const ClaimDevice = React.lazy(
	() => import("../components/pages/ClaimDevice/ClaimDevice"),
);
const Login = React.lazy(() => import("../components/pages/Login/Login"));
const TermsOfService = React.lazy(
	() => import("../components/pages/TermsOfService/TermsOfService"),
);
const UserDashboard = React.lazy(
	() => import("../components/pages/UserDashboard/UserDashboard"),
);
const UserDevices = React.lazy(
	() => import("../components/pages/UserDevices/UserDevices"),
);
const UserAccount = React.lazy(
	() => import("../components/pages/UserAccount/UserAccount"),
);
const VerifyAccount = React.lazy(
	() => import("../components/pages/VerifyAccount/VerifyAccount"),
);
const GeneralFlashMessage = React.lazy(
	() => import("../components/molecules/FlashMessage/FlashMessage"),
);
const Oauth2Authorize = React.lazy(
	() => import("../components/pages/Authorize/Authorize"),
);
const UserDevice = React.lazy(() => import("../components/pages/UserDevice"));
const LandingPage = React.lazy(
	() => import("../components/pages/LandingPage/LandingPage"),
);
const AboutUsPage = React.lazy(
	() => import("../components/pages/AboutUsPage/AboutUsPage"),
);
const PrivacyPage = React.lazy(
	() => import("../components/pages/PrivacyPage/PrivacyPage"),
);
const RequestNewPasswordPage = React.lazy(
	() =>
		import(
			"../components/pages/RequestNewPasswordPage/RequestNewPasswordPage"
		),
);
const ResetPasswordPage = React.lazy(
	() => import("../components/pages/ResetPasswordPage/ResetPasswordPage"),
);
const SignUp = React.lazy(
	() => import("../components/pages/SignUpPage/SignUpPage"),
);
const Applications = React.lazy(
	() => import("../components/pages/Applications/Applications"),
);
const Application = React.lazy(
	() => import("../components/pages/Application/Application"),
);
const EditApplication = React.lazy(
	() => import("../components/pages/EditApplication/EditApplication"),
);
const EditProfile = React.lazy(
	() => import("../components/pages/EditProfile/EditProfile"),
);
const InitializeNewDevicePage = React.lazy(
	() =>
		import(
			"../components/pages/InitializeNewDevicePage/InitializeNewDevicePage"
		),
);
const ImageDownloaderPage = React.lazy(
	() => import("../components/pages/ImageDownloader/ImageDownloader"),
);
const ProfileGlobalConfig = React.lazy(
	() => import("../components/pages/ProfileGlobalConfig/ProfileGlobalConfig"),
);
const BillingPage = React.lazy(
	() => import("../components/pages/BillingPage/BillingPage"),
);
const TokenListPage = React.lazy(
	() => import("../components/pages/TokenListPage/TokenListPage"),
);
const TokenFormPage = React.lazy(
	() => import("../components/pages/TokenFormPage/TokenFormPage"),
);

// Routing layouts
export const Oauth2Layout = (props) => (
	<main>
		<Route
			exact
			path={`${props.match.path}/authorize`}
			component={withLayout(CenterLayout, Oauth2Authorize)}
		/>
	</main>
);

export const FullHeightLoading = () => (
	<div
		style={{
			height: "90vh",
			display: "flex",
			alignItems: "center",
			justifyContent: "center",
		}}
	>
		<Loading />
	</div>
);

// eslint-disable-next-line react/display-name
// const withContainer = (Component) => (props) => (
//   <div className="container">
//     <Component {...props} />
//   </div>
// )

// eslint-disable-next-line react/display-name
const withLayout = (Layout, Component) => (props) => (
	<Layout>
		<Component {...props} />
	</Layout>
);

const UniversalLandingPage = isNative()
	? userIsAuthenticated(withOwnerTracker(UserDashboard, "user/dashboard"))
	: userIsNotAuthenticated(LandingPage);

export const UserLayout = (props) => (
	<React.Fragment>
		<Route
			exact
			path={`${props.match.path}/`}
			component={withOwnerTracker(
				withLayout(SidebarLayout, UserDashboard),
				"user/dashboard",
			)}
		/>
		<Route
			exact
			path={`${props.match.path}/devices`}
			component={withOwnerTracker(
				withLayout(SidebarLayout, UserDevices),
				"user/devices",
			)}
		/>
		<Route
			exact
			path={`${props.match.path}/apps`}
			component={withOwnerTracker(
				withLayout(SidebarLayout, Applications),
				"user/apps",
			)}
		/>
		<Route
			exact
			path={`${props.match.path}/apps/:id`}
			component={withOwnerTracker(
				withLayout(SidebarLayout, Application),
				"user/apps",
			)}
		/>
		<Route
			exact
			path={`${props.match.path}/apps/edit/:id`}
			component={withOwnerTracker(
				withLayout(SidebarLayout, EditApplication),
				"user/apps",
			)}
		/>
		<Route
			exact
			path={`${props.match.path}/claim`}
			component={withOwnerTracker(
				withLayout(SidebarLayout, ClaimDevice),
				"user/claim",
			)}
		/>
		<Route
			exact
			path={`${props.match.path}/account`}
			component={withOwnerTracker(
				withLayout(SidebarLayout, UserAccount),
				"user/account",
			)}
		/>
		<Route
			exact
			path={`${props.match.path}/:username`}
			component={withOwnerTracker(
				withLayout(SidebarLayout, UserDashboard),
				"user/dashboard",
			)}
		/>
		<Route
			exact
			path={`${props.match.path}/:username/devices`}
			component={withOwnerTracker(
				withLayout(SidebarLayout, UserDevices),
				"user/devices",
			)}
		/>
		<Route
			path={[
				`${props.match.path}/:username/devices/:deviceId/step/:revision`,
				`${props.match.path}/:username/devices/:deviceId`,
			]}
			component={withOwnerTracker(
				withLayout(SidebarLayout, UserDevice),
				"user/device",
			)}
		/>
	</React.Fragment>
);

const Header = React.memo(() => {
	return <HeaderNav />;
});

export const RouterLayout = () => {
	return (
		<React.Suspense fallback={<FullHeightLoading />}>
			<GeneralFlashMessage />
			<header>
				<Header />
			</header>
			<Switch>
				<Route
					exact
					path={routes.landingPath}
					component={withLayout(SidebarLayout, UniversalLandingPage)}
				/>
				<Route
					exact
					path={routes.tosPath}
					component={withLayout(SidebarLayout, TermsOfService)}
				/>
				<Route
					exact
					path={routes.aboutUs}
					component={withLayout(SidebarLayout, AboutUsPage)}
				/>
				<Route
					path={routes.userDashboardPath}
					component={userIsAuthenticated(UserLayout)}
				/>
				<Route
					path={routes.oauth2Path}
					component={userIsAuthenticated(Oauth2Layout)}
				/>
				<Route
					exact
					path={routes.loginPath}
					component={userIsNotAuthenticated(
						withLayout(SidebarLayout, Login),
					)}
				/>
				<Route
					exact
					path={routes.signUpPath}
					component={userIsNotAuthenticated(
						withLayout(SidebarLayout, SignUp),
					)}
				/>
				<Route
					exact
					path={routes.verifyPath}
					component={userIsNotAuthenticated(
						withLayout(SidebarLayout, VerifyAccount),
					)}
				/>
				<Route
					exact
					path={routes.privacyPath}
					component={withLayout(SidebarLayout, PrivacyPage)}
				/>
				<Route
					exact
					path={routes.requestNewPassword}
					component={withLayout(
						SidebarLayout,
						RequestNewPasswordPage,
					)}
				/>
				<Route
					exact
					path={routes.resetPassword}
					component={withLayout(SidebarLayout, ResetPasswordPage)}
				/>
				<Route
					exact
					path={routes.globalConfig}
					component={userIsAuthenticated(
						withOwnerTracker(
							withLayout(SidebarLayout, ProfileGlobalConfig),
							"user/config",
						),
					)}
				/>
				<Route
					exact
					path={routes.editProfile}
					component={userIsAuthenticated(
						withOwnerTracker(
							withLayout(SidebarLayout, EditProfile),
							"user/profile",
						),
					)}
				/>
				<Route
					path={routes.billingPath}
					component={userIsAuthenticated(
						withOwnerTracker(
							withLayout(SidebarLayout, BillingPage),
							"user/billing",
						),
					)}
				/>
				<Route
					exact
					path={routes.StartNewDevice}
					component={userIsAuthenticated(
						withOwnerTracker(
							withLayout(SidebarLayout, InitializeNewDevicePage),
							"user/how-to-start",
						),
					)}
				/>
				<Route
					exact
					path={routes.ImageDownloader}
					component={userIsAuthenticated(
						withOwnerTracker(
							withLayout(SidebarLayout, ImageDownloaderPage),
							"user/download-image",
						),
					)}
				/>
				<Route
					exact
					path={routes.tokens}
					component={userIsAuthenticated(
						withOwnerTracker(
							withLayout(SidebarLayout, TokenListPage),
							`user${routes.tokens}`,
						),
					)}
				/>
				<Route
					exact
					path={routes.tokenNew}
					component={userIsAuthenticated(
						withOwnerTracker(
							withLayout(SidebarLayout, TokenFormPage),
							`user${routes.tokens}`,
						),
					)}
				/>
			</Switch>
		</React.Suspense>
	);
};
