/* eslint-disable no-useless-escape */
/* eslint-disable no-use-before-define */

export interface Package {
	id: string;
	name: string;
	type: string;
	content: Packages | Signature | string | JSONContent;
	raw?: JSONContent;
	included?: Set<string>;
	selected?: boolean;
}

export interface Packages {
	[id: string]: Package;
}

export interface JSONContent {
	[key: string]: any;
}

export interface Signature {
	alg: string;
	jwk: Object;
	pvs: {
		include: string[];
		exclude: string[];
	};
	typ: string;
	x5c: string[];
}

export const TYPES = {
	OBJECT: "object",
	JSON: "json",
	SIGNATURE: "signature",
	PACKAGE: "package",
	FOLDER: "folder",
};

const cleanRegex = (glob = "") => {
	const r = glob
		.replace(".", "\\.")
		.replace("**", ".*")
		.replace("/*", "[^/]*");

	try {
		return new RegExp(`^${r}`);
	} catch (e) {
		console.info(e);
		return null;
	}
};

const getTypeStateKey = (
	stateKey: string,
	subpart: boolean = false,
): string => {
	const pathArray = stateKey.split("/");
	switch (true) {
		case pathArray.length === 0:
			return stateKey.includes(".json") ? TYPES.JSON : TYPES.OBJECT;
		case stateKey.includes("_sigs/"):
			return TYPES.SIGNATURE;
		case pathArray[pathArray.length - 1].includes(".json"):
			return TYPES.JSON;
		case subpart:
			return TYPES.FOLDER;
		default:
			return TYPES.OBJECT;
	}
};

const getParentKey = (pkg: Package, relativePath: string): Package => {
	if (!pkg) {
		return undefined;
	}
	let result = pkg;
	const splited = relativePath.split("/");
	if (!pkg.content[splited[0]]) {
		return result;
	}

	for (let index = 0; index < splited.length; index++) {
		const key = splited.slice(0, index + 1).join("/");
		if (result && !result.content[key]) {
			break;
		}
		result = result.content[key];
	}

	return result;
};

const getParentKeyFromPackages = (
	pkgs: Packages,
	relativePath: string,
): Package => {
	let result: Package;
	const splited = relativePath.split("/");
	if (!pkgs[splited[0]]) {
		return result;
	}

	for (let index = 0; index < splited.length; index++) {
		const key = splited.slice(0, index + 1).join("/");
		if (!result && pkgs[key]) {
			result = pkgs[key];
			continue;
		}
		if (result && !result.content[key]) {
			break;
		}
		result = result.content[key];
	}

	return result;
};

const fsFromSignature = (
	result: Packages = {},
	stateKey: string = "",
	state: any = {},
) => {
	const rawSignature = state[stateKey];
	const signature: Signature = JSON.parse(
		window.atob(rawSignature.protected),
	);
	const regexs = [
		...signature.pvs.include.map(cleanRegex),
		...signature.pvs.exclude.map(cleanRegex),
	];

	const packageName = stateKey.replace("_sigs/", "").replace(".json", "");

	const pkg = {
		id: stateKey,
		type: TYPES.PACKAGE,
		name: packageName,
		content: {
			[TYPES.SIGNATURE]: {
				type: TYPES.SIGNATURE,
				id: stateKey,
				name: stateKey.replace("_sigs/", ""),
				content: signature,
				raw: rawSignature,
			},
		},
		included: new Set([stateKey]),
	};

	Object.keys(state).forEach((key: string): Package => {
		const isInsideSignature = regexs.some(
			(regex) => regex && key.match(regex),
		);
		if (
			!isInsideSignature ||
			key.indexOf("_sigs") === 0 ||
			key.indexOf("#spec") === 0
		) {
			return;
		}

		pkg.included.add(key);
		const pathArray = key.split("/");
		pathArray.forEach((name, index) => {
			const subpart = index + 1 < pathArray.length;
			const relativePath = pathArray.slice(0, index + 1).join("/");
			const parentPkg = getParentKey(pkg, relativePath);
			if (!parentPkg) {
				return;
			}
			if (parentPkg.id === relativePath) {
				return;
			}
			const p = {
				type: getTypeStateKey(relativePath, subpart),
				id: relativePath,
				name: name,
				content: !subpart ? state[key] : {},
			};
			parentPkg.content[relativePath] = p;
		});
	});

	result[packageName] = pkg;
	return result;
};

const fsFromUnsigned = (
	result: Packages = {},
	stateKey: string = "",
	state: any = {},
) => {
	const pathArray = stateKey.split("/");
	pathArray.forEach((name, index) => {
		const subpart = index + 1 < pathArray.length;
		const relativePath = pathArray.slice(0, index + 1).join("/");
		const p: Package = {
			type: getTypeStateKey(relativePath, subpart),
			id: relativePath,
			name: name,
			content: !subpart ? state[stateKey] : {},
		};
		const parentPkg = getParentKeyFromPackages(result, relativePath);
		if (!parentPkg) {
			result[relativePath] = p;
			return;
		}
		if (parentPkg.id === relativePath) {
			return;
		}
		parentPkg.content[relativePath] = p;
	});
};

export const FindByID = (fs: Packages, id: string): Package => {
	if (id.indexOf("_sigs/") === 0) {
		const findKey = Object.keys(fs).find((key) => {
			return fs[key].id === id;
		});

		return fs[findKey];
	}

	return getParentKeyFromPackages(fs, id);
};

export const ToggleSelectId = (fs: Packages, id: string): Packages => {
	const pkg = FindByID(fs, id);
	if (!pkg) {
		return fs;
	}

	pkg.selected = !pkg.selected;
	return fs;
};

export const CreateFsDescription = (json = {}) => {
	const packages: Packages = Object.keys(json).reduce((acc, stateKey) => {
		if (
			stateKey.indexOf("_sigs/") !== 0 ||
			stateKey.indexOf("#spec") === 0
		) {
			return acc;
		}

		return fsFromSignature(acc, stateKey, json);
	}, {});

	for (const stateKey of Object.keys(json)) {
		if (
			stateKey.indexOf("_sigs/") === 0 ||
			stateKey.indexOf("#spec") === 0
		) {
			continue;
		}
		if (
			Object.entries(packages).some(
				([_, value]) => value.included && value.included.has(stateKey),
			)
		) {
			continue;
		}

		fsFromUnsigned(packages, stateKey, json);
	}
	return packages;
};

export const GroupByParts = (json = {}) => {
	return Object.keys(json).reduce((acc, upperKey) => {
		if (upperKey.indexOf("/run.json") >= 0) {
			const k = upperKey.replace("/run.json", "");
			acc[k] = Object.keys(json).reduce((part, key) => {
				if (key.indexOf(k) >= 0) {
					return {
						...part,
						[key]: json[key],
					};
				}

				return part;
			}, {});
		}
		if (upperKey.indexOf("_config/") >= 0) {
			const k = [upperKey.split("/")[0], upperKey.split("/")[1]].join(
				"/",
			);
			acc[k] = Object.keys(json).reduce((part, key) => {
				if (key.indexOf(k) >= 0) {
					return {
						...part,
						[key]: json[key],
					};
				}

				return part;
			}, {});
		}
		if (upperKey.indexOf("_sigs/") >= 0) {
			const k = [upperKey.split("/")[0], upperKey.split("/")[1]].join(
				"/",
			);
			acc[k] = Object.keys(json).reduce((part, key) => {
				if (key.indexOf(k) >= 0) {
					return {
						...part,
						[key]: json[key],
					};
				}

				return part;
			}, {});
		}
		if (upperKey.indexOf("_hostconfig/") >= 0) {
			const k = "_hostconfig";
			acc[k] = Object.keys(json).reduce((part, key) => {
				if (key.indexOf(k) >= 0) {
					return {
						...part,
						[key]: json[key],
					};
				}

				return part;
			}, {});
		}
		return acc;
	}, {});
};
