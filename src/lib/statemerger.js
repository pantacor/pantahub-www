/*
 * Copyright (c) 2024 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { CreateFsDescription } from "./state";

/**
 * Merges the state with a given patch.
 * @param {Object} current - The current state to merge.
 * @param {Object} patch - The patch to apply to the current state.
 * @returns {Object} The merged state as a JSON string, sorted by keys.
 */
export function mergeState(current = {}, patch = {}) {
	const current_state = getState(current);
	const patch_state = getState(patch);

	mergePatch(current_state, patch_state);
	return cloneSorted(current_state.json);
}

/**
 * Removes a part from the state.
 * @param {Object} source - The source state to remove from.
 * @param {string} part - The part to remove.
 * @returns {Object} The modified state as a JSON string, sorted by keys.
 */
export function removeState(source, part) {
	const current_state = getState(source);
	removeFromState(current_state, part);
	return cloneSorted(current_state.json);
}

/**
 * Merges a patch into the current state.
 * @param {Object} currentState - The current state to be updated.
 * @param {Object} patch_state - The patch containing the changes to apply.
 * @returns {void}
 */
function mergePatch(currentState, patch_state) {
	for (const part in patch_state.json) {
		if (part.startsWith("_sigs/")) {
			if (part in currentState.json) {
				removeFromState(currentState, part);
			}
		}
	}

	for (const part in patch_state.json) {
		if (part.startsWith("#spec")) {
			continue;
		}
		console.log(`Adding part: ${part}`);
		currentState.json[part] = patch_state.json[part];
	}
}

/**
 * Removes a specified part from the state.
 * @param {Object} state - The state from which to remove the part.
 * @param {string} part - The part to be removed.
 * @returns {void}
 */
function removeFromState(state, part) {
	if (part.startsWith("_sigs/") && state.json[part]) {
		for (const key in state.fs) {
			const app = state.fs[key];
			if (app.id === part) {
				for (const p of app.included) {
					delete state.json[p];
					console.info(`deleting from state ${p}`);
				}
			}
		}
	}

	for (const key in state.json) {
		if (key.includes(part)) {
			delete state.json[key];
		}
	}
}

/**
 * Retrieves the state object from the provided data.
 * @param {Object} data - The data to be converted into a state object.
 * @returns {{json: Object, fs: Packages}} - An object containing the JSON and FS descriptions of the data.
 */
function getState(data) {
	return {
		json: data,
		fs: CreateFsDescription(data),
	};
}

export function sortObjectKeys(obj) {
	return Object.keys(obj)
		.sort()
		.reduce((acc, key) => {
			acc[key] = obj[key];
			return acc;
		}, {});
}

export function cloneSorted(obj) {
	return sortAndParseJSON(obj);
}

function sortKeysRecursively(obj) {
	if (typeof obj === "object" && obj !== null) {
		if (Array.isArray(obj)) {
			return obj.map(sortKeysRecursively);
		} else {
			let sortedObj = {};
			Object.keys(obj)
				.sort()
				.forEach((key) => {
					sortedObj[key] = obj[key];
				});
			return sortedObj;
		}
	} else {
		return obj;
	}
}

export function sortAndParseJSON(inputObject) {
	const sortedObject = JSON.parse(
		JSON.stringify(sortKeysRecursively(inputObject)),
	);

	return sortedObject;
}
