/*
 * Copyright (c) 2024 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * Executes a promise and returns an array containing either a resolved value or null,
 * along with either null (if successful) or an error object.
 *
 * @param {Promise} promise - The promise to execute.
 * @returns {Promise<[T | null, Error | null]>} An array where the first element is the result of
 *          the promise on success and null on failure, while the second element is null
 *          if the promise was successful or an error object otherwise.
 */
export const safeAwait = async (promise) => {
	try {
		const result = await promise;
		return [result, null];
	} catch (e) {
		return [null, e];
	}
};

export const safeResult = (fn) => {
	try {
		const result = fn();
		return [result, null];
	} catch (e) {
		return [null, e];
	}
};
