/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
const classesForMessageType = {
  INFO: 'info',
  ERROR: 'danger',
  SUCCESS: 'success',
  WONTGO: 'warning',
  NEW: 'info',
  INPROGRESS: 'info',
  DONE: 'success'
}

export const classForMessageType = type =>
  classesForMessageType[type] || 'warning'

export const classForDevStatus = classForMessageType

export const capitalize = (string, divider = ' ') =>
  string.toLowerCase().split(divider).map((word) => word[0].toUpperCase() + word.slice(1, word.length)).join(' ')

export const autoPartial = (func) => {
  const numberOfArguments = func.length
  const newFunc = (...args) => {
    return args.length < numberOfArguments
      ? autoPartial(func.bind(null, ...args))
      : func(...args)
  }
  Object.defineProperty(newFunc, 'length', {
    value: numberOfArguments,
    writable: false
  })
  return newFunc
}

export const PropsToQueryString = (props = {}) => {
  return Object.keys(props).reduce((query, key, index) => {
    const separator = index > 0 ? '&' : '?'
    return `${query}${separator}${key}=${props[key]}`
  }, '')
}

export const Slugify = (string) => {
  const a = 'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;'
  const b = 'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------'
  const p = new RegExp(a.split('').join('|'), 'g')

  return string.toString().toLowerCase()
    .replace(/\s+/g, '-') // Replace spaces with -
    .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
    .replace(/&/g, '-and-') // Replace & with 'and'
    .replace(/[^\w-]+/g, '') // Remove all non-word characters
    .replace(/--+/g, '-') // Replace multiple - with single -
    .replace(/^-+/, '') // Trim - from start of text
    .replace(/-+$/, '') // Trim - from end of text
}
