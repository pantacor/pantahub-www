export const logsFactory = () => ({
  start: 0,
  page: 17,
  count: 17,
  entries: [
    {
      id: '5cec5f64f23edd0009e6af58',
      dev: 'prn:::devices:/5cc33b1037966d0008f18375',
      own: 'prn:::accounts:/5c8f93dceea823000876c4fa',
      'time-created': '2020-05-27T22:06:28.891540081Z',
      tsec: 1558994776,
      tnano: 401747,
      src: 'external',
      lvl: 'DEBUG',
      msg: 'will loop again '
    },
    {
      id: '5cec5f64f23edd0009e6af5b',
      dev: 'prn:::devices:/5cc33b1037966d0008f18375',
      own: 'prn:::accounts:/5c8f93dceea823000876c4fa',
      'time-created': '2020-05-27T22:06:28.891546246Z',
      tsec: 1558994780,
      tnano: 44796,
      src: 'controller',
      lvl: 'DEBUG',
      msg: 'going to state = STATE_WAIT'
    },
    {
      id: '5cec5f64f23edd0009e6af59',
      dev: 'prn:::devices:/5cc33b1037966d0008f18375',
      own: 'prn:::accounts:/5c8f93dceea823000876c4fa',
      'time-created': '2020-05-27T22:06:28.891542768Z',
      tsec: 1558994776,
      tnano: 418554,
      src: 'updater',
      lvl: 'DEBUG',
      msg: 'trail found, using remote'
    },
    {
      id: '5cec5f64f23edd0009e6af5a',
      dev: 'prn:::devices:/5cc33b1037966d0008f18375',
      own: 'prn:::accounts:/5c8f93dceea823000876c4fa',
      'time-created': '2020-05-27T22:06:28.891544582Z',
      tsec: 1558994777,
      tnano: 688989,
      src: 'updater',
      lvl: 'DEBUG',
      msg: "steps found in NEW state = '0'"
    },
    {
      id: '5cec5f64f23edd0009e6af5c',
      dev: 'prn:::devices:/5cc33b1037966d0008f18375',
      own: 'prn:::accounts:/5c8f93dceea823000876c4fa',
      'time-created': '2020-05-27T22:06:28.89154758Z',
      tsec: 1558994780,
      tnano: 58057,
      src: 'PLATFORM',
      lvl: 'DEBUG',
      msg: "2020-05-27T22:06:11.250498+00:00 localhost pvr-auto-follow: + echo 'Pantahub not configured yet ...' "
    },
    {
      id: '5cec5f64f23edd0009e6af5f',
      dev: 'prn:::devices:/5cc33b1037966d0008f18375',
      own: 'prn:::accounts:/5c8f93dceea823000876c4fa',
      'time-created': '2020-05-27T22:06:28.891556893Z',
      tsec: 1558994780,
      tnano: 100132,
      src: 'PLATFORM',
      lvl: 'DEBUG',
      msg: "2020-05-27T22:06:16.253666+00:00 localhost pvr-auto-follow: + '[' -f /storage/config/pantahub.config ] "
    },
    {
      id: '5cec5f64f23edd0009e6af5d',
      dev: 'prn:::devices:/5cc33b1037966d0008f18375',
      own: 'prn:::accounts:/5c8f93dceea823000876c4fa',
      'time-created': '2020-05-27T22:06:28.891554828Z',
      tsec: 1558994780,
      tnano: 71556,
      src: 'PLATFORM',
      lvl: 'DEBUG',
      msg: '2020-05-27T22:06:11.250576+00:00 localhost pvr-auto-follow: Pantahub not configured yet ... '
    },
    {
      id: '5cec5f64f23edd0009e6af5e',
      dev: 'prn:::devices:/5cc33b1037966d0008f18375',
      own: 'prn:::accounts:/5c8f93dceea823000876c4fa',
      'time-created': '2020-05-27T22:06:28.891555931Z',
      tsec: 1558994780,
      tnano: 86661,
      src: 'PLATFORM',
      lvl: 'DEBUG',
      msg: '2020-05-27T22:06:11.250617+00:00 localhost pvr-auto-follow: + sleep 5 '
    },
    {
      id: '5cec5f64f23edd0009e6af60',
      dev: 'prn:::devices:/5cc33b1037966d0008f18375',
      own: 'prn:::accounts:/5c8f93dceea823000876c4fa',
      'time-created': '2020-05-27T22:06:28.891557866Z',
      tsec: 1558994780,
      tnano: 113748,
      src: 'PLATFORM',
      lvl: 'DEBUG',
      msg: "2020-05-27T22:06:16.253752+00:00 localhost pvr-auto-follow: + echo 'Pantahub not configured yet ...' "
    },
    {
      id: '5cec5f64f23edd0009e6af63',
      dev: 'prn:::devices:/5cc33b1037966d0008f18375',
      own: 'prn:::accounts:/5c8f93dceea823000876c4fa',
      'time-created': '2020-05-27T22:06:28.891563269Z',
      tsec: 1558994781,
      tnano: 373826,
      src: 'PLATFORM',
      lvl: 'DEBUG',
      msg: "2020-05-27T22:06:21.256978+00:00 localhost pvr-auto-follow: + '[' -f /storage/config/pantahub.config ] "
    },
    {
      id: '5cec5f64f23edd0009e6af61',
      dev: 'prn:::devices:/5cc33b1037966d0008f18375',
      own: 'prn:::accounts:/5c8f93dceea823000876c4fa',
      'time-created': '2020-05-27T22:06:28.891561174Z',
      tsec: 1558994780,
      tnano: 127136,
      src: 'PLATFORM',
      lvl: 'DEBUG',
      msg: '2020-05-27T22:06:16.253822+00:00 localhost pvr-auto-follow: Pantahub not configured yet ... '
    },
    {
      id: '5cec5f64f23edd0009e6af62',
      dev: 'prn:::devices:/5cc33b1037966d0008f18375',
      own: 'prn:::accounts:/5c8f93dceea823000876c4fa',
      'time-created': '2020-05-27T22:06:28.891562197Z',
      tsec: 1558994780,
      tnano: 140893,
      src: 'PLATFORM',
      lvl: 'DEBUG',
      msg: '2020-05-27T22:06:16.253870+00:00 localhost pvr-auto-follow: + sleep 5 '
    },
    {
      id: '5cec5f64f23edd0009e6af64',
      dev: 'prn:::devices:/5cc33b1037966d0008f18375',
      own: 'prn:::accounts:/5c8f93dceea823000876c4fa',
      'time-created': '2020-05-27T22:06:28.891564232Z',
      tsec: 1558994781,
      tnano: 390401,
      src: 'PLATFORM',
      lvl: 'DEBUG',
      msg: "2020-05-27T22:06:21.257065+00:00 localhost pvr-auto-follow: + echo 'Pantahub not configured yet ...' "
    },
    {
      id: '5cec5f64f23edd0009e6af67',
      dev: 'prn:::devices:/5cc33b1037966d0008f18375',
      own: 'prn:::accounts:/5c8f93dceea823000876c4fa',
      'time-created': '2020-05-27T22:06:28.891567159Z',
      tsec: 1558994786,
      tnano: 395228,
      src: 'PLATFORM',
      lvl: 'DEBUG',
      msg: "2020-05-27T22:06:26.260385+00:00 localhost pvr-auto-follow: + '[' -f /storage/config/pantahub.config ] "
    },
    {
      id: '5cec5f64f23edd0009e6af65',
      dev: 'prn:::devices:/5cc33b1037966d0008f18375',
      own: 'prn:::accounts:/5c8f93dceea823000876c4fa',
      'time-created': '2020-05-27T22:06:28.891565244Z',
      tsec: 1558994781,
      tnano: 404317,
      src: 'PLATFORM',
      lvl: 'DEBUG',
      msg: '2020-05-27T22:06:21.257138+00:00 localhost pvr-auto-follow: Pantahub not configured yet ... '
    },
    {
      id: '5cec5f64f23edd0009e6af66',
      dev: 'prn:::devices:/5cc33b1037966d0008f18375',
      own: 'prn:::accounts:/5c8f93dceea823000876c4fa',
      'time-created': '2020-05-27T22:06:28.891566207Z',
      tsec: 1558994781,
      tnano: 417820,
      src: 'PLATFORM',
      lvl: 'DEBUG',
      msg: '2020-05-27T22:06:21.257180+00:00 localhost pvr-auto-follow: + sleep 5 '
    },
    {
      id: '5cec5f64f23edd0009e6af68',
      dev: 'prn:::devices:/5cc33b1037966d0008f18375',
      own: 'prn:::accounts:/5c8f93dceea823000876c4fa',
      'time-created': '2020-05-27T22:06:28.891568142Z',
      tsec: 1558994786,
      tnano: 622471,
      src: 'pantahub-api',
      lvl: 'DEBUG',
      msg: "PH available at '51.15.88.151:443'"
    }
  ]
})
