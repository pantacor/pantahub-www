/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'

// Styles
import '@mdi/font/scss/materialdesignicons.scss'
import './App.scss'

// Third party components
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router/immutable'
import { MemoryRouter } from 'react-router'

// Application data store
import createStore, { history } from './store'

// Router
import { RouterLayout } from './router'
import { fetchUserData } from './store/auth/actions'
import { isElectron } from './lib/native'
import { getProfile } from './store/profile/actions'
import { Track } from './lib/analytics.helper'
import { GetCustomersAction, GetSubscriptionsAction } from './store/billing/actions'
// import { BrowserRouter } from 'react-router-dom'

const Router = isElectron() ? MemoryRouter : ConnectedRouter

// Application Root component
const store = createStore()

class App extends React.Component {
  componentDidMount () {
    const searchParams = new URLSearchParams(window.location.search)
    const isLogged = store.getState().auth.token && store.getState().auth.token !== ''
    const username = store.getState().auth.username

    if (searchParams.get('redirect')) {
      window.localStorage.setItem('returnto', searchParams.get('redirect'))
    }

    if (isLogged) {
      setTimeout(() => store.dispatch(fetchUserData()), 500)
      setTimeout(() => store.dispatch(getProfile()), 0)
      setTimeout(() => store.dispatch(GetCustomersAction()), 0)
      setTimeout(() => store.dispatch(GetSubscriptionsAction()), 0)

      Track('hub-app-loading-with-user', { screen_name: 'Main', username })
      if (window.mixpanel && window.mixpanel.identify) {
        window.mixpanel.identify(username)
      }
    } else {
      if (window.mixpanel && window.mixpanel.identify) {
        window.mixpanel.identify(null)
      }
      Track('hub-app-loading-without-user', { screen_name: 'Main' })
    }
  }

  render () {
    return (
      <Provider store={store}>
        <Router history={history}>
          <RouterLayout />
        </Router>
      </Provider>
    )
  }
}

export default App
