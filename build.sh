#!/bin/bash

set -e 

export NODE_ENV=production
export GENERATE_SOURCEMAP=false 

react-app-rewired build 
# sed -i 's/\],/\,\/\^\\\/blog\/\]\,/' build/service-worker.js || true
# echo 'self.addEventListener("install", (event) => self.skipWaiting())' >> build/service-worker.js
