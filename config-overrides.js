const NodePolyfillPlugin = require("node-polyfill-webpack-plugin");

module.exports = function override(config, env) {
	config.plugins.push(new NodePolyfillPlugin());

	if (config.plugins) {
		config.plugins = config.plugins.filter(
			(plugin) => plugin.constructor.name !== "ESLintWebpackPlugin",
		);
	}

	return config;
};
